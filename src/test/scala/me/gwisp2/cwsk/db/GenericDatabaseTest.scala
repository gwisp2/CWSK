package me.gwisp2.cwsk.db

import me.gwisp2.cwsk.model.DatabaseMigration
import org.scalatest.testng.TestNGSuite
import org.testng.annotations.BeforeClass

abstract class GenericDatabaseTest extends TestNGSuite {
  protected var db: Db = _

  @BeforeClass
  def initDatabase(): Unit = {
    db = TestDatabase.createInstance()
    val migration = new DatabaseMigration(db)
    migration.clean()
    migration.migrate()
  }
}
