package me.gwisp2.cwsk.db

import com.typesafe.config.ConfigFactory
import slick.jdbc.JdbcBackend.Database

object TestDatabase {
  def createInstance(): Db = {
    val jdbcDb = Database.forConfig("test.db", ConfigFactory.load())
    new SlickJdbcDb(jdbcDb)
  }
}
