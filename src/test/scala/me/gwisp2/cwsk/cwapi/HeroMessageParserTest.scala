package me.gwisp2.cwsk.cwapi

import me.gwisp2.cwsk.bot.parsed.{EquipmentItem, HeroMessage}
import me.gwisp2.cwsk.dto.PetInfo
import org.scalatest.FlatSpec

class HeroMessageParserTest extends FlatSpec {
  "Equipment line" should "be parsed" in {
    val data = Array(
      ("War hammer +15⚔️ +15🛡", EquipmentItem("War hammer +15⚔️ +15🛡", "War hammer", bonusAtk = 15, bonusDef = 15)),
      ("Mithril boots +8🛡", EquipmentItem("Mithril boots +8🛡", "Mithril boots", bonusDef = 8)),
      ("Saddlebag +5🎒", EquipmentItem("Saddlebag +5🎒", "Saddlebag", bonusBagSlots = 5)),
      ("Flask +2🔋", EquipmentItem("Flask +2🔋", "Flask", bonusStamina = 2)),
      ("⚡️+4 Some shield +6⚔️ +21\uD83D\uDEE1", EquipmentItem("⚡️+4 Some shield +6⚔️ +21\uD83D\uDEE1",
        "Some shield", enchantmentLevel = 4, bonusAtk = 6, bonusDef = 21))
    )
    for ((line, expected) <- data) {
      assert(HeroMessage.parseEquipmentItem(line) == expected)
    }
  }

  "/hero" should "be parsed" in {
    val message =
      """🌹[BBS]Не капибара
🏅Уровень: 27
⚔️Атака: 79 🛡Защита: 100
🔥Опыт: 30034/32038
🔋Выносливость: 1/14
💧Мана: 270/270
💰39 👝1
📚Ранг: 📕📗
🏛Класс: /class

Питомец:
🐭 мышь Шпоня (19 lvl) 😁 /pet

🎽Экипировка +25⚔️+15🛡
War hammer +15⚔️ +15🛡
Hunter dagger +10⚔️

🎒Рюкзак: 6/20 /inv
📦Warehouse: 326 /stock"""
    assert(HeroMessage.parser.parse(message).contains(HeroMessage("🌹[BBS]Не капибара", Seq(
      EquipmentItem("War hammer +15⚔️ +15🛡", "War hammer", bonusAtk = 15, bonusDef = 15),
      EquipmentItem("Hunter dagger +10⚔️", "Hunter dagger", bonusAtk = 10)
    ), Some(PetInfo("🐭", "мышь Шпоня", 19)))))
  }
}
