package me.gwisp2.cwsk.cwapi

import me.gwisp2.cwsk.cwapi.ApiResponse.Code
import me.gwisp2.cwsk.cwapi.JsonMarshallers._
import me.gwisp2.cwsk.cwapi.methods._
import org.scalatest.FlatSpec

class MarshallingTest extends FlatSpec {
  "toJson and fromJson" should "be mutually inverse for request payloads" in {
    testToFromJsonInverse(CreateAuthCode.Request(123))
    testToFromJsonInverse(GrantToken.Request(123, "authcodehere"))
    testToFromJsonInverse(AuthAdditionalOperation.Request(OperationType.GetStock))
  }
  it should "be mutually inverse for requests" in {
    testToFromJsonInverse(CreateAuthCode.Request(123).toRequest)
    testToFromJsonInverse(GrantToken.Request(123, "authcodehere").toRequest)
    testToFromJsonInverse(AuthAdditionalOperation.Request(OperationType.GetStock).toRequest("TOKEN"))
  }
  it should "be mutually inverse for response payloads" in {
    testToFromJsonInverse(CreateAuthCode.Response(123))
    testToFromJsonInverse(GrantToken.Response(123, "USER INGAME ID", "TOKEN"))
    testToFromJsonInverse(AuthAdditionalOperation.Response(123, OperationType.GetUserProfile))
  }
  it should "be mutually inverse for responses" in {
    testToFromJsonInverse(ApiResponse.Simple(
      CreateAuthCode.action, Code.Ok,
      CreateAuthCode.Response(123)))
    testToFromJsonInverse(ApiResponse.Simple(
      GrantToken.action, Code.Ok,
      GrantToken.Response(123, "USER INGAME ID", "TOKEN")))
    testToFromJsonInverse(ApiResponse.WithUUID(
      AuthAdditionalOperation.action, Code.Ok, "UUID",
      AuthAdditionalOperation.Response(123, OperationType.GetUserProfile)
    ))
    testToFromJsonInverse(ApiResponse.Unknown(
      Some(AuthAdditionalOperation.action), Code.BadFormat))
    testToFromJsonInverse(ApiResponse.Unknown(
      Some(AuthAdditionalOperation.action), Code.ActionNotFound))
    testToFromJsonInverse(ApiResponse.Unknown(
      None, Code.ActionNotFound))
  }
  it should "be mutually inverse for responses without type hints" in {
    testResponseWithoutTypeHint(ApiResponse.Simple(
      CreateAuthCode.action, Code.Ok,
      CreateAuthCode.Response(123)))
    testResponseWithoutTypeHint(ApiResponse.Simple(
      RequestProfile.action, Code.InvalidToken,
      ApiMethod.InvalidToken("x")))
    testResponseWithoutTypeHint(ApiResponse.Simple(
      GrantToken.action, Code.Ok,
      GrantToken.Response(123, "USER INGAME ID", "TOKEN")))
    testResponseWithoutTypeHint(ApiResponse.WithUUID(
      AuthAdditionalOperation.action, Code.Ok, "UUID",
      AuthAdditionalOperation.Response(123, OperationType.GetUserProfile)
    ))
    testResponseWithoutTypeHint(ApiResponse.Unknown(
      Some(AuthAdditionalOperation.action), Code.BadFormat))
    testResponseWithoutTypeHint(ApiResponse.Unknown(
      Some(AuthAdditionalOperation.action), Code.ActionNotFound))
    testResponseWithoutTypeHint(ApiResponse.Unknown(
      None, Code.ActionNotFound))
  }

  private def testToFromJsonInverse[T: Manifest](o: T) {
    val asJson = toJson(o)
    println(asJson)
    val asReq = fromJson[T](asJson)
    assert(o == asReq)
  }

  private def testResponseWithoutTypeHint(o: AnyRef) {
    val asJson = toJson(o)
    println(asJson)
    val asResp = responseFromJson(asJson)
    assert(o == asResp)
  }
}
