package me.gwisp2.cwsk.cwapi

import me.gwisp2.cwsk.bot.parsed._
import org.scalatest.FlatSpec

class StockParserTest extends FlatSpec {
  "Guild warehouse" should "be parsed" in {
    val message =
      """Guild Warehouse:
w31 War hammer x 1
w17 Mithril dagger x 1
r05 Hunter Dagger recipe x 1
r03 Hunter Bow recipe x 1
k18 Clarity Bracers part x 4"""
    val expected = List(Item("w31", "War hammer", 1), Item("w17", "Mithril dagger", 1),
      Item("r05", "Hunter Dagger recipe", 1), Item("r03", "Hunter Bow recipe", 1),
      Item("k18", "Clarity Bracers part", 4))

    assert(GuildWarehouse.parser.parse(message).contains(GuildWarehouse(expected)))
  }

  "Auction item list" should "be parsed" in {
    val message =
      """You have for sale:
/lot_r12 Hunter Helmet recipe (2)
/lot_r05 Hunter Dagger recipe (1)


Your lots: /lots

Your bids: /bids"""
    val expected = List(Item("r12", "Hunter Helmet recipe", 2), Item("r05", "Hunter Dagger recipe", 1))

    assert(AuctionItemList.parser.parse(message).contains(AuctionItemList(expected)))
  }

  "/t" should "be parsed" in {
    val message =
      """Предложения Vial of Greed сейчас:
        |35 шт. по 15💰
        |➕Покупка:
        |Купить 1: /wtb_p07
        |Купить 5: /wtb_p07_5
        |
        |У тебя 15💰 и 0 Vial of Greed
        |➖Продажа:
        |Продать 1 быстро: /wts_p07
        |Продать 5 быстро: /wts_p07_5
        |Продать 10 за 15💰 каждый: /wts_p07_10_15""".stripMargin
    val expected = ExchangeTMessage("p07", "Vial of Greed")
    assert(ExchangeTMessage.parser.parse(message).contains(expected))
  }

  "Stock" should "be parsed" in {
    val message =
      """Cheese (8)  /view_521
        |First Aid Kit (1) /use_aid
        |Pouch of gold (2) /use_100
        |Vial of Nature (37) /use_p10
        |Some item (2)""".stripMargin
    val expected = StockMessage(Seq(
      Item("521", "Cheese", 8),
      Item("aid", "First Aid Kit", 1),
      Item("100", "Pouch of gold", 2),
      Item("p10", "Vial of Nature", 37)
    ))
    assert(StockMessage.parser.parse(message).contains(expected))
  }

  "Workshop" should "be parsed" in {
    val message =
      """⚒На верстаке ты видишь:
        |[пусто]
        |
        |📦Склад:
        |/a_21 Bone powder x 38
        |/a_04 Bone x 5
        |/a_09 Cloth x 1""".stripMargin
    val expected = StockMessage(Seq(
      Item("21", "Bone powder", 38),
      Item("04", "Bone", 5),
      Item("09", "Cloth", 1)
    ))
    assert(StockMessage.parser.parse(message).contains(expected))
  }

  "/more" should "be parsed" in {
    val message =
      """📦Склад:
        |/a_21 Bone powder x 13
        |/a_04 Bone x 122
        |/a_06 Charcoal x 17
        |/a_05 Coal x 96""".stripMargin
    val expected = StockMessage(Seq(
      Item("21", "Bone powder", 13),
      Item("04", "Bone", 122),
      Item("06", "Charcoal", 17),
      Item("05", "Coal", 96)
    ))
    assert(StockMessage.parser.parse(message).contains(expected))
  }

  "/craft list" should "be parsed" in {
    val message =
      """Рецепты:
        |Lvl: 1
        |/craft_100 Pouch of gold
        |/craft_19 Steel
        |Lvl: 2
        |/craft_27 Steel mold
        |/craft_25 Silver alloy
        |
        |Склад: /more""".stripMargin
    val expected = CraftItemList(Seq(
      Item("100", "Pouch of gold", 1),
      Item("19", "Steel", 1),
      Item("27", "Steel mold", 1),
      Item("25", "Silver alloy", 1)
    ))
    assert(CraftItemList.parser.parse(message).contains(expected))
  }

  "Shop items list" should "be parsed" in {
    val message =
      """На прилавке ты видишь следующие товары:
        |
        |Sandals +1🛡
        |1💰
        |/buy_a11
        |
        |Leather shoes +2🛡
        |43💰
        |/buy_a12
        |
        |Steel boots +3🛡
        |92💰
        |/buy_a13
        |
        |Silver boots +5🛡
        |Requires: 📕
        |285💰
        |/buy_a14""".stripMargin
    val expected = NpcShopItemList(Seq(
      Item("a11", "Sandals", 1),
      Item("a12", "Leather shoes", 1),
      Item("a13", "Steel boots", 1),
      Item("a14", "Silver boots", 1)
    ))
    assert(NpcShopItemList.parser.parse(message) === Some(expected))
  }

  "Donate shop items list" should "be parsed" in {
    val message =
      """На прилавке ты видишь следующие товары:
        |
        |Vial of Оblivion
        |3💎
        |/buy_pl1
        |Allows you to forget the skills you've trained so hard [Removes 1SP from the chosen skill]""".stripMargin
    val expected = NpcShopItemList(Seq(
      Item("pl1", "Vial of Оblivion", 1)
    ))
    assert(NpcShopItemList.parser.parse(message) === Some(expected))
  }

  "Player shop items" should "be parsed" in {
    val message =
      """Добро пожаловать в Name Forgery #149.
        |Geninc 116/650💧the Blacksmith Ночи
        |
        |Forgery is открыто.
        |
        |Order Boots, 150💧 50💰 /ws_sSJit_a29
        |Clarity Shoes, 150💧 50💰 /ws_sSJit_a38
        |Order Gauntlets, 150💧 50💰 /ws_sSJit_a30
        |Order Armor, 150💧 50💰 /ws_sSJit_a27
        |Steel arrows pack, 120💧 1💰 /ws_sSJit_511
        |
        |We can do better. /ws_sSJit""".stripMargin
    val expected = PlayerShopItemList(Seq(
      Item("a29", "Order Boots", 1),
      Item("a38", "Clarity Shoes", 1),
      Item("a30", "Order Gauntlets", 1),
      Item("a27", "Order Armor", 1),
      Item("511", "Steel arrows pack", 1)
    ))
    assert(PlayerShopItemList.parser.parse(message) === Some(expected))
  }
}
