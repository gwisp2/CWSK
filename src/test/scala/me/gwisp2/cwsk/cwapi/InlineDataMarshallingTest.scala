package me.gwisp2.cwsk.cwapi

import me.gwisp2.cwsk.bot.inline._
import me.gwisp2.cwsk.dto.GuildPermission
import me.gwisp2.cwsk.dto.items._
import org.scalatest.FlatSpec

class InlineDataMarshallingTest extends FlatSpec {
  "toBytes, fromBytes and fromString, toString" should "be mutually inverse" in {
    val samples = Array(
      ShowUser(120),
      UseItem(1, "u01", 2),
      NoData(5),
      ShowPermissions(3),
      EditPermissions(10, GuildPermission.GsGuild, allowed = true),
      GuildStockReq(ItemCategory.other, Sort[PricedStockItem](PricedItemSortType.FromItemSortType(ItemSortType.byName), desc = true)),
      GuildStockReq(ItemCategory.other, Sort[PricedStockItem](PricedItemSortType.ByTotalPrice(), desc = true)),
    )
    samples.foreach(testToFrom)
  }

  private def testToFrom(o: InlineData) {
    val bytes = InlineData.toBytes(o)
    val fromBytes = InlineData.fromBytes(bytes)
    assert(fromBytes.contains(o))

    val str = InlineData.toString(o)
    val fromString = InlineData.fromString(str)
    assert(fromString.contains(o))

    assert(str.length <= 64)
  }
}
