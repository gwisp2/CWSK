package me.gwisp2.cwsk.cwapi

import me.gwisp2.cwsk.dto.items.StockItemType
import me.gwisp2.cwsk.model.types.GoldSinkRule
import me.gwisp2.cwsk.model.types.GoldSinkRule.ParserException
import org.scalatest.FlatSpec

class GoldSinkRuleParserTest extends FlatSpec {

  val itemNames = Map(
    "Coal" -> StockItemType(Some("05"), "Coal"),
    "Charcoal" -> StockItemType(Some("06"), "Charcoal"),
    "Powder" -> StockItemType(Some("07"), "Powder")
  )

  "Gold sink rule" should "be converted to text and parsed" in {
    val rule = GoldSinkRule(IndexedSeq(
      GoldSinkRule.Item("Coal", "05", None, 4),
      GoldSinkRule.Item("Charcoal", "06", Some(5), 3)
    ))
    val asText = rule.toText
    println(asText)
    val fromText = GoldSinkRule.parseText(asText, itemNames.get)
    assert(fromText == rule)
  }


  "Gold sink rule" should "not parse with unknown item name" in {
    val rule = GoldSinkRule(IndexedSeq(
      GoldSinkRule.Item("Coal", "05", None, 4),
      GoldSinkRule.Item("UNKNOWNITEM", "06", Some(5), 3)
    ))
    val asText = rule.toText
    println(asText)

    assertThrows[ParserException] {
      GoldSinkRule.parseText(asText, itemNames.get)
    }
  }
}
