package me.gwisp2.cwsk.cwapi

import me.gwisp2.cwsk.bot.parsed.GuildCommanderMessage
import org.scalatest.FlatSpec

class GuildCommanderParserTest extends FlatSpec {
  "Gold commander" should "parse with tag" in {
    val message =
      """🌹[BBS] Blood Sunrise
        |Commander: Енот
        |🏅Level: 5 🎖Glory: 6234
        |👥 15/15""".stripMargin
    val fromText = GuildCommanderMessage.parser.parse(message)
    assert(fromText.contains(GuildCommanderMessage(Some("BBS"), "Blood Sunrise", "Енот")))
  }

  "Gold commander with cyrillic characters" should "parse with tag" in {
    val message =
      """☘️[НОЖ] в руки кинжал
        |Commander: ДЛЯ ФРАУ МЮЛЛЕР
        |🏅Level: 5 🎖Glory: 4776
        |👥 15/15""".stripMargin
    val fromText = GuildCommanderMessage.parser.parse(message)
    assert(fromText.contains(GuildCommanderMessage(Some("НОЖ"), "в руки кинжал", "ДЛЯ ФРАУ МЮЛЛЕР")))
  }

  "Gold commander" should "parse without tag" in {
    val message =
      """🌹Blood Sunrise
        |Commander: Енот
        |🏅Level: 3 🎖Glory: 1459
        |👥 9/9
        |➖
        |#1 🏹26 [⚔️] X
        |#2 🏹26 [🛡] X
        |#3 ⚔️26 [🌲] X
        |#4 📦25 [🛌] X
        |#5 ⚒25 [🛡] X
        |#6 📦25 [🛌] X
        |#7 ⚗️23 [🛌] X
        |#8 📦22 [🌲] X
        |#9 🛡21 [🌲] X
        |➖""".stripMargin
    val fromText = GuildCommanderMessage.parser.parse(message)
    assert(fromText.contains(GuildCommanderMessage(None, "Blood Sunrise", "Енот")))
  }
}
