package me.gwisp2.cwsk.bot.formatter

import me.gwisp2.cwsk.dto.items.{StockItem, StockItemType}
import me.gwisp2.cwsk.service.ItemPricesService.ItemPrices
import org.scalatest.FlatSpec

class StockDeltaFormatterTest extends FlatSpec {
  "Stock delta" should "be formatted correctly if empty" in {
    val delta = Seq()
    val prices = ItemPrices.empty

    val formatterFull = new StockDeltaFormatter(short = false)
    val actualFull = formatterFull.format(delta, prices)
    val expectedFull =
      """<b>Изменения в стоке:</b>
        |
        |[изменений нет]
        |""".stripMargin

    val formatterShort = new StockDeltaFormatter(short = true)
    val actualShort = formatterShort.format(delta, prices)
    val expectedShort =
      """, <b>изменений нет</b>""".stripMargin

    assert(actualFull == expectedFull)
    assert(actualShort == expectedShort)
  }

  it should "be formatted correctly with both + and -" in {
    val delta = Seq(
      StockItem(StockItemType(Some("i1"), "Item 1"), 4),
      StockItem(StockItemType(Some("i2"), "Item 2"), 6),
      StockItem(StockItemType(Some("i3"), "Item 3"), -4)
    )
    val prices = ItemPrices.fromDoubleMap(Map(
      "Item 1" -> 4,
      "Item 2" -> 3
    ))

    val formatterFull = new StockDeltaFormatter(short = false)
    val actualFull = formatterFull.format(delta, prices)
    val expectedFull =
      """<b>Изменения в стоке:</b>
        |
        |<b>➕Приобретено (</b><code>≈💰34.0</code><b>):</b>
        |+4 Item 1<code> ≈💰16.0</code>
        |+6 Item 2<code> ≈💰18.0</code>
        |
        |<b>➖Потеряно (</b><code>≈💰0</code><b>):</b>
        |-4 Item 3
        |""".stripMargin

    val formatterShort = new StockDeltaFormatter(short = true)
    val actualShort = formatterShort.format(delta, prices)
    val expectedShort =
      """
        |<b>➕Приобретено (</b><code>≈💰34.0</code><b>):</b>
        |+4 Item 1<code> ≈💰16.0</code>
        |+6 Item 2<code> ≈💰18.0</code>
        |<b>➖Потеряно (</b><code>≈💰0</code><b>):</b>
        |-4 Item 3
        |""".stripMargin

    assert(actualFull == expectedFull)
    assert(actualShort == expectedShort)
  }

  it should "be formatted correctly with only +" in {
    val delta = Seq(
      StockItem(StockItemType(Some("i1"), "Item 1"), 4),
      StockItem(StockItemType(Some("i2"), "Item 2"), 6)
    )
    val prices = ItemPrices.fromDoubleMap(Map(
      "Item 1" -> 4,
      "Item 2" -> 3
    ))

    val formatterFull = new StockDeltaFormatter(short = false)
    val actualFull = formatterFull.format(delta, prices)
    val expectedFull =
      """<b>Изменения в стоке:</b>
        |
        |<b>➕Приобретено (</b><code>≈💰34.0</code><b>):</b>
        |+4 Item 1<code> ≈💰16.0</code>
        |+6 Item 2<code> ≈💰18.0</code>
        |""".stripMargin

    val formatterShort = new StockDeltaFormatter(short = true)
    val actualShort = formatterShort.format(delta, prices)
    val expectedShort =
      """
        |<b>➕Приобретено (</b><code>≈💰34.0</code><b>):</b>
        |+4 Item 1<code> ≈💰16.0</code>
        |+6 Item 2<code> ≈💰18.0</code>
        |""".stripMargin

    assert(actualFull == expectedFull)
    assert(actualShort == expectedShort)
  }

  it should "be formatted correctly with only -" in {
    val delta = Seq(
      StockItem(StockItemType(Some("i1"), "Item 1"), -4),
      StockItem(StockItemType(Some("i2"), "Item 2"), -6)
    )
    val prices = ItemPrices.fromDoubleMap(Map(
      "Item 1" -> 4,
      "Item 2" -> 3
    ))

    val formatterFull = new StockDeltaFormatter(short = false)
    val actualFull = formatterFull.format(delta, prices)
    val expectedFull =
      """<b>Изменения в стоке:</b>
        |
        |<b>➖Потеряно (</b><code>≈💰34.0</code><b>):</b>
        |-4 Item 1<code> ≈💰16.0</code>
        |-6 Item 2<code> ≈💰18.0</code>
        |""".stripMargin

    val formatterShort = new StockDeltaFormatter(short = true)
    val actualShort = formatterShort.format(delta, prices)
    val expectedShort =
      """
        |<b>➖Потеряно (</b><code>≈💰34.0</code><b>):</b>
        |-4 Item 1<code> ≈💰16.0</code>
        |-6 Item 2<code> ≈💰18.0</code>
        |""".stripMargin

    assert(actualFull == expectedFull)
    assert(actualShort == expectedShort)
  }
}
