package me.gwisp2.throttler

import me.gwisp2.scheduler.Scheduler

import scala.collection.mutable
import scala.concurrent.duration.FiniteDuration
import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.util.{Failure, Success, Try}

class Throttler(limit: Int, period: FiniteDuration)(implicit scheduler: Scheduler, ec: ExecutionContext) {
  // Guarded by this
  private val tasksQueue = mutable.Queue[ThrottledTask[_]]()
  private var tasksInProgress = 0
  private var tasksFinishedRecently = 0
  private val finishedTasksQueue = new TemporaryQueue[ThrottledTask[_]](scheduler, _ => onPeriodExpired())

  /** Invariant: finished in last `period` + currently in progress <= limit */
  def throttle[T](action: => Future[T]): Future[T] = {
    val task = new ThrottledTask[T](() => action)
    this.synchronized {
      tasksQueue += task
    }
    tryProcessQueue()
    task.promise.future
  }

  def isIdle: Boolean = this.synchronized {
    tasksInProgress == 0 && tasksFinishedRecently == 0 && tasksQueue.isEmpty
  }

  private def tryProcessQueue(): Unit = {
    val tasksToExecute = this.synchronized {
      val numTasksCanProceed = Math.min(limit - tasksInProgress - tasksFinishedRecently, tasksQueue.size)
      tasksInProgress += numTasksCanProceed
      for (_ <- 1 to numTasksCanProceed) yield tasksQueue.dequeue()
    }
    tasksToExecute.foreach(t => startTask(t))
  }

  private def onPeriodExpired(): Unit = this.synchronized {
    tasksFinishedRecently -= 1
    tryProcessQueue()
  }

  private def startTask[T](task: ThrottledTask[T]): Unit = {
    Try(task.action()) match {
      case Success(future) => task.promise.completeWith(future)
      case Failure(exception) => task.promise.failure(exception)
    }
    task.promise.future.onComplete {
      _ =>
        this.synchronized {
          tasksFinishedRecently += 1
          tasksInProgress -= 1
          finishedTasksQueue.push(task, period)
        }
    }
  }

  protected class ThrottledTask[T](val action: () => Future[T]) {
    val promise: Promise[T] = Promise[T]()
  }

}
