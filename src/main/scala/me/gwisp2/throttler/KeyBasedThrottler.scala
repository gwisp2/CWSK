package me.gwisp2.throttler

import me.gwisp2.scheduler.Scheduler

import scala.collection.mutable
import scala.concurrent.duration.FiniteDuration
import scala.concurrent.{ExecutionContext, Future}

class KeyBasedThrottler[K](limit: Int, period: FiniteDuration)(implicit scheduler: Scheduler, ec: ExecutionContext) {
  private val throttlers = new mutable.HashMap[K, Throttler]()

  def throttle[T](key: K)(action: => Future[T]): Future[T] = {
    val throttler = this.synchronized {
      throttlers.getOrElseUpdate(key, new Throttler(limit, period))
    }

    throttler.throttle(action)
  }
}
