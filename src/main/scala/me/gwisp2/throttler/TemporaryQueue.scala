package me.gwisp2.throttler

import me.gwisp2.scheduler.Scheduler

import scala.collection.mutable
import scala.concurrent.duration.FiniteDuration

class TemporaryQueue[T](scheduler: Scheduler, onRemoved: T => Unit) {
  private val queue = new mutable.Queue[PushedElement]()

  def push(value: T, duration: FiniteDuration): Unit = {
    val removalTime = System.nanoTime() + duration.toNanos

    this.synchronized {
      queue += new PushedElement(value, removalTime)
    }

    scheduler.scheduleIn(duration) {
      removeOld()
    }
  }

  private def removeOld(): Unit = {
    val removedElements = this.synchronized {
      val oldElements = queue.takeWhile(_.shouldBeRemoved)
      for (_ <- oldElements.indices) {
        queue.dequeue()
      }
      oldElements
    }

    removedElements.view.map(_.value).foreach(onRemoved)
  }

  private class PushedElement(val value: T, val removeTime: Long) {
    def shouldBeRemoved: Boolean = {
      System.nanoTime() >= removeTime
    }
  }

}
