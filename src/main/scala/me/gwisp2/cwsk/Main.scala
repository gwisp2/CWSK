package me.gwisp2.cwsk

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.google.inject.{AbstractModule, Guice}
import com.typesafe.config.{Config, ConfigFactory}
import kamon.Kamon
import kamon.statsd.StatsDReporter
import kamon.system.SystemMetrics
import me.gwisp2.cwsk.bot.CwskBot.BotConfig
import me.gwisp2.cwsk.bot.modules._
import me.gwisp2.cwsk.bot.{CwskBot, TelegramErrorHandler}
import me.gwisp2.cwsk.cwapi.{AmpqCwApi, CwApi, MockCwApi}
import me.gwisp2.cwsk.db.{Db, SlickJdbcDb}
import me.gwisp2.cwsk.model.DatabaseMigration
import me.gwisp2.cwsk.service._
import me.gwisp2.scheduler.Scheduler
import me.gwisp2.tbot.PollingBotRunner
import net.codingwell.scalaguice.ScalaModule
import pureconfig.{ConfigReader, Derivation}
import slick.jdbc.JdbcBackend.Database

import scala.reflect.ClassTag

object Main extends App {

  class MyModule extends AbstractModule with ScalaModule {
    override def configure(): Unit = {
      val config = ConfigFactory.load()

      // implicit for loadConfig
      import Derivation.materializeDerivation
      val cwApiCredentials = loadConfig[AmpqCwApi.Credentials](config, "cwapi")

      val botConfig = pureconfig.loadConfigOrThrow[BotConfig](config, "bot")
      val slickDb = Database.forConfig("db", config)
      val db = new SlickJdbcDb(slickDb)

      implicit val actorSystem: ActorSystem = ActorSystem()
      val actorMaterializer = ActorMaterializer()

      bind[ActorSystem].toInstance(actorSystem)
      bind[ActorMaterializer].toInstance(actorMaterializer)

      bind[Config].toInstance(config)
      bind[BotConfig].toInstance(botConfig)
      bind[Database].toInstance(slickDb)
      bind[Db].toInstance(db)

      bind[Scheduler].asEagerSingleton()
      bind[CwApi].toInstance(cwApiCredentials match {
        case Some(credentials) => new AmpqCwApi(AmpqCwApi.Server.Cw3, credentials)
        case None => new MockCwApi()
      })
      bind[CwApiService].asEagerSingleton()
      bind[UserService].asEagerSingleton()
      bind[UserSettingsService].asEagerSingleton()
      bind[StockService].asEagerSingleton()
      bind[ItemPricesService].asEagerSingleton()
      bind[GoldSinkConfigService].asEagerSingleton()
      bind[GoldSinkService].asEagerSingleton()
      bind[ExpHistoryService].asEagerSingleton()
      bind[GuildStockService].asEagerSingleton()
      bind[TelegramErrorHandler].asEagerSingleton()
      bind[CwskBot].asEagerSingleton()
    }

    private def loadConfig[T: ClassTag](config: Config, path: String)(implicit reader: Derivation[ConfigReader[T]]): Option[T] = {
      if (config.hasPath(path)) {
        Some(pureconfig.loadConfigOrThrow[T](config, path))
      } else {
        None
      }
    }
  }

  /* Setup monitoring */
  Kamon.addReporter(new StatsDReporter())
  SystemMetrics.startCollecting()

  /* Startup */
  val injector = Guice.createInjector(new MyModule())

  import net.codingwell.scalaguice.InjectorExtensions._

  val db = injector.instance[Db]
  new DatabaseMigration(db).migrate()

  val bot = injector.instance[CwskBot]
  val api = injector.instance[CwApiService]

  bot.addModule[AnyUpdateModule]()
  bot.addModule[ExpModule]()
  bot.addModule[StockCommandsModule]()
  bot.addModule[GuildStockModule]()
  bot.addModule[AdminToolsModule]()
  bot.addModule[FeedbackModule]()
  bot.addModule[MeModule]()
  bot.addModule[SexModule]()
  bot.addModule[GuildRolesModule]()
  bot.addModule[CwApiAuthModule]()
  bot.addModule[GoldSinkModule]()
  bot.addModule[SettingsModule]()
  bot.addModule[MiscActionsModule]()
  bot.addModule[ProfileStockUpdateModule]()
  bot.addModule[DefaultReplyModule]()

  api.start()
  new PollingBotRunner(bot).run()
}
