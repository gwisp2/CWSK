package me.gwisp2.cwsk

import java.time.{LocalTime, ZoneId}

import me.gwisp2.scheduler.Schedule

object Schedules {
  val battle = Schedule.EveryDay(Seq(
    LocalTime.of(9, 0), LocalTime.of(17, 0), LocalTime.of(1, 0)
  ), ZoneId.of("Europe/Moscow"))
}
