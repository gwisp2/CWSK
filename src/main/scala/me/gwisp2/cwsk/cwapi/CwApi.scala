package me.gwisp2.cwsk.cwapi

import me.gwisp2.cwsk.cwapi.messages.{CwApiMessage, DealsPayload}

trait CwApi {
  def sendRequest[P <: ApiRequest.Payload](request: ApiRequest[P])

  def listenForResponses(handler: ApiResponse => Unit)

  def listenForDeals(handler: CwApiMessage[DealsPayload] => Unit)
}
