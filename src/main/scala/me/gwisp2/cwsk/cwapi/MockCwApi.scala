package me.gwisp2.cwsk.cwapi

import me.gwisp2.cwsk.cwapi.messages.{CwApiMessage, DealsPayload}
import me.gwisp2.cwsk.cwapi.methods.{ApiMethod, RequestProfile, RequestStock}

class MockCwApi extends CwApi {
  private var responseHandler: Option[ApiResponse => Unit] = None
  private var stockCounter = 1

  override def sendRequest[P <: ApiRequest.Payload](request: ApiRequest[P]): Unit = {
    request.payload match {
      case RequestStock.Request() =>
        stockCounter += 1
        handleResponse(ApiResponse.Simple(
          RequestStock.action, ApiResponse.Code.Ok, RequestStock.Response(252017642, Map(
            "Powder" -> stockCounter
          ))
        ))
      case RequestProfile.Request() =>
        handleResponse(ApiResponse.Simple(
          RequestProfile.action, ApiResponse.Code.Forbidden, ApiMethod.Forbidden("ZZZ", 252017642)
        ))
      case _ => /* Ignore */
    }
  }

  override def listenForResponses(handler: ApiResponse => Unit): Unit = {
    responseHandler = Some(handler)
  }

  override def listenForDeals(handler: CwApiMessage[DealsPayload] => Unit): Unit = {
    /* noop */
  }

  private def handleResponse(response: ApiResponse): Unit = {
    responseHandler.foreach(rh => rh(response))
  }
}
