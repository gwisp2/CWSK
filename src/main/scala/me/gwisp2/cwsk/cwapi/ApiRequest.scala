package me.gwisp2.cwsk.cwapi

case class ApiRequest[+P <: ApiRequest.Payload](action: String, token: Option[String] = None, payload: P)

object ApiRequest {

  abstract class Payload {
    def actionName: String
  }

  object Payload {

    abstract class Simple extends Payload {
      def toRequest: ApiRequest[this.type] = ApiRequest[this.type](action = actionName, token = None, payload = this)
    }

    abstract class WithToken extends Payload {
      def toRequest(token: String): ApiRequest[this.type] = ApiRequest[this.type](action = actionName, token = Some(token), payload = this)
    }

  }

}










