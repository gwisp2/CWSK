package me.gwisp2.cwsk.cwapi.messages

import java.util.Date

case class CwApiMessage[T](date: Date, payload: T)

case class DealsPayload(sellerId: String, sellerName: String, sellerCastle: String,
                        buyerId: String, buyerName: String, buyerCastle: String,
                        item: String, qty: Int, price: Int)
