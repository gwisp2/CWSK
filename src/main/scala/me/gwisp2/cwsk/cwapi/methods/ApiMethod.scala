package me.gwisp2.cwsk.cwapi.methods

import me.gwisp2.cwsk.cwapi.ApiResponse.Code
import me.gwisp2.cwsk.cwapi.methods.ApiMethod.{Forbidden, InvalidToken}
import me.gwisp2.cwsk.cwapi.{ApiResponse, JsonMarshallers}
import org.json4s.JsonAST.JValue

abstract class ApiMethod {
  val action: String
  protected val baseResponseTypesMap: Map[Code, Manifest[_ <: ApiResponse]] = Map(
    Code.InvalidToken -> manifest[ApiResponse.Simple[InvalidToken]],
    Code.Forbidden -> manifest[ApiResponse.Simple[Forbidden]]
  )
  protected val responseTypesMap: Map[Code, Manifest[_ <: ApiResponse]]
  protected val defaultResponseType: Manifest[_ <: ApiResponse] = manifest[ApiResponse.Unknown]

  final def decodeResponse(codeAndAction: ApiResponse.Unknown, value: JValue): ApiResponse = {
    val baseResponseType = baseResponseTypesMap.get(codeAndAction.result)
    val responseType = baseResponseType.getOrElse(responseTypesMap.getOrElse(codeAndAction.result, defaultResponseType))

    JsonMarshallers.fromJsonAST(value)(responseType)
  }
}

object ApiMethod {

  case class InvalidToken(token: String) extends ApiResponse.Payload

  case class Forbidden(requiredOperation: String, userId: Int) extends ApiResponse.Payload

}