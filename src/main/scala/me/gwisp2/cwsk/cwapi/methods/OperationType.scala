package me.gwisp2.cwsk.cwapi.methods

object OperationType extends Enumeration {
  type OperationType = OperationType.Value

  val GetUserProfile: OperationType = Value("GetUserProfile")
  val GetStock: OperationType = Value("GetStock")
  val TradeTerminal: OperationType = Value("TradeTerminal")
  val GuildInfo: OperationType = Value("GuildInfo")
}
