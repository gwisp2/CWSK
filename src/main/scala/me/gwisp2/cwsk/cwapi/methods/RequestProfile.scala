package me.gwisp2.cwsk.cwapi.methods

import me.gwisp2.cwsk.cwapi.ApiResponse.Code
import me.gwisp2.cwsk.cwapi.{ApiRequest, ApiResponse}

object RequestProfile extends ApiMethod {
  val action = "requestProfile"
  override val responseTypesMap = Map(
    Code.Ok -> manifest[ApiResponse.Simple[Response]]
  )

  case class Profile(atk: Int, castle: String, `class`: String, `def`: Int, exp: Int, gold: Int,
                     guild: Option[String], guild_tag: Option[String], lvl: Int, mana: Int, pouches: Option[Int],
                     stamina: Int, userName: String)

  case class Response(userId: Int, profile: Profile) extends ApiResponse.Payload

  case class Request() extends ApiRequest.Payload.WithToken {
    def actionName: String = action
  }

}
