package me.gwisp2.cwsk.cwapi.methods

import me.gwisp2.cwsk.cwapi.ApiResponse.Code
import me.gwisp2.cwsk.cwapi.{ApiRequest, ApiResponse}

object RequestStock extends ApiMethod {
  val action = "requestStock"
  override val responseTypesMap = Map(
    Code.Ok -> manifest[ApiResponse.Simple[Response]]
  )

  case class Response(userId: Int, stock: Map[String, Int]) extends ApiResponse.Payload

  case class Request() extends ApiRequest.Payload.WithToken {
    def actionName: String = action
  }

}
