package me.gwisp2.cwsk.cwapi.methods

import me.gwisp2.cwsk.cwapi.ApiResponse.Code
import me.gwisp2.cwsk.cwapi.{ApiRequest, ApiResponse}

object GrantAdditionalOperation extends ApiMethod {
  val action = "grantAdditionalOperation"
  override val responseTypesMap = Map(
    Code.Ok -> manifest[ApiResponse.Simple[Response]]
  )

  case class Response(userId: Int, requestId: String) extends ApiResponse.Payload

  case class Request(requestId: String, authCode: String) extends ApiRequest.Payload.WithToken {
    def actionName: String = action
  }

}
