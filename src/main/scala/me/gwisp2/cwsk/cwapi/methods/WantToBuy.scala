package me.gwisp2.cwsk.cwapi.methods

import me.gwisp2.cwsk.cwapi.ApiResponse.Code
import me.gwisp2.cwsk.cwapi.{ApiRequest, ApiResponse}

object WantToBuy extends ApiMethod {
  val action = "wantToBuy"
  override val responseTypesMap = Map(
    Code.Ok -> manifest[ApiResponse.Simple[Response]]
  )
  override val defaultResponseType = manifest[ApiResponse.Simple[ResponseFail]]

  case class Response(itemName: String, quantity: Int, userId: Int) extends ApiResponse.Payload

  case class ResponseFail(userId: Int) extends ApiResponse.Payload

  case class Request(itemCode: String, quantity: Int, price: Int, exactPrice: Boolean) extends ApiRequest.Payload.WithToken {
    def actionName: String = action
  }
}
