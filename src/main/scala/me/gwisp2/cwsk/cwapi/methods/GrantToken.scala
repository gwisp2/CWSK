package me.gwisp2.cwsk.cwapi.methods

import me.gwisp2.cwsk.cwapi.ApiResponse.Code
import me.gwisp2.cwsk.cwapi.{ApiRequest, ApiResponse}

object GrantToken extends ApiMethod {
  val action = "grantToken"
  override val responseTypesMap = Map(
    Code.Ok -> manifest[ApiResponse.Simple[Response]]
  )

  case class Response(userId: Int, id: String, token: String) extends ApiResponse.Payload

  case class Request(userId: Int, authCode: String) extends ApiRequest.Payload.Simple {
    def actionName: String = action
  }
}
