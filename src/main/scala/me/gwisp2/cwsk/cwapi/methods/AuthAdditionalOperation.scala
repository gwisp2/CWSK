package me.gwisp2.cwsk.cwapi.methods

import me.gwisp2.cwsk.cwapi.ApiResponse.Code
import me.gwisp2.cwsk.cwapi.methods.OperationType.OperationType
import me.gwisp2.cwsk.cwapi.{ApiRequest, ApiResponse}

object AuthAdditionalOperation extends ApiMethod {
  val action = "authAdditionalOperation"
  override val responseTypesMap = Map(
    Code.Ok -> manifest[ApiResponse.WithUUID[Response]]
  )

  case class Response(userId: Int, operation: OperationType) extends ApiResponse.Payload

  case class Request(operation: OperationType) extends ApiRequest.Payload.WithToken {
    def actionName: String = action
  }
}
