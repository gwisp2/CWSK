package me.gwisp2.cwsk.cwapi.methods

import me.gwisp2.cwsk.cwapi.ApiResponse.Code
import me.gwisp2.cwsk.cwapi.{ApiRequest, ApiResponse}

object GetInfo extends ApiMethod {
  val action = "getInfo"

  override val responseTypesMap = Map(
    Code.Ok -> manifest[ApiResponse.Simple[Response]]
  )

  case class Response(balance: Int) extends ApiResponse.Payload

  case class Request() extends ApiRequest.Payload.Simple {
    def actionName: String = action
  }

}
