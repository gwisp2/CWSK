package me.gwisp2.cwsk.cwapi.methods

import me.gwisp2.cwsk.cwapi.ApiResponse.Code
import me.gwisp2.cwsk.cwapi.{ApiRequest, ApiResponse}

object CreateAuthCode extends ApiMethod {
  val action = "createAuthCode"
  override val responseTypesMap = Map(
    Code.Ok -> manifest[ApiResponse.Simple[Response]]
  )

  case class Response(userId: Int) extends ApiResponse.Payload

  case class Request(userId: Int) extends ApiRequest.Payload.Simple {
    def actionName: String = action
  }

}
