package me.gwisp2.cwsk.cwapi

abstract class ApiResponse {
  def actionOption: Option[String]

  val result: ApiResponse.Code
}

object ApiResponse {

  abstract class Payload() {}

  case class Simple[P <: Payload](action: String, result: Code, payload: P) extends ApiResponse {
    def actionOption: Option[String] = Some(action)
  }

  case class WithUUID[P <: Payload](action: String, result: Code, uuid: String, payload: P) extends ApiResponse {
    def actionOption: Option[String] = Some(action)
  }

  case class Unknown(action: Option[String], result: Code) extends ApiResponse {
    def actionOption: Option[String] = action
  }

  case class Code(name: String)

  object Code {
    val Ok = Code("Ok")
    val BadAmount = Code("BadAmount")
    val BadCurrency = Code("BadCurrency")
    val BadFormat = Code("BadFormat")
    val ActionNotFound = Code("ActionNotFound")
    val NoSuchUser = Code("NoSuchUser")
    val NoOffersFoundByPrice = Code("NoOffersFoundByPrice")
    val UserIsBusy = Code("UserIsBusy")
    val NotRegistered = Code("NotRegistered")
    val InvalidCode = Code("InvalidCode")
    val TryAgain = Code("TryAgain")
    val AuthorizationFailed = Code("AuthorizationFailed")
    val InsufficientFunds = Code("InsufficientFunds")
    val InvalidToken = Code("InvalidToken")
    val Forbidden = Code("Forbidden")
  }

}