package me.gwisp2.cwsk.cwapi

import me.gwisp2.cwsk.cwapi.methods._
import org.json4s.JsonAST.{JString, JValue}
import org.json4s.ext.EnumNameSerializer
import org.json4s.jackson.JsonMethods.{compact, parse, render}
import org.json4s.{CustomSerializer, DateFormat, DefaultFormats, Extraction, Formats}

object JsonMarshallers {

  private val formats: Formats = new Formats {
    val dateFormat: DateFormat = DefaultFormats.lossless.dateFormat

    // Throws MappingException if Option[_] cannot be parsed.
    override def strictOptionParsing: Boolean = true
  } + new EnumNameSerializer(OperationType) + new CustomSerializer[ApiResponse.Code]((_: Formats) => ( {
    case JString(value) => ApiResponse.Code(value)
  }, {
    case ApiResponse.Code(value) => JString(value)
  }))

  private val apiMethods = Array(
    AuthAdditionalOperation,
    CreateAuthCode,
    GetInfo,
    GrantAdditionalOperation,
    GrantToken,
    RequestProfile,
    RequestStock,
    WantToBuy
  )

  def toJson[T](t: T): String = {
    implicit val format: Formats = formats
    compact(render(Extraction.decompose(t)))
  }

  def fromJson[T: Manifest](json: String): T = {
    fromJsonAST(parse(json))
  }

  def fromJsonAST[T: Manifest](json: JValue): T = {
    implicit val format: Formats = formats
    json.extract[T]
  }

  def responseFromJson(json: String): ApiResponse = {
    val parsedJson = parse(json)
    val codeAndAction = fromJsonAST[ApiResponse.Unknown](parsedJson)
    codeAndAction.action match {
      case Some(action) =>
        val apiMethodOpt = apiMethods.find(method => method.action == action)
        apiMethodOpt match {
          case Some(apiMethod) => apiMethod.decodeResponse(codeAndAction, parsedJson)
          case _ => codeAndAction
        }
      case _ => codeAndAction
    }
  }
}