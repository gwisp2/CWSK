package me.gwisp2.cwsk.cwapi

import java.nio.charset.StandardCharsets
import java.util.Date

import com.rabbitmq.client._
import com.typesafe.scalalogging.StrictLogging
import javax.net.ssl.SSLContext
import me.gwisp2.cwsk.cwapi.AmpqCwApi.{Credentials, Server}
import me.gwisp2.cwsk.cwapi.messages.{CwApiMessage, DealsPayload}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

class AmpqCwApi(
                 server: Server,
                 credentials: Credentials
               ) extends CwApi with StrictLogging {

  val connection: Future[Connection] = {
    val factory = new ConnectionFactory
    factory.setUsername(credentials.user)
    factory.setPassword(credentials.password)
    factory.setHost(server.host)
    factory.setPort(server.port)
    factory.useSslProtocol(SSLContext.getDefault)
    factory.setAutomaticRecoveryEnabled(true)
    Future {
      factory.newConnection()
    }
  }

  connection.onComplete({
    case Success(conn) =>
      logger.info("Connected to CW API")
      conn match {
        case recoverable: Recoverable => registerRecoveryListener(recoverable)
        case _ =>
      }
    case Failure(e) => logger.error("Error connecting to CW API", e)
  })

  val outChannel: Future[Channel] = connection.map(conn => conn.createChannel())
  val directExchange: Future[String] = declareExchange(credentials.user + "_ex")

  val queueTestPrefix: String = if (credentials.test) "t" else ""
  val inboundQueue: Future[String] = declareQueue(credentials.user + s"_${queueTestPrefix}i")
  val outboundQueue: Future[String] = declareQueue(credentials.user + s"_${queueTestPrefix}o")
  val dealsQueue: Future[String] = declareQueue(credentials.user + s"_deals")

  def sendRequest[P <: ApiRequest.Payload](request: ApiRequest[P]) {
    val requestJson = JsonMarshallers.toJson(request)
    val requestBytes = requestJson.getBytes(StandardCharsets.UTF_8)

    for (channel <- outChannel; outboundQueue <- outboundQueue; directExchange <- directExchange) {
      channel.basicPublish(directExchange, outboundQueue, null, requestBytes)
    }
  }

  def listenForResponses(handler: ApiResponse => Unit) {
    for (conn <- connection;
         channel = conn.createChannel();
         inboundQueue <- inboundQueue) {
      channel.basicConsume(inboundQueue, true, new DefaultConsumer(channel) {
        override def handleCancel(consumerTag: String): Unit = logger.warn("Inbound queue consuming cancelled for external reason")

        override def handleDelivery(consumerTag: String, envelope: Envelope,
                                    properties: AMQP.BasicProperties, body: Array[Byte]): Unit = {
          try {
            val bodyAsString = new String(body, StandardCharsets.UTF_8)
            logger.info(bodyAsString)
            val response = JsonMarshallers.responseFromJson(bodyAsString)
            handler(response)
          } catch {
            case t: Throwable => logger.warn("Exception handling api response", t)
          }
        }
      })
    }
  }

  def listenForDeals(handler: CwApiMessage[DealsPayload] => Unit) {
    for (conn <- connection;
         channel = conn.createChannel();
         dealsQueue <- dealsQueue) {
      channel.basicConsume(dealsQueue, true, new DefaultConsumer(channel) {
        override def handleCancel(consumerTag: String): Unit = logger.warn("Deals queue consuming cancelled for external reason")

        override def handleDelivery(consumerTag: String, envelope: Envelope,
                                    properties: AMQP.BasicProperties, body: Array[Byte]): Unit = {
          try {
            val bodyAsString = new String(body, StandardCharsets.UTF_8)
            val dealsPayload = JsonMarshallers.fromJson[DealsPayload](bodyAsString)
            val timestamp = Option(properties.getTimestamp).getOrElse(new Date())
            handler(CwApiMessage(timestamp, dealsPayload))
          } catch {
            case t: Throwable => logger.warn("Exception handling message from deals queue", t)
          }
        }
      })
    }
  }

  private def declareExchange(name: String): Future[String] = {
    val exchangeDeclare = outChannel.map(c => c.exchangeDeclarePassive(name))
    exchangeDeclare.onComplete {
      case Success(_) => logger.info("Exchange {} found", name)
      case Failure(e) => logger.error("Failed to check for existence of exchange {}", name, e)
    }
    exchangeDeclare.map(_ => name)
  }

  private def declareQueue(name: String): Future[String] = {

    val queueDeclare = outChannel.map(c => c.queueDeclarePassive(name))
    queueDeclare.onComplete {
      case Success(declareOk) => logger.info("Queue {} found. It contains {} messages.", name, declareOk.getMessageCount)
      case Failure(e) => logger.error("Failed to check for existence of queue {}", name, e)
    }
    queueDeclare.map(declareOk => declareOk.getQueue)
  }

  private def registerRecoveryListener(recoverable: Recoverable): Unit = {
    recoverable.addRecoveryListener(new RecoveryListener {
      override def handleRecovery(recoverable: Recoverable): Unit = logger.info("Connection recovery completed")

      override def handleRecoveryStarted(recoverable: Recoverable): Unit = logger.info("Connection recovery started")
    })
  }
}

object AmpqCwApi {

  case class Server(host: String, port: Int, useSsl: Boolean)

  case class Credentials(user: String, password: String, test: Boolean = false)

  object Server {
    val Cw3 = Server("api.chtwrs.com", 5673, useSsl = true)
  }

}
