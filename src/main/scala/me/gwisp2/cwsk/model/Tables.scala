package me.gwisp2.cwsk.model
// AUTO-GENERATED Slick data model
/** Stand-alone Slick data model for immediate use */
object Tables extends {
  val profile = slick.jdbc.MySQLProfile
} with Tables

/** Slick data model trait for extension, choice of backend or usage in the cake pattern. (Make sure to initialize this late.) */
trait Tables {
  val profile: slick.jdbc.JdbcProfile
  import profile.api._
  import slick.model.ForeignKeyAction
  // NOTE: GetResult mappers for plain SQL are only generated for tables where Slick knows how to map the types of all columns.
  import slick.jdbc.{GetResult => GR}

  /** DDL for all tables. Call .create to execute. */
  lazy val schema: profile.SchemaDescription = Array(Chars.schema, CharsEquipment.schema, CwapiTokens.schema, DealsHistory.schema, EquipmentItems.schema, ExpHistory.schema, FlywaySchemaHistory.schema, GoldSinkGuildSettings.schema, GoldSinkRules.schema, GoldSinkUserSettings.schema, GuildRoles.schema, Guilds.schema, GuildStockDiffs.schema, GuildStocks.schema, Items.schema, StockContents.schema, StockSnapshotContents.schema, StockSnapshots.schema, Users.schema, UserSettings.schema).reduceLeft(_ ++ _)
  @deprecated("Use .schema instead of .ddl", "3.0")
  def ddl = schema

  /** Entity class storing rows of table Chars
    *
    * @param id               Database column id SqlType(INT), PrimaryKey
    * @param name             Database column name SqlType(VARCHAR), Length(255,true)
    * @param castle           Database column castle SqlType(VARCHAR), Length(8,true), Default(?)
    * @param gameClass        Database column game_class SqlType(VARCHAR), Length(64,true)
    * @param guildId          Database column guild_id SqlType(INT), Default(None)
    * @param petIcon          Database column pet_icon SqlType(VARCHAR), Length(8,true), Default(None)
    * @param petFullName      Database column pet_full_name SqlType(VARCHAR), Length(60,true), Default(None)
    * @param petLevel         Database column pet_level SqlType(INT), Default(None)
    * @param createdAt        Database column created_at SqlType(TIMESTAMP)
    * @param updatedAt        Database column updated_at SqlType(TIMESTAMP)
    * @param `def`            Database column def SqlType(INT), Default(0)
    * @param atk              Database column atk SqlType(INT), Default(0)
    * @param exp              Database column exp SqlType(INT), Default(1)
    * @param level            Database column level SqlType(INT), Default(1)
    * @param pog              Database column pog SqlType(INT), Default(0)
    * @param equipmentIsValid Database column equipment_is_valid SqlType(BIT), Default(false) */
  case class CharsRow(id: Int, name: String, castle: String = "?", gameClass: String, guildId: Option[Int] = None, petIcon: Option[String] = None, petFullName: Option[String] = None, petLevel: Option[Int] = None, createdAt: java.sql.Timestamp, updatedAt: java.sql.Timestamp, `def`: Int = 0, atk: Int = 0, exp: Int = 1, level: Int = 1, pog: Int = 0, equipmentIsValid: Boolean = false)
  /** GetResult implicit for fetching CharsRow objects using plain SQL queries */
  implicit def GetResultCharsRow(implicit e0: GR[Int], e1: GR[String], e2: GR[Option[Int]], e3: GR[Option[String]], e4: GR[java.sql.Timestamp], e5: GR[Boolean]): GR[CharsRow] = GR {
    prs =>
      import prs._
      CharsRow.tupled((<<[Int], <<[String], <<[String], <<[String], <<?[Int], <<?[String], <<?[String], <<?[Int], <<[java.sql.Timestamp], <<[java.sql.Timestamp], <<[Int], <<[Int], <<[Int], <<[Int], <<[Int], <<[Boolean]))
  }
  /** Table description of table chars. Objects of this class serve as prototypes for rows in queries.
    * NOTE: The following names collided with Scala keywords and were escaped: def */
  class Chars(_tableTag: Tag) extends profile.api.Table[CharsRow](_tableTag, "chars") {
    def * = (id, name, castle, gameClass, guildId, petIcon, petFullName, petLevel, createdAt, updatedAt, `def`, atk, exp, level, pog, equipmentIsValid) <> (CharsRow.tupled, CharsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(name), Rep.Some(castle), Rep.Some(gameClass), guildId, petIcon, petFullName, petLevel, Rep.Some(createdAt), Rep.Some(updatedAt), Rep.Some(`def`), Rep.Some(atk), Rep.Some(exp), Rep.Some(level), Rep.Some(pog), Rep.Some(equipmentIsValid)).shaped.<>({ r => import r._; _1.map(_ => CharsRow.tupled((_1.get, _2.get, _3.get, _4.get, _5, _6, _7, _8, _9.get, _10.get, _11.get, _12.get, _13.get, _14.get, _15.get, _16.get))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(INT), PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.PrimaryKey)
    /** Database column name SqlType(VARCHAR), Length(255,true) */
    val name: Rep[String] = column[String]("name", O.Length(255, varying = true))
    /** Database column castle SqlType(VARCHAR), Length(8,true), Default(?) */
    val castle: Rep[String] = column[String]("castle", O.Length(8, varying = true), O.Default("?"))
    /** Database column game_class SqlType(VARCHAR), Length(64,true) */
    val gameClass: Rep[String] = column[String]("game_class", O.Length(64, varying = true))
    /** Database column guild_id SqlType(INT), Default(None) */
    val guildId: Rep[Option[Int]] = column[Option[Int]]("guild_id", O.Default(None))
    /** Database column pet_icon SqlType(VARCHAR), Length(8,true), Default(None) */
    val petIcon: Rep[Option[String]] = column[Option[String]]("pet_icon", O.Length(8, varying = true), O.Default(None))
    /** Database column pet_full_name SqlType(VARCHAR), Length(60,true), Default(None) */
    val petFullName: Rep[Option[String]] = column[Option[String]]("pet_full_name", O.Length(60, varying = true), O.Default(None))
    /** Database column pet_level SqlType(INT), Default(None) */
    val petLevel: Rep[Option[Int]] = column[Option[Int]]("pet_level", O.Default(None))
    /** Database column created_at SqlType(TIMESTAMP) */
    val createdAt: Rep[java.sql.Timestamp] = column[java.sql.Timestamp]("created_at")
    /** Database column updated_at SqlType(TIMESTAMP) */
    val updatedAt: Rep[java.sql.Timestamp] = column[java.sql.Timestamp]("updated_at")
    /** Database column def SqlType(INT), Default(0)
      * NOTE: The name was escaped because it collided with a Scala keyword. */
    val `def`: Rep[Int] = column[Int]("def", O.Default(0))
    /** Database column atk SqlType(INT), Default(0) */
    val atk: Rep[Int] = column[Int]("atk", O.Default(0))
    /** Database column exp SqlType(INT), Default(1) */
    val exp: Rep[Int] = column[Int]("exp", O.Default(1))
    /** Database column level SqlType(INT), Default(1) */
    val level: Rep[Int] = column[Int]("level", O.Default(1))
    /** Database column pog SqlType(INT), Default(0) */
    val pog: Rep[Int] = column[Int]("pog", O.Default(0))
    /** Database column equipment_is_valid SqlType(BIT), Default(false) */
    val equipmentIsValid: Rep[Boolean] = column[Boolean]("equipment_is_valid", O.Default(false))

    /** Foreign key referencing Guilds (database name chars_guilds_id_fk) */
    lazy val guildsFk = foreignKey("chars_guilds_id_fk", guildId, Guilds)(r => Rep.Some(r.id), onUpdate = ForeignKeyAction.Restrict, onDelete = ForeignKeyAction.Restrict)
    /** Foreign key referencing Users (database name chars_users_id_fk) */
    lazy val usersFk = foreignKey("chars_users_id_fk", id, Users)(r => r.id, onUpdate = ForeignKeyAction.Restrict, onDelete = ForeignKeyAction.Restrict)
  }
  /** Collection-like TableQuery object for table Chars */
  lazy val Chars = new TableQuery(tag => new Chars(tag))

  /** Entity class storing rows of table CharsEquipment
    *
    * @param id        Database column id SqlType(INT), PrimaryKey
    * @param e1        Database column e1 SqlType(INT), Default(None)
    * @param e2        Database column e2 SqlType(INT), Default(None)
    * @param e3        Database column e3 SqlType(INT), Default(None)
    * @param e4        Database column e4 SqlType(INT), Default(None)
    * @param e5        Database column e5 SqlType(INT), Default(None)
    * @param e6        Database column e6 SqlType(INT), Default(None)
    * @param e7        Database column e7 SqlType(INT), Default(None)
    * @param e8        Database column e8 SqlType(INT), Default(None)
    * @param e9        Database column e9 SqlType(INT), Default(None)
    * @param e10       Database column e10 SqlType(INT), Default(None)
    * @param updatedAt Database column updated_at SqlType(TIMESTAMP) */
  case class CharsEquipmentRow(id: Int, e1: Option[Int] = None, e2: Option[Int] = None, e3: Option[Int] = None, e4: Option[Int] = None, e5: Option[Int] = None, e6: Option[Int] = None, e7: Option[Int] = None, e8: Option[Int] = None, e9: Option[Int] = None, e10: Option[Int] = None, updatedAt: java.sql.Timestamp)
  /** GetResult implicit for fetching CharsEquipmentRow objects using plain SQL queries */
  implicit def GetResultCharsEquipmentRow(implicit e0: GR[Int], e1: GR[Option[Int]], e2: GR[java.sql.Timestamp]): GR[CharsEquipmentRow] = GR {
    prs =>
      import prs._
      CharsEquipmentRow.tupled((<<[Int], <<?[Int], <<?[Int], <<?[Int], <<?[Int], <<?[Int], <<?[Int], <<?[Int], <<?[Int], <<?[Int], <<?[Int], <<[java.sql.Timestamp]))
  }
  /** Table description of table chars_equipment. Objects of this class serve as prototypes for rows in queries. */
  class CharsEquipment(_tableTag: Tag) extends profile.api.Table[CharsEquipmentRow](_tableTag, "chars_equipment") {
    def * = (id, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, updatedAt) <> (CharsEquipmentRow.tupled, CharsEquipmentRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, Rep.Some(updatedAt)).shaped.<>({ r => import r._; _1.map(_ => CharsEquipmentRow.tupled((_1.get, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12.get))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(INT), PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.PrimaryKey)
    /** Database column e1 SqlType(INT), Default(None) */
    val e1: Rep[Option[Int]] = column[Option[Int]]("e1", O.Default(None))
    /** Database column e2 SqlType(INT), Default(None) */
    val e2: Rep[Option[Int]] = column[Option[Int]]("e2", O.Default(None))
    /** Database column e3 SqlType(INT), Default(None) */
    val e3: Rep[Option[Int]] = column[Option[Int]]("e3", O.Default(None))
    /** Database column e4 SqlType(INT), Default(None) */
    val e4: Rep[Option[Int]] = column[Option[Int]]("e4", O.Default(None))
    /** Database column e5 SqlType(INT), Default(None) */
    val e5: Rep[Option[Int]] = column[Option[Int]]("e5", O.Default(None))
    /** Database column e6 SqlType(INT), Default(None) */
    val e6: Rep[Option[Int]] = column[Option[Int]]("e6", O.Default(None))
    /** Database column e7 SqlType(INT), Default(None) */
    val e7: Rep[Option[Int]] = column[Option[Int]]("e7", O.Default(None))
    /** Database column e8 SqlType(INT), Default(None) */
    val e8: Rep[Option[Int]] = column[Option[Int]]("e8", O.Default(None))
    /** Database column e9 SqlType(INT), Default(None) */
    val e9: Rep[Option[Int]] = column[Option[Int]]("e9", O.Default(None))
    /** Database column e10 SqlType(INT), Default(None) */
    val e10: Rep[Option[Int]] = column[Option[Int]]("e10", O.Default(None))
    /** Database column updated_at SqlType(TIMESTAMP) */
    val updatedAt: Rep[java.sql.Timestamp] = column[java.sql.Timestamp]("updated_at")

    /** Foreign key referencing Chars (database name chars_equipment_chars_id_fk) */
    lazy val charsFk = foreignKey("chars_equipment_chars_id_fk", id, Chars)(r => r.id, onUpdate = ForeignKeyAction.Restrict, onDelete = ForeignKeyAction.Restrict)
    /** Foreign key referencing EquipmentItems (database name chars_equipment_equipment_items_id10_fk) */
    lazy val equipmentItemsFk2 = foreignKey("chars_equipment_equipment_items_id10_fk", e10, EquipmentItems)(r => Rep.Some(r.id), onUpdate = ForeignKeyAction.Restrict, onDelete = ForeignKeyAction.Restrict)
    /** Foreign key referencing EquipmentItems (database name chars_equipment_equipment_items_id1_fk) */
    lazy val equipmentItemsFk3 = foreignKey("chars_equipment_equipment_items_id1_fk", e1, EquipmentItems)(r => Rep.Some(r.id), onUpdate = ForeignKeyAction.Restrict, onDelete = ForeignKeyAction.Restrict)
    /** Foreign key referencing EquipmentItems (database name chars_equipment_equipment_items_id2_fk) */
    lazy val equipmentItemsFk4 = foreignKey("chars_equipment_equipment_items_id2_fk", e2, EquipmentItems)(r => Rep.Some(r.id), onUpdate = ForeignKeyAction.Restrict, onDelete = ForeignKeyAction.Restrict)
    /** Foreign key referencing EquipmentItems (database name chars_equipment_equipment_items_id3_fk) */
    lazy val equipmentItemsFk5 = foreignKey("chars_equipment_equipment_items_id3_fk", e3, EquipmentItems)(r => Rep.Some(r.id), onUpdate = ForeignKeyAction.Restrict, onDelete = ForeignKeyAction.Restrict)
    /** Foreign key referencing EquipmentItems (database name chars_equipment_equipment_items_id4_fk) */
    lazy val equipmentItemsFk6 = foreignKey("chars_equipment_equipment_items_id4_fk", e4, EquipmentItems)(r => Rep.Some(r.id), onUpdate = ForeignKeyAction.Restrict, onDelete = ForeignKeyAction.Restrict)
    /** Foreign key referencing EquipmentItems (database name chars_equipment_equipment_items_id5_fk) */
    lazy val equipmentItemsFk7 = foreignKey("chars_equipment_equipment_items_id5_fk", e5, EquipmentItems)(r => Rep.Some(r.id), onUpdate = ForeignKeyAction.Restrict, onDelete = ForeignKeyAction.Restrict)
    /** Foreign key referencing EquipmentItems (database name chars_equipment_equipment_items_id6_fk) */
    lazy val equipmentItemsFk8 = foreignKey("chars_equipment_equipment_items_id6_fk", e6, EquipmentItems)(r => Rep.Some(r.id), onUpdate = ForeignKeyAction.Restrict, onDelete = ForeignKeyAction.Restrict)
    /** Foreign key referencing EquipmentItems (database name chars_equipment_equipment_items_id7_fk) */
    lazy val equipmentItemsFk9 = foreignKey("chars_equipment_equipment_items_id7_fk", e7, EquipmentItems)(r => Rep.Some(r.id), onUpdate = ForeignKeyAction.Restrict, onDelete = ForeignKeyAction.Restrict)
    /** Foreign key referencing EquipmentItems (database name chars_equipment_equipment_items_id8_fk) */
    lazy val equipmentItemsFk10 = foreignKey("chars_equipment_equipment_items_id8_fk", e8, EquipmentItems)(r => Rep.Some(r.id), onUpdate = ForeignKeyAction.Restrict, onDelete = ForeignKeyAction.Restrict)
    /** Foreign key referencing EquipmentItems (database name chars_equipment_equipment_items_id9_fk) */
    lazy val equipmentItemsFk11 = foreignKey("chars_equipment_equipment_items_id9_fk", e9, EquipmentItems)(r => Rep.Some(r.id), onUpdate = ForeignKeyAction.Restrict, onDelete = ForeignKeyAction.Restrict)
  }
  /** Collection-like TableQuery object for table CharsEquipment */
  lazy val CharsEquipment = new TableQuery(tag => new CharsEquipment(tag))

  /** Entity class storing rows of table CwapiTokens
    *
    * @param userId            Database column user_id SqlType(INT), PrimaryKey
    * @param cwId              Database column cw_id SqlType(VARCHAR), Length(64,true)
    * @param token             Database column token SqlType(VARCHAR), Length(64,true)
    * @param allowedGetStock   Database column allowed_get_stock SqlType(BIT), Default(false)
    * @param allowedWtb        Database column allowed_wtb SqlType(BIT), Default(false)
    * @param allowedGetProfile Database column allowed_get_profile SqlType(BIT), Default(false)
    * @param allowedGuildInfo  Database column allowed_guild_info SqlType(BIT), Default(false) */
  case class CwapiTokensRow(userId: Int, cwId: String, token: String, allowedGetStock: Boolean = false, allowedWtb: Boolean = false, allowedGetProfile: Boolean = false, allowedGuildInfo: Boolean = false)
  /** GetResult implicit for fetching CwapiTokensRow objects using plain SQL queries */
  implicit def GetResultCwapiTokensRow(implicit e0: GR[Int], e1: GR[String], e2: GR[Boolean]): GR[CwapiTokensRow] = GR {
    prs =>
      import prs._
      CwapiTokensRow.tupled((<<[Int], <<[String], <<[String], <<[Boolean], <<[Boolean], <<[Boolean], <<[Boolean]))
  }
  /** Table description of table cwapi_tokens. Objects of this class serve as prototypes for rows in queries. */
  class CwapiTokens(_tableTag: Tag) extends profile.api.Table[CwapiTokensRow](_tableTag, "cwapi_tokens") {
    def * = (userId, cwId, token, allowedGetStock, allowedWtb, allowedGetProfile, allowedGuildInfo) <> (CwapiTokensRow.tupled, CwapiTokensRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(userId), Rep.Some(cwId), Rep.Some(token), Rep.Some(allowedGetStock), Rep.Some(allowedWtb), Rep.Some(allowedGetProfile), Rep.Some(allowedGuildInfo)).shaped.<>({ r => import r._; _1.map(_ => CwapiTokensRow.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get, _7.get))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column user_id SqlType(INT), PrimaryKey */
    val userId: Rep[Int] = column[Int]("user_id", O.PrimaryKey)
    /** Database column cw_id SqlType(VARCHAR), Length(64,true) */
    val cwId: Rep[String] = column[String]("cw_id", O.Length(64, varying=true))
    /** Database column token SqlType(VARCHAR), Length(64,true) */
    val token: Rep[String] = column[String]("token", O.Length(64, varying=true))
    /** Database column allowed_get_stock SqlType(BIT), Default(false) */
    val allowedGetStock: Rep[Boolean] = column[Boolean]("allowed_get_stock", O.Default(false))
    /** Database column allowed_wtb SqlType(BIT), Default(false) */
    val allowedWtb: Rep[Boolean] = column[Boolean]("allowed_wtb", O.Default(false))
    /** Database column allowed_get_profile SqlType(BIT), Default(false) */
    val allowedGetProfile: Rep[Boolean] = column[Boolean]("allowed_get_profile", O.Default(false))
    /** Database column allowed_guild_info SqlType(BIT), Default(false) */
    val allowedGuildInfo: Rep[Boolean] = column[Boolean]("allowed_guild_info", O.Default(false))
  }
  /** Collection-like TableQuery object for table CwapiTokens */
  lazy val CwapiTokens = new TableQuery(tag => new CwapiTokens(tag))

  /** Entity class storing rows of table DealsHistory
    *
    * @param sellerId  Database column seller_id SqlType(VARCHAR), Length(32,true)
    * @param buyerId   Database column buyer_id SqlType(VARCHAR), Length(32,true)
    * @param itemId    Database column item_id SqlType(INT)
    * @param qty       Database column qty SqlType(INT)
    * @param price     Database column price SqlType(INT)
    * @param timestamp Database column timestamp SqlType(TIMESTAMP) */
  case class DealsHistoryRow(sellerId: String, buyerId: String, itemId: Int, qty: Int, price: Int, timestamp: java.sql.Timestamp)
  /** GetResult implicit for fetching DealsHistoryRow objects using plain SQL queries */
  implicit def GetResultDealsHistoryRow(implicit e0: GR[String], e1: GR[Int], e2: GR[java.sql.Timestamp]): GR[DealsHistoryRow] = GR {
    prs =>
      import prs._
      DealsHistoryRow.tupled((<<[String], <<[String], <<[Int], <<[Int], <<[Int], <<[java.sql.Timestamp]))
  }
  /** Table description of table deals_history. Objects of this class serve as prototypes for rows in queries. */
  class DealsHistory(_tableTag: Tag) extends profile.api.Table[DealsHistoryRow](_tableTag, "deals_history") {
    def * = (sellerId, buyerId, itemId, qty, price, timestamp) <> (DealsHistoryRow.tupled, DealsHistoryRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(sellerId), Rep.Some(buyerId), Rep.Some(itemId), Rep.Some(qty), Rep.Some(price), Rep.Some(timestamp)).shaped.<>({ r => import r._; _1.map(_ => DealsHistoryRow.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column seller_id SqlType(VARCHAR), Length(32,true) */
    val sellerId: Rep[String] = column[String]("seller_id", O.Length(32, varying=true))
    /** Database column buyer_id SqlType(VARCHAR), Length(32,true) */
    val buyerId: Rep[String] = column[String]("buyer_id", O.Length(32, varying=true))
    /** Database column item_id SqlType(INT) */
    val itemId: Rep[Int] = column[Int]("item_id")
    /** Database column qty SqlType(INT) */
    val qty: Rep[Int] = column[Int]("qty")
    /** Database column price SqlType(INT) */
    val price: Rep[Int] = column[Int]("price")
    /** Database column timestamp SqlType(TIMESTAMP) */
    val timestamp: Rep[java.sql.Timestamp] = column[java.sql.Timestamp]("timestamp")

    /** Foreign key referencing Items (database name deals_history_item_id_fk) */
    lazy val itemsFk = foreignKey("deals_history_item_id_fk", itemId, Items)(r => r.id, onUpdate = ForeignKeyAction.Restrict, onDelete = ForeignKeyAction.Restrict)
  }
  /** Collection-like TableQuery object for table DealsHistory */
  lazy val DealsHistory = new TableQuery(tag => new DealsHistory(tag))

  /** Entity class storing rows of table EquipmentItems
    *
    * @param id               Database column id SqlType(INT), AutoInc, PrimaryKey
    * @param line             Database column line SqlType(VARCHAR), Length(256,true)
    * @param name             Database column name SqlType(VARCHAR), Length(256,true)
    * @param enchantmentLevel Database column enchantment_level SqlType(INT), Default(0)
    * @param atkBonus         Database column atk_bonus SqlType(INT), Default(0)
    * @param defBonus         Database column def_bonus SqlType(INT), Default(0)
    * @param staminaBonus     Database column stamina_bonus SqlType(INT), Default(0)
    * @param bagBonus         Database column bag_bonus SqlType(INT), Default(0)
    * @param slotId           Database column slot_id SqlType(INT), Default(None) */
  case class EquipmentItemsRow(id: Int, line: String, name: String, enchantmentLevel: Int = 0, atkBonus: Int = 0, defBonus: Int = 0, staminaBonus: Int = 0, bagBonus: Int = 0, slotId: Option[Int] = None)
  /** GetResult implicit for fetching EquipmentItemsRow objects using plain SQL queries */
  implicit def GetResultEquipmentItemsRow(implicit e0: GR[Int], e1: GR[String], e2: GR[Option[Int]]): GR[EquipmentItemsRow] = GR {
    prs =>
      import prs._
      EquipmentItemsRow.tupled((<<[Int], <<[String], <<[String], <<[Int], <<[Int], <<[Int], <<[Int], <<[Int], <<?[Int]))
  }
  /** Table description of table equipment_items. Objects of this class serve as prototypes for rows in queries. */
  class EquipmentItems(_tableTag: Tag) extends profile.api.Table[EquipmentItemsRow](_tableTag, "equipment_items") {
    def * = (id, line, name, enchantmentLevel, atkBonus, defBonus, staminaBonus, bagBonus, slotId) <> (EquipmentItemsRow.tupled, EquipmentItemsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(line), Rep.Some(name), Rep.Some(enchantmentLevel), Rep.Some(atkBonus), Rep.Some(defBonus), Rep.Some(staminaBonus), Rep.Some(bagBonus), slotId).shaped.<>({ r => import r._; _1.map(_ => EquipmentItemsRow.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get, _7.get, _8.get, _9))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(INT), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column line SqlType(VARCHAR), Length(256,true) */
    val line: Rep[String] = column[String]("line", O.Length(256,varying=true))
    /** Database column name SqlType(VARCHAR), Length(256,true) */
    val name: Rep[String] = column[String]("name", O.Length(256,varying=true))
    /** Database column enchantment_level SqlType(INT), Default(0) */
    val enchantmentLevel: Rep[Int] = column[Int]("enchantment_level", O.Default(0))
    /** Database column atk_bonus SqlType(INT), Default(0) */
    val atkBonus: Rep[Int] = column[Int]("atk_bonus", O.Default(0))
    /** Database column def_bonus SqlType(INT), Default(0) */
    val defBonus: Rep[Int] = column[Int]("def_bonus", O.Default(0))
    /** Database column stamina_bonus SqlType(INT), Default(0) */
    val staminaBonus: Rep[Int] = column[Int]("stamina_bonus", O.Default(0))
    /** Database column bag_bonus SqlType(INT), Default(0) */
    val bagBonus: Rep[Int] = column[Int]("bag_bonus", O.Default(0))
    /** Database column slot_id SqlType(INT), Default(None) */
    val slotId: Rep[Option[Int]] = column[Option[Int]]("slot_id", O.Default(None))
  }
  /** Collection-like TableQuery object for table EquipmentItems */
  lazy val EquipmentItems = new TableQuery(tag => new EquipmentItems(tag))

  /** Entity class storing rows of table ExpHistory
    *
    * @param userId Database column user_id SqlType(INT)
    * @param exp    Database column exp SqlType(INT)
    * @param at     Database column at SqlType(TIMESTAMP) */
  case class ExpHistoryRow(userId: Int, exp: Int, at: java.sql.Timestamp)
  /** GetResult implicit for fetching ExpHistoryRow objects using plain SQL queries */
  implicit def GetResultExpHistoryRow(implicit e0: GR[Int], e1: GR[java.sql.Timestamp]): GR[ExpHistoryRow] = GR {
    prs =>
      import prs._
      ExpHistoryRow.tupled((<<[Int], <<[Int], <<[java.sql.Timestamp]))
  }
  /** Table description of table exp_history. Objects of this class serve as prototypes for rows in queries. */
  class ExpHistory(_tableTag: Tag) extends profile.api.Table[ExpHistoryRow](_tableTag, "exp_history") {
    def * = (userId, exp, at) <> (ExpHistoryRow.tupled, ExpHistoryRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(userId), Rep.Some(exp), Rep.Some(at)).shaped.<>({ r => import r._; _1.map(_ => ExpHistoryRow.tupled((_1.get, _2.get, _3.get))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column user_id SqlType(INT) */
    val userId: Rep[Int] = column[Int]("user_id")
    /** Database column exp SqlType(INT) */
    val exp: Rep[Int] = column[Int]("exp")
    /** Database column at SqlType(TIMESTAMP) */
    val at: Rep[java.sql.Timestamp] = column[java.sql.Timestamp]("at")

    /** Index over (userId,at) (database name exp_history_index) */
    val index1 = index("exp_history_index", (userId, at))
  }
  /** Collection-like TableQuery object for table ExpHistory */
  lazy val ExpHistory = new TableQuery(tag => new ExpHistory(tag))

  /** Entity class storing rows of table FlywaySchemaHistory
    *
    * @param installedRank Database column installed_rank SqlType(INT), PrimaryKey
    * @param version       Database column version SqlType(VARCHAR), Length(50,true), Default(None)
    * @param description   Database column description SqlType(VARCHAR), Length(200,true)
    * @param `type`        Database column type SqlType(VARCHAR), Length(20,true)
    * @param script        Database column script SqlType(VARCHAR), Length(1000,true)
    * @param checksum      Database column checksum SqlType(INT), Default(None)
    * @param installedBy   Database column installed_by SqlType(VARCHAR), Length(100,true)
    * @param installedOn   Database column installed_on SqlType(TIMESTAMP)
    * @param executionTime Database column execution_time SqlType(INT)
    * @param success       Database column success SqlType(BIT) */
  case class FlywaySchemaHistoryRow(installedRank: Int, version: Option[String] = None, description: String, `type`: String, script: String, checksum: Option[Int] = None, installedBy: String, installedOn: java.sql.Timestamp, executionTime: Int, success: Boolean)
  /** GetResult implicit for fetching FlywaySchemaHistoryRow objects using plain SQL queries */
  implicit def GetResultFlywaySchemaHistoryRow(implicit e0: GR[Int], e1: GR[Option[String]], e2: GR[String], e3: GR[Option[Int]], e4: GR[java.sql.Timestamp], e5: GR[Boolean]): GR[FlywaySchemaHistoryRow] = GR {
    prs =>
      import prs._
      FlywaySchemaHistoryRow.tupled((<<[Int], <<?[String], <<[String], <<[String], <<[String], <<?[Int], <<[String], <<[java.sql.Timestamp], <<[Int], <<[Boolean]))
  }
  /** Table description of table flyway_schema_history. Objects of this class serve as prototypes for rows in queries.
    * NOTE: The following names collided with Scala keywords and were escaped: type */
  class FlywaySchemaHistory(_tableTag: Tag) extends profile.api.Table[FlywaySchemaHistoryRow](_tableTag, "flyway_schema_history") {
    def * = (installedRank, version, description, `type`, script, checksum, installedBy, installedOn, executionTime, success) <> (FlywaySchemaHistoryRow.tupled, FlywaySchemaHistoryRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(installedRank), version, Rep.Some(description), Rep.Some(`type`), Rep.Some(script), checksum, Rep.Some(installedBy), Rep.Some(installedOn), Rep.Some(executionTime), Rep.Some(success)).shaped.<>({ r => import r._; _1.map(_ => FlywaySchemaHistoryRow.tupled((_1.get, _2, _3.get, _4.get, _5.get, _6, _7.get, _8.get, _9.get, _10.get))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column installed_rank SqlType(INT), PrimaryKey */
    val installedRank: Rep[Int] = column[Int]("installed_rank", O.PrimaryKey)
    /** Database column version SqlType(VARCHAR), Length(50,true), Default(None) */
    val version: Rep[Option[String]] = column[Option[String]]("version", O.Length(50, varying = true), O.Default(None))
    /** Database column description SqlType(VARCHAR), Length(200,true) */
    val description: Rep[String] = column[String]("description", O.Length(200,varying=true))
    /** Database column type SqlType(VARCHAR), Length(20,true)
      * NOTE: The name was escaped because it collided with a Scala keyword. */
    val `type`: Rep[String] = column[String]("type", O.Length(20,varying=true))
    /** Database column script SqlType(VARCHAR), Length(1000,true) */
    val script: Rep[String] = column[String]("script", O.Length(1000,varying=true))
    /** Database column checksum SqlType(INT), Default(None) */
    val checksum: Rep[Option[Int]] = column[Option[Int]]("checksum", O.Default(None))
    /** Database column installed_by SqlType(VARCHAR), Length(100,true) */
    val installedBy: Rep[String] = column[String]("installed_by", O.Length(100,varying=true))
    /** Database column installed_on SqlType(TIMESTAMP) */
    val installedOn: Rep[java.sql.Timestamp] = column[java.sql.Timestamp]("installed_on")
    /** Database column execution_time SqlType(INT) */
    val executionTime: Rep[Int] = column[Int]("execution_time")
    /** Database column success SqlType(BIT) */
    val success: Rep[Boolean] = column[Boolean]("success")

    /** Index over (success) (database name flyway_schema_history_s_idx) */
    val index1 = index("flyway_schema_history_s_idx", success)
  }
  /** Collection-like TableQuery object for table FlywaySchemaHistory */
  lazy val FlywaySchemaHistory = new TableQuery(tag => new FlywaySchemaHistory(tag))

  /** Entity class storing rows of table GoldSinkGuildSettings
    *
    * @param guildId    Database column guild_id SqlType(INT), PrimaryKey
    * @param sinkRuleId Database column sink_rule_id SqlType(INT), Default(None) */
  case class GoldSinkGuildSettingsRow(guildId: Int, sinkRuleId: Option[Int] = None)
  /** GetResult implicit for fetching GoldSinkGuildSettingsRow objects using plain SQL queries */
  implicit def GetResultGoldSinkGuildSettingsRow(implicit e0: GR[Int], e1: GR[Option[Int]]): GR[GoldSinkGuildSettingsRow] = GR {
    prs =>
      import prs._
      GoldSinkGuildSettingsRow.tupled((<<[Int], <<?[Int]))
  }
  /** Table description of table gold_sink_guild_settings. Objects of this class serve as prototypes for rows in queries. */
  class GoldSinkGuildSettings(_tableTag: Tag) extends profile.api.Table[GoldSinkGuildSettingsRow](_tableTag, "gold_sink_guild_settings") {
    def * = (guildId, sinkRuleId) <> (GoldSinkGuildSettingsRow.tupled, GoldSinkGuildSettingsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(guildId), sinkRuleId).shaped.<>({ r => import r._; _1.map(_ => GoldSinkGuildSettingsRow.tupled((_1.get, _2))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column guild_id SqlType(INT), PrimaryKey */
    val guildId: Rep[Int] = column[Int]("guild_id", O.PrimaryKey)
    /** Database column sink_rule_id SqlType(INT), Default(None) */
    val sinkRuleId: Rep[Option[Int]] = column[Option[Int]]("sink_rule_id", O.Default(None))

    /** Foreign key referencing GoldSinkRules (database name gold_sink_guild_settings_gold_sink_rules_id_fk) */
    lazy val goldSinkRulesFk = foreignKey("gold_sink_guild_settings_gold_sink_rules_id_fk", sinkRuleId, GoldSinkRules)(r => Rep.Some(r.id), onUpdate = ForeignKeyAction.Restrict, onDelete = ForeignKeyAction.Restrict)
    /** Foreign key referencing Guilds (database name gold_sink_guild_settings_guilds_id_fk) */
    lazy val guildsFk = foreignKey("gold_sink_guild_settings_guilds_id_fk", guildId, Guilds)(r => r.id, onUpdate = ForeignKeyAction.Restrict, onDelete = ForeignKeyAction.Restrict)
  }
  /** Collection-like TableQuery object for table GoldSinkGuildSettings */
  lazy val GoldSinkGuildSettings = new TableQuery(tag => new GoldSinkGuildSettings(tag))

  /** Entity class storing rows of table GoldSinkRules
    *
    * @param id   Database column id SqlType(INT), AutoInc, PrimaryKey
    * @param rule Database column rule SqlType(VARCHAR), Length(2000,true) */
  case class GoldSinkRulesRow(id: Int, rule: String)
  /** GetResult implicit for fetching GoldSinkRulesRow objects using plain SQL queries */
  implicit def GetResultGoldSinkRulesRow(implicit e0: GR[Int], e1: GR[String]): GR[GoldSinkRulesRow] = GR {
    prs =>
      import prs._
      GoldSinkRulesRow.tupled((<<[Int], <<[String]))
  }
  /** Table description of table gold_sink_rules. Objects of this class serve as prototypes for rows in queries. */
  class GoldSinkRules(_tableTag: Tag) extends profile.api.Table[GoldSinkRulesRow](_tableTag, "gold_sink_rules") {
    def * = (id, rule) <> (GoldSinkRulesRow.tupled, GoldSinkRulesRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(rule)).shaped.<>({ r => import r._; _1.map(_ => GoldSinkRulesRow.tupled((_1.get, _2.get))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(INT), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column rule SqlType(VARCHAR), Length(2000,true) */
    val rule: Rep[String] = column[String]("rule", O.Length(2000,varying=true))
  }
  /** Collection-like TableQuery object for table GoldSinkRules */
  lazy val GoldSinkRules = new TableQuery(tag => new GoldSinkRules(tag))

  /** Entity class storing rows of table GoldSinkUserSettings
    *
    * @param userId           Database column user_id SqlType(INT), PrimaryKey
    * @param sinkRuleId       Database column sink_rule_id SqlType(INT), Default(None)
    * @param autosinkEnabled  Database column autosink_enabled SqlType(BIT), Default(false)
    * @param goldSinkLimit    Database column gold_sink_limit SqlType(INT), Default(None)
    * @param goldSinkTimeslot Database column gold_sink_timeslot SqlType(INT), Default(None) */
  case class GoldSinkUserSettingsRow(userId: Int, sinkRuleId: Option[Int] = None, autosinkEnabled: Boolean = false, goldSinkLimit: Option[Int] = None, goldSinkTimeslot: Option[Int] = None)
  /** GetResult implicit for fetching GoldSinkUserSettingsRow objects using plain SQL queries */
  implicit def GetResultGoldSinkUserSettingsRow(implicit e0: GR[Int], e1: GR[Option[Int]], e2: GR[Boolean]): GR[GoldSinkUserSettingsRow] = GR {
    prs =>
      import prs._
      GoldSinkUserSettingsRow.tupled((<<[Int], <<?[Int], <<[Boolean], <<?[Int], <<?[Int]))
  }
  /** Table description of table gold_sink_user_settings. Objects of this class serve as prototypes for rows in queries. */
  class GoldSinkUserSettings(_tableTag: Tag) extends profile.api.Table[GoldSinkUserSettingsRow](_tableTag, "gold_sink_user_settings") {
    def * = (userId, sinkRuleId, autosinkEnabled, goldSinkLimit, goldSinkTimeslot) <> (GoldSinkUserSettingsRow.tupled, GoldSinkUserSettingsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(userId), sinkRuleId, Rep.Some(autosinkEnabled), goldSinkLimit, goldSinkTimeslot).shaped.<>({ r => import r._; _1.map(_ => GoldSinkUserSettingsRow.tupled((_1.get, _2, _3.get, _4, _5))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column user_id SqlType(INT), PrimaryKey */
    val userId: Rep[Int] = column[Int]("user_id", O.PrimaryKey)
    /** Database column sink_rule_id SqlType(INT), Default(None) */
    val sinkRuleId: Rep[Option[Int]] = column[Option[Int]]("sink_rule_id", O.Default(None))
    /** Database column autosink_enabled SqlType(BIT), Default(false) */
    val autosinkEnabled: Rep[Boolean] = column[Boolean]("autosink_enabled", O.Default(false))
    /** Database column gold_sink_limit SqlType(INT), Default(None) */
    val goldSinkLimit: Rep[Option[Int]] = column[Option[Int]]("gold_sink_limit", O.Default(None))
    /** Database column gold_sink_timeslot SqlType(INT), Default(None) */
    val goldSinkTimeslot: Rep[Option[Int]] = column[Option[Int]]("gold_sink_timeslot", O.Default(None))

    /** Foreign key referencing GoldSinkRules (database name gold_sink_user_settings_gold_sink_rules_id_fk) */
    lazy val goldSinkRulesFk = foreignKey("gold_sink_user_settings_gold_sink_rules_id_fk", sinkRuleId, GoldSinkRules)(r => Rep.Some(r.id), onUpdate = ForeignKeyAction.Restrict, onDelete=ForeignKeyAction.Restrict)
  }
  /** Collection-like TableQuery object for table GoldSinkUserSettings */
  lazy val GoldSinkUserSettings = new TableQuery(tag => new GoldSinkUserSettings(tag))

  /** Entity class storing rows of table GuildRoles
    *
    * @param guildId            Database column guild_id SqlType(INT)
    * @param userId             Database column user_id SqlType(INT)
    * @param isLeader           Database column is_leader SqlType(BIT), Default(false)
    * @param permissionsBitmask Database column permissions_bitmask SqlType(INT), Default(0) */
  case class GuildRolesRow(guildId: Int, userId: Int, isLeader: Boolean = false, permissionsBitmask: Int = 0)
  /** GetResult implicit for fetching GuildRolesRow objects using plain SQL queries */
  implicit def GetResultGuildRolesRow(implicit e0: GR[Int], e1: GR[Boolean]): GR[GuildRolesRow] = GR {
    prs =>
      import prs._
      GuildRolesRow.tupled((<<[Int], <<[Int], <<[Boolean], <<[Int]))
  }
  /** Table description of table guild_roles. Objects of this class serve as prototypes for rows in queries. */
  class GuildRoles(_tableTag: Tag) extends profile.api.Table[GuildRolesRow](_tableTag, "guild_roles") {
    def * = (guildId, userId, isLeader, permissionsBitmask) <> (GuildRolesRow.tupled, GuildRolesRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(guildId), Rep.Some(userId), Rep.Some(isLeader), Rep.Some(permissionsBitmask)).shaped.<>({ r => import r._; _1.map(_ => GuildRolesRow.tupled((_1.get, _2.get, _3.get, _4.get))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column guild_id SqlType(INT) */
    val guildId: Rep[Int] = column[Int]("guild_id")
    /** Database column user_id SqlType(INT) */
    val userId: Rep[Int] = column[Int]("user_id")
    /** Database column is_leader SqlType(BIT), Default(false) */
    val isLeader: Rep[Boolean] = column[Boolean]("is_leader", O.Default(false))
    /** Database column permissions_bitmask SqlType(INT), Default(0) */
    val permissionsBitmask: Rep[Int] = column[Int]("permissions_bitmask", O.Default(0))

    /** Primary key of GuildRoles (database name guild_roles_PK) */
    val pk = primaryKey("guild_roles_PK", (guildId, userId))

    /** Foreign key referencing Chars (database name guild_roles_chars_id_fk) */
    lazy val charsFk = foreignKey("guild_roles_chars_id_fk", userId, Chars)(r => r.id, onUpdate = ForeignKeyAction.Restrict, onDelete=ForeignKeyAction.Restrict)
    /** Foreign key referencing Guilds (database name guild_roles_guilds_id_fk) */
    lazy val guildsFk = foreignKey("guild_roles_guilds_id_fk", guildId, Guilds)(r => r.id, onUpdate = ForeignKeyAction.Restrict, onDelete=ForeignKeyAction.Restrict)
  }
  /** Collection-like TableQuery object for table GuildRoles */
  lazy val GuildRoles = new TableQuery(tag => new GuildRoles(tag))

  /** Entity class storing rows of table Guilds
    *
    * @param id     Database column id SqlType(INT), AutoInc, PrimaryKey
    * @param name   Database column name SqlType(VARCHAR), Length(64,true)
    * @param tag    Database column tag SqlType(VARCHAR), Length(8,true), Default(None)
    * @param castle Database column castle SqlType(VARCHAR), Length(8,true), Default(?) */
  case class GuildsRow(id: Int, name: String, tag: Option[String] = None, castle: String = "?")
  /** GetResult implicit for fetching GuildsRow objects using plain SQL queries */
  implicit def GetResultGuildsRow(implicit e0: GR[Int], e1: GR[String], e2: GR[Option[String]]): GR[GuildsRow] = GR {
    prs =>
      import prs._
      GuildsRow.tupled((<<[Int], <<[String], <<?[String], <<[String]))
  }
  /** Table description of table guilds. Objects of this class serve as prototypes for rows in queries. */
  class Guilds(_tableTag: Tag) extends profile.api.Table[GuildsRow](_tableTag, "guilds") {
    def * = (id, name, tag, castle) <> (GuildsRow.tupled, GuildsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(name), tag, Rep.Some(castle)).shaped.<>({ r => import r._; _1.map(_ => GuildsRow.tupled((_1.get, _2.get, _3, _4.get))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(INT), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column name SqlType(VARCHAR), Length(64,true) */
    val name: Rep[String] = column[String]("name", O.Length(64,varying=true))
    /** Database column tag SqlType(VARCHAR), Length(8,true), Default(None) */
    val tag: Rep[Option[String]] = column[Option[String]]("tag", O.Length(8,varying=true), O.Default(None))
    /** Database column castle SqlType(VARCHAR), Length(8,true), Default(?) */
    val castle: Rep[String] = column[String]("castle", O.Length(8,varying=true), O.Default("?"))

    /** Uniqueness Index over (tag) (database name guilds_tag_uindex) */
    val index1 = index("guilds_tag_uindex", tag, unique=true)
  }
  /** Collection-like TableQuery object for table Guilds */
  lazy val Guilds = new TableQuery(tag => new Guilds(tag))

  /** Entity class storing rows of table GuildStockDiffs
    *
    * @param id              Database column id SqlType(INT), AutoInc, PrimaryKey
    * @param guildId         Database column guild_id SqlType(INT)
    * @param deltaSnapshotId Database column delta_snapshot_id SqlType(INT) */
  case class GuildStockDiffsRow(id: Int, guildId: Int, deltaSnapshotId: Int)
  /** GetResult implicit for fetching GuildStockDiffsRow objects using plain SQL queries */
  implicit def GetResultGuildStockDiffsRow(implicit e0: GR[Int]): GR[GuildStockDiffsRow] = GR {
    prs =>
      import prs._
      GuildStockDiffsRow.tupled((<<[Int], <<[Int], <<[Int]))
  }
  /** Table description of table guild_stock_diffs. Objects of this class serve as prototypes for rows in queries. */
  class GuildStockDiffs(_tableTag: Tag) extends profile.api.Table[GuildStockDiffsRow](_tableTag, "guild_stock_diffs") {
    def * = (id, guildId, deltaSnapshotId) <> (GuildStockDiffsRow.tupled, GuildStockDiffsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(guildId), Rep.Some(deltaSnapshotId)).shaped.<>({ r => import r._; _1.map(_ => GuildStockDiffsRow.tupled((_1.get, _2.get, _3.get))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(INT), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column guild_id SqlType(INT) */
    val guildId: Rep[Int] = column[Int]("guild_id")
    /** Database column delta_snapshot_id SqlType(INT) */
    val deltaSnapshotId: Rep[Int] = column[Int]("delta_snapshot_id")

    /** Foreign key referencing Guilds (database name guild_stock_diff_guilds_id_fk) */
    lazy val guildsFk = foreignKey("guild_stock_diff_guilds_id_fk", guildId, Guilds)(r => r.id, onUpdate = ForeignKeyAction.Restrict, onDelete=ForeignKeyAction.Cascade)
    /** Foreign key referencing StockSnapshots (database name guild_stock_diff_stock_snapshots_id_fk) */
    lazy val stockSnapshotsFk = foreignKey("guild_stock_diff_stock_snapshots_id_fk", deltaSnapshotId, StockSnapshots)(r => r.id, onUpdate = ForeignKeyAction.Restrict, onDelete=ForeignKeyAction.Restrict)
  }
  /** Collection-like TableQuery object for table GuildStockDiffs */
  lazy val GuildStockDiffs = new TableQuery(tag => new GuildStockDiffs(tag))

  /** Entity class storing rows of table GuildStocks
    *
    * @param guildId                  Database column guild_id SqlType(INT), PrimaryKey
    * @param stockResourcesSnapshotId Database column stock_resources_snapshot_id SqlType(INT), Default(None)
    * @param stockAlchemySnapshotId   Database column stock_alchemy_snapshot_id SqlType(INT), Default(None)
    * @param stockMiscSnapshotId      Database column stock_misc_snapshot_id SqlType(INT), Default(None)
    * @param stockOtherSnapshotId     Database column stock_other_snapshot_id SqlType(INT), Default(None) */
  case class GuildStocksRow(guildId: Int, stockResourcesSnapshotId: Option[Int] = None, stockAlchemySnapshotId: Option[Int] = None, stockMiscSnapshotId: Option[Int] = None, stockOtherSnapshotId: Option[Int] = None)
  /** GetResult implicit for fetching GuildStocksRow objects using plain SQL queries */
  implicit def GetResultGuildStocksRow(implicit e0: GR[Int], e1: GR[Option[Int]]): GR[GuildStocksRow] = GR {
    prs =>
      import prs._
      GuildStocksRow.tupled((<<[Int], <<?[Int], <<?[Int], <<?[Int], <<?[Int]))
  }
  /** Table description of table guild_stocks. Objects of this class serve as prototypes for rows in queries. */
  class GuildStocks(_tableTag: Tag) extends profile.api.Table[GuildStocksRow](_tableTag, "guild_stocks") {
    def * = (guildId, stockResourcesSnapshotId, stockAlchemySnapshotId, stockMiscSnapshotId, stockOtherSnapshotId) <> (GuildStocksRow.tupled, GuildStocksRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(guildId), stockResourcesSnapshotId, stockAlchemySnapshotId, stockMiscSnapshotId, stockOtherSnapshotId).shaped.<>({ r => import r._; _1.map(_ => GuildStocksRow.tupled((_1.get, _2, _3, _4, _5))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column guild_id SqlType(INT), PrimaryKey */
    val guildId: Rep[Int] = column[Int]("guild_id", O.PrimaryKey)
    /** Database column stock_resources_snapshot_id SqlType(INT), Default(None) */
    val stockResourcesSnapshotId: Rep[Option[Int]] = column[Option[Int]]("stock_resources_snapshot_id", O.Default(None))
    /** Database column stock_alchemy_snapshot_id SqlType(INT), Default(None) */
    val stockAlchemySnapshotId: Rep[Option[Int]] = column[Option[Int]]("stock_alchemy_snapshot_id", O.Default(None))
    /** Database column stock_misc_snapshot_id SqlType(INT), Default(None) */
    val stockMiscSnapshotId: Rep[Option[Int]] = column[Option[Int]]("stock_misc_snapshot_id", O.Default(None))
    /** Database column stock_other_snapshot_id SqlType(INT), Default(None) */
    val stockOtherSnapshotId: Rep[Option[Int]] = column[Option[Int]]("stock_other_snapshot_id", O.Default(None))

    /** Foreign key referencing Guilds (database name guild_stocks_guilds_id_fk) */
    lazy val guildsFk = foreignKey("guild_stocks_guilds_id_fk", guildId, Guilds)(r => r.id, onUpdate = ForeignKeyAction.Restrict, onDelete=ForeignKeyAction.Cascade)
    /** Foreign key referencing StockSnapshots (database name guild_stocks_stock_alchemy_snapshots_id_fk) */
    lazy val stockSnapshotsFk2 = foreignKey("guild_stocks_stock_alchemy_snapshots_id_fk", stockAlchemySnapshotId, StockSnapshots)(r => Rep.Some(r.id), onUpdate = ForeignKeyAction.Restrict, onDelete=ForeignKeyAction.SetNull)
    /** Foreign key referencing StockSnapshots (database name guild_stocks_stock_resources_snapshots_id_fk) */
    lazy val stockSnapshotsFk3 = foreignKey("guild_stocks_stock_resources_snapshots_id_fk", stockResourcesSnapshotId, StockSnapshots)(r => Rep.Some(r.id), onUpdate = ForeignKeyAction.Restrict, onDelete=ForeignKeyAction.SetNull)
    /** Foreign key referencing StockSnapshots (database name guild_stocks_stock_snapshots_misc_id_fk) */
    lazy val stockSnapshotsFk4 = foreignKey("guild_stocks_stock_snapshots_misc_id_fk", stockMiscSnapshotId, StockSnapshots)(r => Rep.Some(r.id), onUpdate = ForeignKeyAction.Restrict, onDelete=ForeignKeyAction.SetNull)
    /** Foreign key referencing StockSnapshots (database name guild_stocks_stock_snapshots_other_id_fk) */
    lazy val stockSnapshotsFk5 = foreignKey("guild_stocks_stock_snapshots_other_id_fk", stockOtherSnapshotId, StockSnapshots)(r => Rep.Some(r.id), onUpdate = ForeignKeyAction.Restrict, onDelete=ForeignKeyAction.SetNull)
  }
  /** Collection-like TableQuery object for table GuildStocks */
  lazy val GuildStocks = new TableQuery(tag => new GuildStocks(tag))

  /** Entity class storing rows of table Items
    *
    * @param id        Database column id SqlType(INT), AutoInc, PrimaryKey
    * @param ingameId  Database column ingame_id SqlType(VARCHAR), Length(8,true), Default(None)
    * @param name      Database column name SqlType(VARCHAR), Length(128,true)
    * @param priceGold Database column price_gold SqlType(DECIMAL), Default(None) */
  case class ItemsRow(id: Int, ingameId: Option[String] = None, name: String, priceGold: Option[scala.math.BigDecimal] = None)
  /** GetResult implicit for fetching ItemsRow objects using plain SQL queries */
  implicit def GetResultItemsRow(implicit e0: GR[Int], e1: GR[Option[String]], e2: GR[String], e3: GR[Option[scala.math.BigDecimal]]): GR[ItemsRow] = GR {
    prs =>
      import prs._
      ItemsRow.tupled((<<[Int], <<?[String], <<[String], <<?[scala.math.BigDecimal]))
  }
  /** Table description of table items. Objects of this class serve as prototypes for rows in queries. */
  class Items(_tableTag: Tag) extends profile.api.Table[ItemsRow](_tableTag, "items") {
    def * = (id, ingameId, name, priceGold) <> (ItemsRow.tupled, ItemsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), ingameId, Rep.Some(name), priceGold).shaped.<>({ r => import r._; _1.map(_ => ItemsRow.tupled((_1.get, _2, _3.get, _4))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(INT), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column ingame_id SqlType(VARCHAR), Length(8,true), Default(None) */
    val ingameId: Rep[Option[String]] = column[Option[String]]("ingame_id", O.Length(8,varying=true), O.Default(None))
    /** Database column name SqlType(VARCHAR), Length(128,true) */
    val name: Rep[String] = column[String]("name", O.Length(128,varying=true))
    /** Database column price_gold SqlType(DECIMAL), Default(None) */
    val priceGold: Rep[Option[scala.math.BigDecimal]] = column[Option[scala.math.BigDecimal]]("price_gold", O.Default(None))

    /** Uniqueness Index over (name) (database name items_name_uindex) */
    val index1 = index("items_name_uindex", name, unique=true)
  }
  /** Collection-like TableQuery object for table Items */
  lazy val Items = new TableQuery(tag => new Items(tag))

  /** Entity class storing rows of table StockContents
    *
    * @param userId Database column user_id SqlType(INT)
    * @param itemId Database column item_id SqlType(INT)
    * @param amount Database column amount SqlType(INT) */
  case class StockContentsRow(userId: Int, itemId: Int, amount: Int)
  /** GetResult implicit for fetching StockContentsRow objects using plain SQL queries */
  implicit def GetResultStockContentsRow(implicit e0: GR[Int]): GR[StockContentsRow] = GR {
    prs =>
      import prs._
      StockContentsRow.tupled((<<[Int], <<[Int], <<[Int]))
  }
  /** Table description of table stock_contents. Objects of this class serve as prototypes for rows in queries. */
  class StockContents(_tableTag: Tag) extends profile.api.Table[StockContentsRow](_tableTag, "stock_contents") {
    def * = (userId, itemId, amount) <> (StockContentsRow.tupled, StockContentsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(userId), Rep.Some(itemId), Rep.Some(amount)).shaped.<>({ r => import r._; _1.map(_ => StockContentsRow.tupled((_1.get, _2.get, _3.get))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column user_id SqlType(INT) */
    val userId: Rep[Int] = column[Int]("user_id")
    /** Database column item_id SqlType(INT) */
    val itemId: Rep[Int] = column[Int]("item_id")
    /** Database column amount SqlType(INT) */
    val amount: Rep[Int] = column[Int]("amount")

    /** Foreign key referencing Items (database name stock_contents_items_id_fk) */
    lazy val itemsFk = foreignKey("stock_contents_items_id_fk", itemId, Items)(r => r.id, onUpdate = ForeignKeyAction.Restrict, onDelete=ForeignKeyAction.Restrict)

    /** Uniqueness Index over (userId,itemId) (database name stock_contents_user_id_item_id_uindex) */
    val index1 = index("stock_contents_user_id_item_id_uindex", (userId, itemId), unique=true)
  }
  /** Collection-like TableQuery object for table StockContents */
  lazy val StockContents = new TableQuery(tag => new StockContents(tag))

  /** Entity class storing rows of table StockSnapshotContents
    *
    * @param snapshotId Database column snapshot_id SqlType(INT)
    * @param itemId     Database column item_id SqlType(INT)
    * @param quantity Database column quantity SqlType(INT) */
  case class StockSnapshotContentsRow(snapshotId: Int, itemId: Int, quantity: Int)
  /** GetResult implicit for fetching StockSnapshotContentsRow objects using plain SQL queries */
  implicit def GetResultStockSnapshotContentsRow(implicit e0: GR[Int]): GR[StockSnapshotContentsRow] = GR {
    prs =>
      import prs._
      StockSnapshotContentsRow.tupled((<<[Int], <<[Int], <<[Int]))
  }
  /** Table description of table stock_snapshot_contents. Objects of this class serve as prototypes for rows in queries. */
  class StockSnapshotContents(_tableTag: Tag) extends profile.api.Table[StockSnapshotContentsRow](_tableTag, "stock_snapshot_contents") {
    def * = (snapshotId, itemId, quantity) <> (StockSnapshotContentsRow.tupled, StockSnapshotContentsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(snapshotId), Rep.Some(itemId), Rep.Some(quantity)).shaped.<>({ r => import r._; _1.map(_ => StockSnapshotContentsRow.tupled((_1.get, _2.get, _3.get))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column snapshot_id SqlType(INT) */
    val snapshotId: Rep[Int] = column[Int]("snapshot_id")
    /** Database column item_id SqlType(INT) */
    val itemId: Rep[Int] = column[Int]("item_id")
    /** Database column quantity SqlType(INT) */
    val quantity: Rep[Int] = column[Int]("quantity")

    /** Foreign key referencing Items (database name stock_snapshot_contents_items_id_fk) */
    lazy val itemsFk = foreignKey("stock_snapshot_contents_items_id_fk", itemId, Items)(r => r.id, onUpdate = ForeignKeyAction.Restrict, onDelete=ForeignKeyAction.Restrict)
    /** Foreign key referencing StockSnapshots (database name stock_snapshot_contents_stock_snapshots_id_fk) */
    lazy val stockSnapshotsFk = foreignKey("stock_snapshot_contents_stock_snapshots_id_fk", snapshotId, StockSnapshots)(r => r.id, onUpdate = ForeignKeyAction.Restrict, onDelete=ForeignKeyAction.Cascade)

    /** Uniqueness Index over (snapshotId,itemId) (database name stock_snapshot_contents_snapshot_id_item_id_uindex) */
    val index1 = index("stock_snapshot_contents_snapshot_id_item_id_uindex", (snapshotId, itemId), unique=true)
  }
  /** Collection-like TableQuery object for table StockSnapshotContents */
  lazy val StockSnapshotContents = new TableQuery(tag => new StockSnapshotContents(tag))

  /** Entity class storing rows of table StockSnapshots
    *
    * @param id        Database column id SqlType(INT), AutoInc, PrimaryKey
    * @param createdAt Database column created_at SqlType(TIMESTAMP) */
  case class StockSnapshotsRow(id: Int, createdAt: java.sql.Timestamp)
  /** GetResult implicit for fetching StockSnapshotsRow objects using plain SQL queries */
  implicit def GetResultStockSnapshotsRow(implicit e0: GR[Int], e1: GR[java.sql.Timestamp]): GR[StockSnapshotsRow] = GR {
    prs =>
      import prs._
      StockSnapshotsRow.tupled((<<[Int], <<[java.sql.Timestamp]))
  }
  /** Table description of table stock_snapshots. Objects of this class serve as prototypes for rows in queries. */
  class StockSnapshots(_tableTag: Tag) extends profile.api.Table[StockSnapshotsRow](_tableTag, "stock_snapshots") {
    def * = (id, createdAt) <> (StockSnapshotsRow.tupled, StockSnapshotsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(createdAt)).shaped.<>({ r => import r._; _1.map(_ => StockSnapshotsRow.tupled((_1.get, _2.get))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(INT), AutoInc, PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.AutoInc, O.PrimaryKey)
    /** Database column created_at SqlType(TIMESTAMP) */
    val createdAt: Rep[java.sql.Timestamp] = column[java.sql.Timestamp]("created_at")
  }
  /** Collection-like TableQuery object for table StockSnapshots */
  lazy val StockSnapshots = new TableQuery(tag => new StockSnapshots(tag))

  /** Entity class storing rows of table Users
    *
    * @param id            Database column id SqlType(INT), PrimaryKey
    * @param firstName     Database column first_name SqlType(VARCHAR), Length(128,true)
    * @param lastName      Database column last_name SqlType(VARCHAR), Length(128,true), Default(None)
    * @param username      Database column username SqlType(VARCHAR), Length(128,true), Default(None)
    * @param role          Database column role SqlType(VARCHAR), Length(16,true), Default(USER)
    * @param botIsBlocked  Database column bot_is_blocked SqlType(BIT), Default(false)
    * @param isDeactivated Database column is_deactivated SqlType(BIT), Default(false) */
  case class UsersRow(id: Int, firstName: String, lastName: Option[String] = None, username: Option[String] = None, role: String = "USER", botIsBlocked: Boolean = false, isDeactivated: Boolean = false)
  /** GetResult implicit for fetching UsersRow objects using plain SQL queries */
  implicit def GetResultUsersRow(implicit e0: GR[Int], e1: GR[String], e2: GR[Option[String]], e3: GR[Boolean]): GR[UsersRow] = GR {
    prs =>
      import prs._
      UsersRow.tupled((<<[Int], <<[String], <<?[String], <<?[String], <<[String], <<[Boolean], <<[Boolean]))
  }
  /** Table description of table users. Objects of this class serve as prototypes for rows in queries. */
  class Users(_tableTag: Tag) extends profile.api.Table[UsersRow](_tableTag, "users") {
    def * = (id, firstName, lastName, username, role, botIsBlocked, isDeactivated) <> (UsersRow.tupled, UsersRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), Rep.Some(firstName), lastName, username, Rep.Some(role), Rep.Some(botIsBlocked), Rep.Some(isDeactivated)).shaped.<>({ r => import r._; _1.map(_ => UsersRow.tupled((_1.get, _2.get, _3, _4, _5.get, _6.get, _7.get))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(INT), PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.PrimaryKey)
    /** Database column first_name SqlType(VARCHAR), Length(128,true) */
    val firstName: Rep[String] = column[String]("first_name", O.Length(128,varying=true))
    /** Database column last_name SqlType(VARCHAR), Length(128,true), Default(None) */
    val lastName: Rep[Option[String]] = column[Option[String]]("last_name", O.Length(128,varying=true), O.Default(None))
    /** Database column username SqlType(VARCHAR), Length(128,true), Default(None) */
    val username: Rep[Option[String]] = column[Option[String]]("username", O.Length(128,varying=true), O.Default(None))
    /** Database column role SqlType(VARCHAR), Length(16,true), Default(USER) */
    val role: Rep[String] = column[String]("role", O.Length(16,varying=true), O.Default("USER"))
    /** Database column bot_is_blocked SqlType(BIT), Default(false) */
    val botIsBlocked: Rep[Boolean] = column[Boolean]("bot_is_blocked", O.Default(false))
    /** Database column is_deactivated SqlType(BIT), Default(false) */
    val isDeactivated: Rep[Boolean] = column[Boolean]("is_deactivated", O.Default(false))
  }
  /** Collection-like TableQuery object for table Users */
  lazy val Users = new TableQuery(tag => new Users(tag))

  /** Entity class storing rows of table UserSettings
    *
    * @param userId                     Database column user_id SqlType(INT), PrimaryKey
    * @param stockDiffEnabled           Database column stock_diff_enabled SqlType(BIT), Default(false)
    * @param sexSellNotificationEnabled Database column sex_sell_notification_enabled SqlType(BIT), Default(false)
    * @param eventTradingEnabled        Database column event_trading_enabled SqlType(BIT), Default(false) */
  case class UserSettingsRow(userId: Int, stockDiffEnabled: Boolean = false, sexSellNotificationEnabled: Boolean = false, eventTradingEnabled: Boolean = false)
  /** GetResult implicit for fetching UserSettingsRow objects using plain SQL queries */
  implicit def GetResultUserSettingsRow(implicit e0: GR[Int], e1: GR[Boolean]): GR[UserSettingsRow] = GR {
    prs =>
      import prs._
      UserSettingsRow.tupled((<<[Int], <<[Boolean], <<[Boolean], <<[Boolean]))
  }
  /** Table description of table user_settings. Objects of this class serve as prototypes for rows in queries. */
  class UserSettings(_tableTag: Tag) extends profile.api.Table[UserSettingsRow](_tableTag, "user_settings") {
    def * = (userId, stockDiffEnabled, sexSellNotificationEnabled, eventTradingEnabled) <> (UserSettingsRow.tupled, UserSettingsRow.unapply)

    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(userId), Rep.Some(stockDiffEnabled), Rep.Some(sexSellNotificationEnabled), Rep.Some(eventTradingEnabled)).shaped.<>({ r => import r._; _1.map(_ => UserSettingsRow.tupled((_1.get, _2.get, _3.get, _4.get))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column user_id SqlType(INT), PrimaryKey */
    val userId: Rep[Int] = column[Int]("user_id", O.PrimaryKey)
    /** Database column stock_diff_enabled SqlType(BIT), Default(false) */
    val stockDiffEnabled: Rep[Boolean] = column[Boolean]("stock_diff_enabled", O.Default(false))
    /** Database column sex_sell_notification_enabled SqlType(BIT), Default(false) */
    val sexSellNotificationEnabled: Rep[Boolean] = column[Boolean]("sex_sell_notification_enabled", O.Default(false))
    /** Database column event_trading_enabled SqlType(BIT), Default(false) */
    val eventTradingEnabled: Rep[Boolean] = column[Boolean]("event_trading_enabled", O.Default(false))
  }
  /** Collection-like TableQuery object for table UserSettings */
  lazy val UserSettings = new TableQuery(tag => new UserSettings(tag))
}
