package me.gwisp2.cwsk.model

import java.sql.Timestamp

import me.gwisp2.cwsk.bot.parsed.EquipmentItem
import me.gwisp2.cwsk.dto.PetInfo
import me.gwisp2.cwsk.model.Tables._
import me.gwisp2.cwsk.model.extension.TableQueryExtensions._
import slick.dbio.Effect
import slick.jdbc.MySQLProfile.api._
import slick.sql.SqlAction

object CharsDao extends GenericDao {

  case class CharData(
                       charsRow: CharsRow, guildsRow: Option[GuildsRow],
                       equipmment: Seq[EquipmentItem]
                     )

  val guildsReturningId = Guilds returning Guilds.map(_.id)

  private val qUpdateChar = Compiled {
    userId: Rep[Int] =>
      Chars.filter(_.id === userId).map(t => (
        t.name, t.castle, t.gameClass, t.atk, t.`def`, t.exp, t.level, t.pog, t.guildId, t.updatedAt))
  }

  def getGuildId(userId: Int): DBIOAction[Option[Int], NoStream, Effect.Read] = {
    for (
      optOptGuildId <- Chars.filter(_.id === userId).map(_.guildId).result.headOption
    ) yield optOptGuildId.flatten
  }

  def getChar(id: Int): DBIOAction[Option[CharData], NoStream, Effect.Read] = {
    getChars(Some(id)).map(_.headOption)
  }

  def getChars(ids: Traversable[Int]): DBIOAction[Seq[CharData], NoStream, Effect.Read] = {
    for (
      data <- (Chars
        joinLeft Guilds on (_.guildId === _.id)
        joinLeft CharsEquipment on (_._1.id === _.id))
        .filter(_._1._1.id.inSet(ids)).result;
      allEquipmentIds = data.flatMap { case (_, optEquipment) => extractEquipmentIds(optEquipment) }.toSet;
      equipment <- EquipmentItems.filter(_.id.inSet(allEquipmentIds)).result;
      equipmentById = equipment.map(row => (row.id, extractEquipmentItem(row))).toMap
    ) yield data.map { case ((char, optGuild), optEq) => CharData(
      char, optGuild, extractEquipmentIds(optEq).map(eqId => equipmentById(eqId))
    )
    }
  }

  def findCreateEquipmentItemIds(items: Seq[EquipmentItem]): DBIOAction[Seq[Int], NoStream, Effect.Read with Effect.Write] = {
    val itemLines = items.map(_.line)
    for (
      existingLines <- EquipmentItems.filter(_.line.inSet(itemLines)).map(_.line).result;
      newItems = items.filter(item => !existingLines.contains(item.line));
      _ <- EquipmentItems ++= newItems.map(item => EquipmentItemsRow(0, item.line, item.name,
        item.enchantmentLevel, item.bonusAtk, item.bonusDef, item.bonusStamina, item.bonusBagSlots, None));
      rows <- EquipmentItems.filter(_.line.inSet(itemLines)).map(t => (t.line, t.id)).result;
      lineToId = rows.toMap
    ) yield items.map(item => lineToId(item.line))
  }

  def updateEquipment(userId: Int, equipment: Seq[EquipmentItem], updatedAt: Timestamp): DBIOAction[Unit, NoStream, Effect.Read with Effect.Write] = {
    for (
      equipmentIds <- findCreateEquipmentItemIds(equipment);
      e1 = equipmentIds.lift(0);
      e2 = equipmentIds.lift(1);
      e3 = equipmentIds.lift(2);
      e4 = equipmentIds.lift(3);
      e5 = equipmentIds.lift(4);
      e6 = equipmentIds.lift(5);
      e7 = equipmentIds.lift(6);
      e8 = equipmentIds.lift(7);
      e9 = equipmentIds.lift(8);
      e10 = equipmentIds.lift(9);
      _ <- CharsEquipment.insertOrUpdate(CharsEquipmentRow(userId, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, updatedAt));
      _ <- changeEquipmentValidity(userId, newValidity = true)
    ) yield ()
  }

  def createOrUpdateChar(userId: Int, name: String, gameClass: String, castle: String,
                         atk: Int, `def`: Int, exp: Int, level: Int, pog: Int,
                         guildId: Option[Int], updatedAt: Timestamp
                        ): DBIOAction[Unit, NoStream, Effect.Read with Effect.Write] = {
    for (existingChar <- CharsDao.findCharById(userId);
         _ <- existingChar match {
           case Some(_) => qUpdateChar(userId).update((name, castle, gameClass, atk, `def`, exp, level, pog, guildId, updatedAt))
           case _ => Chars += CharsRow(
             id = userId,
             name = name,
             castle = castle,
             gameClass = gameClass,
             atk = atk,
             `def` = `def`,
             exp = exp,
             level = level,
             pog = pog,
             guildId = guildId,
             createdAt = updatedAt, updatedAt = updatedAt)
         }) yield ()
  }

  def updatePet(userId: Int, pet: Option[PetInfo]): DBIOAction[Unit, NoStream, Effect.Read with Effect.Write] = {
    for (
      _ <- Chars.filter(_.id === userId).map(t => (t.petIcon, t.petFullName, t.petLevel)).update(
        (pet.map(_.icon), pet.map(_.fullName), pet.map(_.level))
      )
    ) yield ()
  }

  def changeEquipmentValidity(userId: Int, newValidity: Boolean): DBIOAction[Unit, NoStream, Effect.Write] = {
    for (_ <- Chars.filter(_.id === userId).map(_.equipmentIsValid).update(newValidity)) yield ()
  }

  val findCharById: Int => SqlAction[Option[CharsRow], NoStream, Effect.Read] = Chars.findOneBy(_.id)

  private def extractEquipmentIds(row: Option[CharsEquipmentRow]): Seq[Int] = {
    row.map(row => Seq(
      row.e1, row.e2, row.e3, row.e4, row.e5, row.e6, row.e7, row.e8, row.e9, row.e10
    ).collect { case Some(x) => x }).getOrElse(Seq())
  }

  private def extractEquipmentItem(row: EquipmentItemsRow): EquipmentItem = {
    EquipmentItem(row.line, row.name, row.enchantmentLevel, row.atkBonus, row.defBonus,
      row.staminaBonus, row.bagBonus)
  }
}
