package me.gwisp2.cwsk.model

import me.gwisp2.cwsk.dto.items.{ItemSearcher, ItemSet, StockItem, StockItemType}
import me.gwisp2.cwsk.model.Tables._
import slick.dbio.Effect
import slick.jdbc.MySQLProfile.api._
import slick.sql.{FixedSqlAction, FixedSqlStreamingAction}

object StockDao extends GenericDao {
  def saveStock(userId: Int, stock: Map[String, Int]): DBIOAction[Unit, NoStream, Effect.Write with Effect.Read] = {
    for (
      _ <- StockContents.filter(_.userId === userId).delete;
      itemIdsMap <- findItemIds(stock.keySet);
      _ <- StockContents ++= stock.map { case (name, amount) => StockContentsRow(userId, itemIdsMap(name), amount) }
    ) yield ()
  }

  private val findByUserId = StockContents.findBy(_.userId)

  def loadStock(userId: Int): DBIOAction[Seq[StockItem], NoStream, Effect.Read] = {
    for (
      stockContentsRows <- findByUserId(userId).result;
      itemIds = stockContentsRows.map(_.itemId).toSet;
      items <- Items.filter(t => t.id.inSet(itemIds)).result;
      id2Item = items.map(row => (row.id, row)).toMap
    ) yield stockContentsRows.map { row =>
      val item = id2Item(row.itemId)
      StockItem(StockItemType(item.ingameId, item.name), row.amount)
    }
  }

  def loadStocks(userIds: Traversable[Int], itemIds: Traversable[Int]): DBIO[Map[Int, ItemSet]] = {
    for (
      stockContentsRows <- StockContents.filter(sc => sc.itemId.inSet(itemIds) && sc.userId.inSet(userIds)).result;
      itemIds = stockContentsRows.map(_.itemId).toSet;
      items <- Items.filter(t => t.id.inSet(itemIds)).result;
      id2Item = items.map(row => (row.id, row)).toMap
    ) yield stockContentsRows.groupBy(_.userId).mapValues { rows =>
      ItemSet(rows.map { row =>
        val item = id2Item(row.itemId)
        StockItem(StockItemType(item.ingameId, item.name), row.amount)
      })
    }.withDefaultValue(ItemSet.empty)
  }

  def loadAllItems(): DBIOAction[Seq[StockItemType], NoStream, Effect.Read] = {
    Items.map(t => (t.ingameId, t.name)).result.map(_.map((StockItemType.apply _).tupled))
  }

  def createItemSearcher(): DBIO[ItemSearcher] = {
    Items.map(t => (t.id, t.ingameId, t.name)).result.map(_.map {
      case (id, ingameId, name) => ItemSearcher.ItemTypeWithId(id, StockItemType(ingameId, name))
    }).map(new ItemSearcher(_))
  }

  def loadItemNameToIngameIdMap(): DBIOAction[Map[String, String], NoStream, Effect.Read] = {
    for (
      items <- Items.result;
      name2ingameId = items.map(row => row.ingameId match {
        case Some(ingameId) => Some((row.name, ingameId))
        case None => None
      }).collect {
        case Some(x) => x
      }.toMap
    ) yield name2ingameId
  }

  def updateItemIngameIds(nameIdMap: Map[String, String]): DBIOAction[Seq[String], NoStream, Effect.Read with Effect.Write] = {
    for (
      _ <- findItemIds(nameIdMap.keySet); // Insert new items
      newNames <- Items.filter(t => t.name.inSet(nameIdMap.keySet) && t.ingameId.isEmpty).map(_.name).result;
      _ <- DBIO.seq(newNames.map(name => updateItemIngameId(nameIdMap(name), name)): _*)
    ) yield newNames
  }

  def updateItemIngameId(id: String, name: String): FixedSqlAction[Int, NoStream, Effect.Write] = {
    Items.filter(_.name === name).map(_.ingameId).update(Some(id))
  }

  def findItemNamesWithoutIngameId(itemNames: Traversable[String]): FixedSqlStreamingAction[Seq[String], String, Effect.Read] = {
    Items.filter(t => t.name.inSet(itemNames) && t.ingameId.isEmpty).map(_.name).result
  }

  def findItemId(itemName: String): DBIOAction[Int, NoStream, Effect.Read with Effect.Write] = {
    findItemIds(Set(itemName)).map(_.values.head)
  }

  def findItemIds(itemNames: Traversable[String]): DBIOAction[Map[String, Int], NoStream, Effect.Read with Effect.Write] = {
    for (
      existingNames <- Items.filter(_.name.inSet(itemNames)).map(_.name).result;
      newNames = itemNames.filter(itemName => !existingNames.contains(itemName));
      _ <- Items ++= newNames.toIterable.map(name => ItemsRow(0, name = name));
      rows <- Items.filter(_.name.inSet(itemNames)).map(t => (t.name, t.id)).result
    ) yield rows.toMap
  }
}