package me.gwisp2.cwsk.model

import me.gwisp2.cwsk.model.Tables._
import me.gwisp2.cwsk.model.extension.TableQueryExtensions._
import me.gwisp2.cwsk.model.types.GoldSinkRule
import slick.dbio.Effect
import slick.jdbc.MySQLProfile.api._


object GoldSinkRulesDao extends GenericDao {
  def changeAutosinkStatus(userId: Int, newStatus: Boolean): DBIOAction[Unit, NoStream, Effect.Read with Effect.Write] = {
    for (
      _ <- ensureUserSettingsExist(userId);
      _ <- GoldSinkUserSettings.filter(_.userId === userId).map(_.autosinkEnabled).update(newStatus)
    ) yield ()
  }

  def changeSinkLimit(userId: Int, newLimit: Option[Int]): DBIOAction[Unit, NoStream, Effect.Read with Effect.Write] = {
    for (
      _ <- ensureUserSettingsExist(userId);
      _ <- GoldSinkUserSettings.filter(_.userId === userId).map(_.goldSinkLimit).update(newLimit)
    ) yield ()
  }

  def changeSinkTimeslot(userId: Int, newTimeslot: Option[Int]): DBIO[Unit] = {
    for (
      _ <- ensureUserSettingsExist(userId);
      _ <- GoldSinkUserSettings.filter(_.userId === userId).map(_.goldSinkTimeslot).update(newTimeslot)
    ) yield ()
  }

  def getAutosinkStatus(userId: Int): DBIOAction[Boolean, NoStream, Effect.Read] = {
    for (
      status <- GoldSinkUserSettings.filter(_.userId === userId).map(_.autosinkEnabled).result.headOption
    ) yield status.getOrElse(false)
  }

  def getUsersWithAutosinkEnabled(timeslot: Option[Int]): DBIOAction[Seq[Int], NoStream, Effect.Read] = {
    val tables = GoldSinkUserSettings join Users on (_.userId === _.id)
    val condition: GoldSinkUserSettings => Rep[Option[Boolean]] = timeslot match {
      case Some(ts) => gs => gs.goldSinkTimeslot.isDefined && gs.goldSinkTimeslot === ts
      case None => gs => !gs.goldSinkTimeslot.isDefined.?
    }

    for (
      userIds <- tables.filter { case (settings, user) =>
        condition(settings) && settings.autosinkEnabled && !user.isDeactivated && !user.botIsBlocked
      }.map { case (settings, _) => settings.userId }.result
    ) yield userIds
  }

  def addRule(rule: GoldSinkRule): DBIOAction[Int, NoStream, Effect.Write] = {
    for (
      id <- (GoldSinkRules returning GoldSinkRules.map(_.id)) += GoldSinkRulesRow(0, GoldSinkRule.toJson(rule))
    ) yield id
  }

  def getRuleForUserAndGuild(userId: Int): DBIOAction[(Option[Int], Option[Int], Option[GoldSinkRule], Option[GoldSinkRule]), NoStream, Effect.Read] = {
    for (
      optGuildId <- CharsDao.findCharById(userId).map(_.flatMap(_.guildId));
      userSettingsOpt <- findUserSettingsById(userId);
      userSettings = userSettingsOpt.getOrElse(GoldSinkUserSettingsRow(userId));
      ruleIdForGuild <- DBIO.sequenceOption(optGuildId.map(
        guildId => findGuildSettingsById(guildId).map(_.flatMap(_.sinkRuleId))
      )).map(_.flatten);
      ruleForUser <- DBIO.sequenceOption(userSettings.sinkRuleId.map(findRuleById)).map(_.flatten);
      ruleForGuild <- DBIO.sequenceOption(ruleIdForGuild.map(findRuleById)).map(_.flatten)
    ) yield (userSettings.goldSinkLimit, userSettings.goldSinkTimeslot, ruleForUser, ruleForGuild)
  }

  def setRuleForUser(userId: Int, ruleId: Option[Int]): DBIOAction[Unit, NoStream, Effect.Read with Effect.Write] = {
    for (
      _ <- ensureUserSettingsExist(userId);
      _ <- GoldSinkUserSettings.filter(_.userId === userId).map(_.sinkRuleId).update(ruleId)
    ) yield ()
  }

  def setRuleForGuild(guildId: Int, ruleId: Option[Int]): DBIOAction[Unit, NoStream, Effect.Read with Effect.Write] = {
    for (
      _ <- ensureGuildSettingsExist(guildId);
      _ <- GoldSinkGuildSettings.filter(_.guildId === guildId).map(_.sinkRuleId).update(ruleId)
    ) yield ()
  }

  private def findRuleById(ruleId: Int) = {
    findRuleRowById(ruleId).map(optRow => optRow.map(row => GoldSinkRule.fromJson(row.rule)))
  }

  private val findRuleRowById = GoldSinkRules.findOneBy(_.id)
  private val findUserSettingsById = GoldSinkUserSettings.findOneBy(_.userId)
  private val findGuildSettingsById = GoldSinkGuildSettings.findOneBy(_.guildId)

  private def ensureUserSettingsExist(userId: Int) = {
    for (
      optRow <- GoldSinkUserSettings.filter(_.userId === userId).result.headOption;
      _ <- optRow match {
        case Some(_) => DBIO.successful(())
        case None => GoldSinkUserSettings += GoldSinkUserSettingsRow(userId)
      }
    ) yield ()
  }

  private def ensureGuildSettingsExist(guildId: Int) = {
    for (
      optRow <- GoldSinkGuildSettings.filter(_.guildId === guildId).result.headOption;
      _ <- optRow match {
        case Some(_) => DBIO.successful(())
        case None => GoldSinkGuildSettings += GoldSinkGuildSettingsRow(guildId)
      }
    ) yield ()
  }
}
