package me.gwisp2.cwsk.model

import scala.concurrent.ExecutionContext

abstract class GenericDao {
  private[model] implicit val executionContext: ExecutionContext = scala.concurrent.ExecutionContext.global
}
