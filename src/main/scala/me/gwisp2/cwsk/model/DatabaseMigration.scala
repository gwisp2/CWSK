package me.gwisp2.cwsk.model

import me.gwisp2.cwsk.db.Db
import org.flywaydb.core.Flyway

class DatabaseMigration(db: Db) {
  private lazy val flyway = {
    val flywayConfig = Flyway.configure().dataSource(db.asDataSource)
    new Flyway(flywayConfig)
  }

  def migrate(): Unit = {
    flyway.migrate()
  }

  def clean(): Unit = {
    flyway.clean()
  }

}
