package me.gwisp2.cwsk.model

import me.gwisp2.cwsk.cwapi.methods.OperationType
import me.gwisp2.cwsk.cwapi.methods.OperationType.OperationType
import me.gwisp2.cwsk.dto.UserCwapiInfo
import me.gwisp2.cwsk.model.Tables._
import slick.dbio.Effect
import slick.jdbc.MySQLProfile.api._
import slick.sql.{FixedSqlAction, SqlAction}

object CwApiTokensDao extends GenericDao {
  def getToken(userId: Int): SqlAction[Option[String], NoStream, Effect.Read] = {
    CwapiTokens.filter(_.userId === userId).map(_.token).result.headOption
  }

  def getActiveUserIdsWithPermission(operationType: OperationType): DBIOAction[Seq[Int], NoStream, Effect.Read] = {
    val columnOpt = operationTypeToColumn(operationType)
    val tables = CwapiTokens join Users on (_.userId === _.id)

    columnOpt match {
      case Some(column) => tables.filter {
        case (token, user) => column(token) === true && !user.botIsBlocked && !user.isDeactivated
      }.map { case (token, _) => token.userId }.result
      case None => DBIO.successful(Seq())
    }
  }

  def getUserInfo(userId: Int): DBIOAction[UserCwapiInfo, NoStream, Effect.Read] = {
    for (
      optRow <- CwapiTokens.filter(_.userId === userId).result.headOption;
      userInfo = optRow match {
        case Some(row) => UserCwapiInfo(hasToken = true, Set(
          Some(OperationType.GetUserProfile).filter(_ => row.allowedGetProfile),
          Some(OperationType.GetStock).filter(_ => row.allowedGetStock),
          Some(OperationType.TradeTerminal).filter(_ => row.allowedWtb),
          Some(OperationType.GuildInfo).filter(_ => row.allowedGuildInfo),
        ).collect { case Some(x) => x })
        case _ => UserCwapiInfo(hasToken = false, Set())
      }
    ) yield userInfo
  }

  def saveToken(userId: Int, cwId: String, token: String): FixedSqlAction[Int, NoStream, Effect.Write] = {
    CwapiTokens.insertOrUpdate(CwapiTokensRow(
      userId = userId,
      cwId = cwId,
      token = token
    ))
  }

  def addPermission(userId: Int, operationType: OperationType): DBIOAction[Option[Int], NoStream, Effect.Write] = {
    val permissionColumnOpt = operationTypeToColumn(operationType)

    DBIO.sequenceOption(for (permColumn <- permissionColumnOpt) yield {
      val q = for {c <- CwapiTokens if c.userId === userId} yield permColumn(c)
      q.update(true)
    })
  }

  private def operationTypeToColumn(operationType: OperationType): Option[CwapiTokens => Rep[Boolean]] = {
    operationType match {
      case OperationType.GetUserProfile => Some(_.allowedGetProfile)
      case OperationType.GetStock => Some(_.allowedGetStock)
      case OperationType.TradeTerminal => Some(_.allowedWtb)
      case OperationType.GuildInfo => Some(_.allowedGuildInfo)
      /* Unknown operation */
      case _ => None
    }
  }
}
