package me.gwisp2.cwsk.model

import me.gwisp2.cwsk.dto._
import me.gwisp2.cwsk.model.Tables._
import slick.dbio.Effect
import slick.jdbc.MySQLProfile.api._
import slick.sql.SqlAction


object GuildsDao extends GenericDao {
  /*
   * Find a GuildsRow or create it if it is not present
   * If optName is empty then None will be returned
   */
  def findCreateGuildId(castle: String, optTag: Option[String], optName: Option[String]): DBIOAction[Option[Int], NoStream, Effect.Read with Effect.Write] = {
    optName match {
      case Some(name) =>
        for (
          existingGuild <- findGuildByName(name);
          id <- existingGuild match {
            case Some(g) =>
              if (g.tag != optTag || g.castle != castle)
                Guilds.filter(_.id === g.id).update(g.copy(tag = optTag, castle = castle)).map(_ => g.id)
              else
                DBIO.successful(g.id)
            case _ => guildsReturningId += GuildsRow(0, name, optTag, castle)
          }
        ) yield Some(id)
      case _ => DBIO.successful(None)
    }
  }

  def reifiedGuildMembers[T](guildId: Int, extractor: CharsRow => T): DBIO[ReifiedGuild[T]] = {
    for (
      guildInfo <- GuildsDao.findGuildById(guildId).map(_.getOrElse(throw new ServiceException("Нет гильдии")));
      chars <- Chars.filter(_.guildId === guildId).result
    ) yield ReifiedGuild(extractGuildInfo(guildInfo), chars.map { c =>
      ReifiedChar(SimpleChar(c.id, c.name, c.gameClass), extractor(c))
    })
  }

  def extractGuildInfo(g: Tables.GuildsRow): GuildInfo = {
    GuildInfo(g.castle, g.tag, g.name)
  }


  def transferLeadership(guildId: Int, newLeaderId: Int): DBIOAction[Unit, NoStream, Effect.Write with Effect.Read with Effect.Transactional] = {
    (for (
      _ <- removeLeader(guildId);
      _ <- addLeader(guildId, newLeaderId)
    ) yield ()).transactionally
  }

  def getRole(guildId: Int, userId: Int): DBIO[GuildRole] = {
    for (
      optRole <- GuildRoles.filter(t => t.guildId === guildId && t.userId === userId).result.headOption
    ) yield rowToRole(optRole)
  }

  def updatePermissions(guildId: Int, userId: Int, permissions: GuildPermission.ValueSet): DBIO[Unit] = {
    DBIO.seq(
      ensureGuildRoleRowExists(guildId, userId),
      GuildRoles
      .filter(t => t.guildId === guildId && t.userId === userId)
      .map(_.permissionsBitmask)
        .update(permissions.toBitMask.headOption.getOrElse(0L).toInt))
  }

  def rowToRole(optRow: Option[GuildRolesRow]): GuildRole = {
    optRow.map { row =>
      val permissions = GuildPermission.ValueSet.fromBitMask(Array(row.permissionsBitmask))
      GuildRole(row.isLeader, permissions)
    }.getOrElse(GuildRole(false, GuildPermission.ValueSet.empty))
  }

  private def removeLeader(guildId: Int) = {
    GuildRoles.filter(t => t.guildId === guildId).map(_.isLeader).update(false)
  }

  private def addLeader(guildId: Int, userId: Int) = {
    for (
      _ <- ensureGuildRoleRowExists(guildId, userId);
      _ <- GuildRoles.filter(t => t.guildId === guildId && t.userId === userId).map(_.isLeader).update(true)
    ) yield ()
  }

  private def ensureGuildRoleRowExists(guildId: Int, userId: Int) = {
    for (
      existingRow <- findGuildRoleRow(guildId, userId);
      row <- existingRow match {
        case Some(row) => DBIO.successful(row)
        case None =>
          val row = GuildRolesRow(guildId, userId)
          (GuildRoles += row).map(_ => row)
      }
    ) yield row
  }

  private val findGuildRoleRow: (Int, Int) => SqlAction[Option[GuildRolesRow], NoStream, Effect.Read] = {
    val query = Compiled { (guildId: Rep[Int], userId: Rep[Int]) =>
      GuildRoles.filter(t => t.guildId === guildId && t.userId === userId)
    }
    (guildId: Int, userId: Int) => query((guildId, userId)).result.headOption
  }

  val guildsReturningId = Guilds returning Guilds.map(_.id)

  val findGuildByName: String => SqlAction[Option[GuildsRow], NoStream, Effect.Read] = {
    val query = Compiled { name: Rep[String] => Guilds.filter(_.name === name) }
    name: String => query(name).result.headOption
  }

  val findGuildById: Int => SqlAction[Option[GuildsRow], NoStream, Effect.Read] = {
    val query = Compiled { id: Rep[Int] => Guilds.filter(_.id === id) }
    id: Int => query(id).result.headOption
  }
}
