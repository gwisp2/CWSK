package me.gwisp2.cwsk.model.extension

import slick.dbio.Effect
import slick.jdbc.MySQLProfile.api._
import slick.relational.RelationalProfile
import slick.sql.SqlAction

import scala.language.implicitConversions

class TableQueryExtensions[T <: RelationalProfile#Table[_], U](val q: Query[T, U, Seq] with TableQuery[T]) {
  /** Create a `Compiled` query which selects at most one row where the specified
    * key matches the parameter value. */
  def findOneBy[P](f: (T => Rep[P]))(implicit ashape: Shape[ColumnsShapeLevel, Rep[P], P, Rep[P]], pshape: Shape[ColumnsShapeLevel, P, P, _]): P => SqlAction[Option[U], NoStream, Effect.Read] = {
    val compiledFunction = q.findBy(f)
    (param: P) => compiledFunction(param).result.headOption
  }
}

object TableQueryExtensions {
  implicit def tableQueryToTableQueryExtensions[T <: RelationalProfile#Table[_], U](q: Query[T, U, Seq] with TableQuery[T]): TableQueryExtensions[T, U] =
    new TableQueryExtensions[T, U](q)
}