package me.gwisp2.cwsk.model.types

import me.gwisp2.cwsk.dto.items.StockItemType
import org.json4s.jackson.JsonMethods.{compact, parse, render}
import org.json4s.{DefaultFormats, Extraction, Formats}

import scala.util.parsing.combinator.RegexParsers

case class GoldSinkRule(items: IndexedSeq[GoldSinkRule.Item]) {
  def toText: String = {
    items.map { item =>
      item.desiredAmountInStock.map(amount => s"(target $amount) ").getOrElse("") +
        item.name + s" ${item.price}"
    }.mkString("\n")
  }

  def append(another: GoldSinkRule): GoldSinkRule = {
    GoldSinkRule(items = items ++ another.items)
  }

  def nonEmpty: Boolean = items.nonEmpty
}

object GoldSinkRule {

  class ParserException(val line: String, message: String) extends Exception(message)

  case class Item(name: String, code: String, desiredAmountInStock: Option[Int], price: Int, exactPrice: Boolean = false) {
    require(price >= 1)
  }

  def concat(traversable: Traversable[GoldSinkRule]): GoldSinkRule = {
    traversable.fold(GoldSinkRule(IndexedSeq()))(_ append _)
  }

  def toJson(t: GoldSinkRule): String = {
    implicit val formats: Formats = DefaultFormats
    compact(render(Extraction.decompose(t)))
  }

  def fromJson(json: String): GoldSinkRule = {
    implicit val formats: Formats = DefaultFormats
    parse(json).extract[GoldSinkRule]
  }

  def parseText(text: String, itemCodeResolver: String => Option[StockItemType]): GoldSinkRule = {
    val itemParser = Parser.item(itemCodeResolver)
    val items = text.linesIterator.filter(_.nonEmpty).map { line =>
      Parser.parseAll(itemParser, line) match {
        case Parser.Success(matched, _) => matched
        case Parser.Failure(msg, _) => throw new ParserException(line, msg)
        case Parser.Error(msg, _) => throw new ParserException(line, msg)
      }
    }.toIndexedSeq
    GoldSinkRule(items)
  }

  private object Parser extends RegexParsers {

    private def number: Parser[Int] =
      """(0|[1-9]\d*)""".r ^^ {
        _.toInt
      }

    private def itemTarget: Parser[Int] = literal("(target ") ~> number <~ literal(")")

    private def itemName: Parser[String] =
      """([^\d]+|[\w\d]+)\s+""".r ^^ {
        _.trim()
      }

    private def itemPrice: Parser[Int] = number

    def item(itemTypeResolver: String => Option[StockItemType]): Parser[Item] = itemTarget.? ~ itemName ~ itemPrice >> {
      case optTarget ~ parsedName ~ price =>
        itemTypeResolver(parsedName) match {
          case Some(StockItemType(Some(ingameId), name)) => success(Item(name, ingameId, optTarget, price))
          case Some(StockItemType(None, name)) => failure(s"Мне неизвестен id ресурса: $name")
          case None => failure(s"Неизвестный ресурс: $parsedName")
        }
    }
  }
}