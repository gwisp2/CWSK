package me.gwisp2.cwsk.util

import akka.NotUsed
import akka.stream.OverflowStrategy
import akka.stream.scaladsl.Source

import scala.concurrent.{ExecutionContext, Future, Promise}

object ProgressSource {
  /*
   * Creates a source with manually provided values.
   * Note: materializable only one, subsequent materializations emit no values
   */
  def create[T](f: ProgressConsumer[T] => Unit)(implicit ec: ExecutionContext): Source[T, NotUsed] = {
    val queue = Source.queue[T](0, OverflowStrategy.dropHead)
    val (queueSource, futureQueue) = peekMatValue(queue)

    futureQueue.foreach { queue =>
      val consumer = new ProgressConsumer[T] {
        override def consume(v: T): Unit = this.synchronized {
          queue.offer(v)
        }

        override def complete(): Unit = queue.complete()
      }
      f(consumer)
    }

    queueSource.mapMaterializedValue(_ => NotUsed)
  }

  def peekMatValue[T, M](src: Source[T, M]): (Source[T, M], Future[M]) = {
    // https://stackoverflow.com/questions/36397424/how-to-use-an-akka-streams-sourcequeue-with-playframework/36467193#36467193
    val p = Promise[M]
    val s = src.mapMaterializedValue { m =>
      p.trySuccess(m)
      m
    }
    (s, p.future)
  }

  trait ProgressConsumer[T] {
    def consume(v: T)

    def complete()
  }

}
