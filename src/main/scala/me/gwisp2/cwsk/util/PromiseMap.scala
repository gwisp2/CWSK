package me.gwisp2.cwsk.util

import scala.collection.mutable
import scala.concurrent.{Future, Promise}

class PromiseMap[K, V] {
  private val map = mutable.Map[K, Promise[V]]()

  def resultFuture(key: K): Future[V] = map.synchronized {
    map.getOrElseUpdate(key, Promise[V]()).future
  }

  def fulfill(key: K, value: V): Unit = map.synchronized {
    map.remove(key) match {
      case Some(promise) => promise.success(value)
      case None => /* ignore */
    }
  }

  def reject(key: K, ex: Exception): Unit = map.synchronized {
    map.remove(key) match {
      case Some(promise) => promise.failure(ex)
      case None => /* ignore */
    }
  }
}
