package me.gwisp2.cwsk.util

import java.time.format.DateTimeFormatter
import java.time.{Instant, ZoneId, ZonedDateTime}

object DateFormat {
  private val zoneId = ZoneId.of("Europe/Moscow")
  private val longFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm")
  private val shortFormatter = DateTimeFormatter.ofPattern("HH:mm")

  def format(instant: Instant): String = {
    val now = ZonedDateTime.now(zoneId)
    val zonedInstant = instant.atZone(zoneId)

    val formatter = if (now.toLocalDate != zonedInstant.toLocalDate) {
      longFormatter
    } else {
      shortFormatter
    }

    formatter.format(zonedInstant)
  }

  def formatLong(instant: Instant): String = {
    val zonedInstant = instant.atZone(zoneId)
    longFormatter.format(zonedInstant)
  }
}
