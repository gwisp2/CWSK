package me.gwisp2.cwsk.util

import com.typesafe.scalalogging.Logger

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Failure

object FutureLogger {
  def logFailure(logger: Logger, future: Future[_]): Unit = {
    future.onComplete {
      case Failure(exception) =>
        logger.warn("Future completed with the error", exception)
      case _ => /* Ignore success */
    }
  }
}
