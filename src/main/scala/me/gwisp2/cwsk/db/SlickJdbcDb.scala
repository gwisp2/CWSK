package me.gwisp2.cwsk.db

import javax.sql.DataSource
import slick.dbio.DBIO
import slick.jdbc.JdbcBackend.Database
import slick.jdbc.hikaricp.HikariCPJdbcDataSource

class SlickJdbcDb(db: Database) extends Db {
  override def run[T](io: DBIO[T]) = db.run(io)
  override def asDataSource: DataSource = db.source.asInstanceOf[HikariCPJdbcDataSource].ds
}
