package me.gwisp2.cwsk.db

import javax.sql.DataSource
import slick.dbio.DBIO

import scala.concurrent.Future

trait Db {
  def asDataSource: DataSource
  def run[T](io: DBIO[T]): Future[T]
}
