package me.gwisp2.cwsk.service

import java.sql.Timestamp
import java.time.Instant
import java.util

import com.google.inject.Inject
import me.gwisp2.cwsk.model.Tables._
import slick.jdbc.JdbcBackend.Database
import slick.jdbc.MySQLProfile.api._

import scala.concurrent.Future

class ExpHistoryService @Inject()(db: Database) extends GenericService {

  import ExpHistoryService._

  case class auth()(implicit sc: SecurityContext) {
    def getExpHistory(start: Instant, end: Instant): Future[UserExpHistory] = {
      val userId = sc.effectiveUserId
      val startTimestamp = new Timestamp(start.toEpochMilli)
      val endTimestamp = new Timestamp(end.toEpochMilli)

      val action =
        for (rows <- ExpHistory.filter(t => t.at.between(startTimestamp, endTimestamp) && t.userId === userId)
          .sortBy(_.at).result) yield {
          UserExpHistory(userId, rows.map(r => ExpHistoryItem(
            Instant.ofEpochMilli(r.at.getTime),
            r.exp
          )))
        }

      db.run(action)
    }
  }

  def getLevelUpPrediction(userId: Int): Future[Option[LevelUpPrediction]] = {
    val action = for (
      optLastEntry <- ExpHistory.filter(t => t.userId === userId).sortBy(_.at.desc).result.headOption;
      optStartEntry <- optLastEntry match {
        case Some(lastEntry) =>
          val weekAgo = new Timestamp(lastEntry.at.getTime - 7 * 24 * 3600 * 1000)
          ExpHistory.filter(t => t.userId === userId && t.at >= weekAgo).result.headOption
        case None => DBIO.successful(None)
      }
    ) yield {
      for (
        start <- optStartEntry;
        last <- optLastEntry;
        timeDeltaMillis = last.at.getTime - start.at.getTime if timeDeltaMillis > 0;
        level = LevelTable.getLevel(last.exp);
        nextLevelExp <- LevelTable.totalExpForLevel(level + 1);
        expDelta = last.exp - start.exp if expDelta > 0
      ) yield {
        val dailyExp = (24L * 3600 * 1000 * expDelta / timeDeltaMillis).toInt

        val levelUpInMillis = (nextLevelExp - last.exp) * timeDeltaMillis / expDelta
        val levelUpInstant = Instant.ofEpochMilli(last.at.getTime + levelUpInMillis)

        LevelUpPrediction(level + 1, nextLevelExp, dailyExp, levelUpInstant)
      }
    } 

    db.run(action)
  }
}

object ExpHistoryService {

  case class LevelUpPrediction(level: Int, exp: Int, dailyExp: Int, instant: Instant)

  case class UserExpHistory(userId: Int, items: Seq[ExpHistoryItem])

  case class ExpHistoryItem(at: Instant, exp: Int)

  class LevelTable(private val levelExp: Array[Int]) {
    def totalExpForLevel(level: Int): Option[Int] = {
      levelExp.lift(level - 1)
    }

    def getLevel(exp: Int): Int = util.Arrays.binarySearch(levelExp, exp) match {
      case index if index >= 0 => index + 1
      case index => -index - 1
    }

    def minLevel: Int = 1

    def maxLevel: Int = levelExp.length
  }

  object LevelTable extends LevelTable(Array(
    0, 5, 15, 38, 79, 142, 227, 329, 444, 577, // 1-10
    721, 902, 1127, 1409, 1761, 2202, 2752, 3440, 4300, 5375, // 11 - 20
    6719, 8399, 10498, 13123, 16404, 20504, 25631, 32038, 39023, 46636, // 21 - 30
    54934, 63979, 73838, 84584, 96297, 109065, 122982, 138151, 154685, 172708, // 31 - 40
    192353, 213765, 237105, 262545, 290275, 320501, 353447, 389358, 428501, 471167, // 41 - 50
    517673, 568364, 623618, 683845, 804299, 935594, 1077392, 1229117, 1389944, 1663352, // 51 - 60
    1961366, 2283221, 2627606, 2992654, 3540225, 4137079, 4781681, 5471404, 6202512, 7226062,  // 61-70
    8341732, 9546655 // 71-
  ))

}
