package me.gwisp2.cwsk.service

import com.google.inject.Inject
import com.typesafe.scalalogging.StrictLogging
import me.gwisp2.cwsk.dto._
import me.gwisp2.cwsk.model.Tables._
import me.gwisp2.cwsk.model._
import me.gwisp2.cwsk.service.UserService.ProfileUpdated
import slick.jdbc.JdbcBackend.Database
import slick.jdbc.MySQLProfile.api._

import scala.concurrent.Future

class GuildRolesService @Inject()(
                                   db: Database
                                 ) extends GenericService with StrictLogging {
  val profileUpdatedEvent = new EventHub[ProfileUpdated]()

  case class auth()(implicit securityContext: SecurityContext) {
    def getGuildRole(userId: Int): Future[ReifiedChar[GuildRole]] = {
      val action = for (
        charWithGuild <- findCharWithGuild(userId);
        (char, guildId) = charWithGuild;
        _ <- checkInGuild(guildId);
        role <- GuildsDao.getRole(guildId, userId)
      ) yield ReifiedChar(char, role)
      db.run(action)
    }

    def modifyPermissions(userId: Int, modification: GuildPermission.ValueSet => GuildPermission.ValueSet): Future[ReifiedChar[GuildRole]] = {
      db.run((for (
        (char, guildId) <- findCharWithGuild(userId);
        _ <- checkGuildPermission(guildId, GuildPermission.PermissionsEdit);
        role <- GuildsDao.getRole(guildId, userId);
        newPermissions = modification(role.permissions);
        _ <- GuildsDao.updatePermissions(guildId, userId, newPermissions);
        newRole <- GuildsDao.getRole(guildId, userId)
      ) yield ReifiedChar(char, newRole)).transactionally)
    }

    private def findCharWithGuild(userId: Int): DBIO[(SimpleChar, Int)] = {
      CharsDao.findCharById(userId).map(optRow => optRow.map { row =>
        val guildId = row.guildId.getOrElse(throw new ServiceException("Пользователь не нахрдится в гильдии"))
        (SimpleChar(row.id, row.name, row.gameClass), guildId)
      }.getOrElse(throw new ServiceException("Пользователь не найден")))
    }
  }

  def transferLeadership(guildName: String, newLeaderName: String): Future[Unit] = {
    val action = for (
      optGuildId <- GuildsDao.findGuildByName(guildName).map(_.map(_.id));
      guildId = optGuildId.getOrElse(throw new ServiceException("Нет такой гильдии"));

      userIds <- Chars
        .filter(t => t.guildId.isDefined && t.guildId === guildId && t.name === newLeaderName)
        .map(_.id).result;

      _ <- userIds match {
        case _ +: _ +: _ => dbioFail("В гильдии несколько людей с таким именем")
        case Seq() => dbioFail("В гильдии нет людей с таким именем")
        case leaderId +: _ => GuildsDao.transferLeadership(guildId, leaderId)
      }
    ) yield ()

    db.run(action)
  }
}

