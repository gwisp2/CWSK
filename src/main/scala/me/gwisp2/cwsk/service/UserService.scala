package me.gwisp2.cwsk.service

import java.sql.Timestamp
import java.util.concurrent.ConcurrentHashMap

import com.google.inject.Inject
import com.typesafe.scalalogging.StrictLogging
import info.mukel.telegrambot4s.models.User
import me.gwisp2.cwsk.bot.parsed.HeroMessage
import me.gwisp2.cwsk.cwapi.ApiResponse
import me.gwisp2.cwsk.cwapi.methods.RequestProfile
import me.gwisp2.cwsk.cwapi.methods.RequestProfile.Profile
import me.gwisp2.cwsk.dto._
import me.gwisp2.cwsk.model.Tables._
import me.gwisp2.cwsk.model._
import me.gwisp2.cwsk.service.UserService.ProfileUpdated
import slick.jdbc.JdbcBackend.Database
import slick.jdbc.MySQLProfile.api._

import scala.concurrent.Future
import scala.util.{Failure, Success}

class UserService @Inject()(api: CwApiService, db: Database) extends GenericService with StrictLogging {
  val profileUpdatedEvent = new EventHub[ProfileUpdated]()
  val userIdsDontInvalidateEquipment = new ConcurrentHashMap[Int, AnyRef]()

  case class auth()(implicit securityContext: SecurityContext) {
    def getFullGuildInfo(): Future[FullGuildInfo] = {
      for (optInfo <- getGuildInfoForUser(securityContext.effectiveUserId)) yield optInfo match {
        case Some(info) => info
        case None => throw new ServiceException("Ты не состоишь в гильдии")
      }
    }
  }

  def updateUserAndMarkAsActive(tgUser: User): Future[Unit] = {
    db.run(for (
      existing <- Users.filter(_.id === tgUser.id).result.headOption;
      _ <- existing match {
        case Some(_) => Users.filter(_.id === tgUser.id)
          .map(t => (t.firstName, t.lastName, t.username, t.botIsBlocked, t.isDeactivated))
          .update((tgUser.firstName, tgUser.lastName, tgUser.username, false, false))
        case None =>
          Users += UsersRow(tgUser.id, tgUser.firstName, tgUser.lastName, tgUser.username, "User")
      }
    ) yield ()).map(_ => ())
  }

  def markThatUserBlockedTheBot(userId: Int): Future[Unit] = {
    db.run(
      Users.filter(_.id === userId).map(_.botIsBlocked).update(true)
    ).map(_ => ())
  }

  def markThatUserIsDeactivated(userId: Int): Future[Unit] = {
    db.run(
      Users.filter(_.id === userId).map(_.isDeactivated).update(true)
    ).map(_ => ())
  }

  def checkIsAdmin(implicit sc: SecurityContext): Future[Unit] = {
    db.run(for (
      optRole <- Users.filter(_.id === sc.effectiveUserId).map(_.role).result.headOption;
      _ <- dbioFailIf(!optRole.contains("Admin"), "Нет доступа")
    ) yield ())
  }

  def getCharWithPermissionCheck(viewerId: Int, charId: Int): Future[Option[FullChar]] = {
    val action = for (
      sameGuild <- inSameGuild(viewerId, charId);
      fullChar <- if (sameGuild) getCharsAction(Some(charId))
      else DBIO.failed(new ServiceException("Нет доступа"))
    ) yield fullChar.headOption
    db.run(action)
  }

  def getGuildPog(optGuildId: Option[Int])(implicit sc: SecurityContext): Future[ReifiedGuild[Int]] = {
    db.run(for (
      guildId <- getGuildId(optGuildId);
      _ <- checkGuildPermission(guildId, GuildPermission.GuildPog);
      r <- GuildsDao.reifiedGuildMembers(guildId, _.pog)
    ) yield r)
  }

  private def getGuildId(optGuildId: Option[Int])(implicit sc: SecurityContext) = {
    optGuildId match {
      case Some(gid) => DBIO.successful(gid)
      case _ =>
        CharsDao.findCharById(sc.effectiveUserId).map(_.flatMap(_.guildId).getOrElse(throw new ServiceException("Нет гильдии")))
    }
  }

  private def inSameGuild(id1: Int, id2: Int) = {
    for (
      optOptGuild1 <- Chars.filter(_.id === id1).map(_.guildId).result.headOption;
      optOptGuild2 <- Chars.filter(_.id === id2).map(_.guildId).result.headOption
    ) yield (optOptGuild1, optOptGuild2) match {
      case (Some(Some(gid1)), Some(Some(gid2))) if gid1 == gid2 => true
      case _ => false
    }
  }

  def getChar(id: Int): Future[Option[FullChar]] = {
    getChars(Some(id)).map(_.headOption)
  }

  def getGuildInfoForUser(userId: Int): Future[Option[FullGuildInfo]] = {
    val action = for (
      optChar <- Chars.filter(_.id === userId).result.headOption;
      optOptGuildInfo <- DBIO.sequenceOption(
        for (c <- optChar; gid <- c.guildId) yield Guilds.filter(_.id === gid).result.headOption
      );
      optGuildInfo = optOptGuildInfo.flatten;
      optMembers <- DBIO.sequenceOption(
        for (c <- optChar; gid <- c.guildId) yield guildMembers(gid)
      )
    ) yield for (
      guildInfo <- optGuildInfo;
      members <- optMembers
    ) yield new ReifiedGuild[ShortChar](
      GuildsDao.extractGuildInfo(guildInfo), members.map(c => new ReifiedChar[ShortChar](
        SimpleChar(c.userId, c.name, c.gameClass), c
      ))
    )

    db.run(action)
  }

  private def guildMembers(guildId: Int) = {
    val query = for (
      (c, optRole) <- Chars joinLeft GuildRoles on ((c, gr) => c.guildId === gr.guildId && c.id === gr.userId)
      if c.guildId.isDefined && c.guildId === guildId
    ) yield (c, optRole)

    for (
      rows <- query.result
    ) yield rows.map { case (c, optRole) =>
      val guildRole = GuildsDao.rowToRole(optRole)
      ShortChar(c.id, c.name, c.gameClass, c.level, c.exp, c.equipmentIsValid, guildRole)
    }
  }

  def getChars(ids: Traversable[Int]): Future[Traversable[FullChar]] = {
    /* Equipment, guild, class */
    val action = getCharsAction(ids)
    db.run(action)
  }

  private def getCharsAction(ids: Traversable[Int]) = {
    for (
      chars <- CharsDao.getChars(ids)
    ) yield chars.map { case CharsDao.CharData(char, optGuild, equipment) =>
      FullChar(
        char.id, optGuild.map(g => GuildsDao.extractGuildInfo(g)), char.name, char.gameClass,
        char.castle, char.level, char.exp, char.atk, char.`def`,
        equipment, char.equipmentIsValid,
        for (icon <- char.petIcon; name <- char.petFullName; lvl <- char.petLevel) yield PetInfo(icon, name, lvl),
        char.updatedAt.toInstant
      )
    }
  }

  def updateHero(userId: Int, hero: HeroMessage): Future[Unit] = {
    val future = db.run(for (
      existingChar <- CharsDao.findCharById(userId);
      _ <- existingChar match {
        case Some(c) if hero.name.endsWith(c.name) =>
          DBIO.seq(
            CharsDao.updateEquipment(userId, hero.equipment, TimeUtil.now),
            CharsDao.updatePet(userId, hero.pet)
          )
        case Some(_) => throw new ServiceException("Я не могу удостовериться, что это твой <code>/hero</code>. Сначала обнови профиль (/update).")
        case _ => throw new ServiceException("Сначала обнови профиль с помощью /update.");
      }
    ) yield ())
    future.foreach { _ =>
      // Atk/def may be changed... update profile now
      userIdsDontInvalidateEquipment.put(userId, this)
      api.send(userId, RequestProfile.Request())
    }
    future
  }

  api.responseEvent.registerListener {
    case ApiResponse.Simple(_, _, RequestProfile.Response(userId, userProfile)) =>
      updateProfile(userId, userProfile).onComplete {
        case Success(event) => profileUpdatedEvent.fire(event)
        case Failure(e) => logger.warn("Exception while updating profile", e)
      }
  }

  private def updateProfile(userId: Int, profile: Profile): Future[ProfileUpdated] = {
    val now = new Timestamp(System.currentTimeMillis())
    val action = for (
      oldChar <- CharsDao.findCharById(userId);
      needToInvalidateEquipment = (userIdsDontInvalidateEquipment.remove(userId) == null) &&
        oldChar.exists(c => (c.atk != profile.atk || c.`def` != profile.`def`) && c.equipmentIsValid);
      guildId <- GuildsDao.findCreateGuildId(profile.castle, profile.guild_tag, profile.guild);
      _ <- CharsDao.createOrUpdateChar(userId, profile.userName, profile.`class`, profile.`castle`,
        profile.atk, profile.`def`, profile.exp, profile.lvl, profile.pouches.getOrElse(0), guildId, now);
      _ <- if (needToInvalidateEquipment) CharsDao.changeEquipmentValidity(userId, newValidity = false)
      else DBIO.successful(());
      _ <- ExpHistory += ExpHistoryRow(userId, profile.exp, now)
    ) yield ProfileUpdated(userId, needToInvalidateEquipment)

    db.run(action.transactionally)
  }
}

object UserService {

  case class ProfileUpdated(userId: Int, equipmentInvalidated: Boolean)

}