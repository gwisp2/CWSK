package me.gwisp2.cwsk.service

import java.sql.Timestamp
import java.time.Instant

import com.google.inject.Inject
import com.typesafe.scalalogging.StrictLogging
import me.gwisp2.cwsk.cwapi.messages.{CwApiMessage, DealsPayload}
import me.gwisp2.cwsk.dto.items.StockItemType
import me.gwisp2.cwsk.model.StockDao
import me.gwisp2.cwsk.model.Tables._
import me.gwisp2.cwsk.service.ItemPricesService.ItemPrices
import me.gwisp2.scheduler.{Schedule, ScheduledAction, Scheduler}
import slick.jdbc.JdbcBackend.Database
import slick.jdbc.MySQLProfile.api._

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.{Failure, Success}

class ItemPricesService @Inject()(
                                   db: Database,
                                   cwApiService: CwApiService,
                                   scheduler: Scheduler
                                 ) extends GenericService with StrictLogging {
  @volatile
  private var latestPricesMap = Map[String, Int]()

  {
    cwApiService.dealsEvent.registerListener { case deal => handleDeal(deal) }
    scheduler.runOnSchedule(new ScheduledAction {
      override val name: String = "Refresh prices"
      override val schedule: Schedule = Schedule.Periodically(Instant.now(), 4.hour)
      override def run(): Unit = refreshPrices()
    })
  }

  def getPrices: Future[ItemPrices] = {
    db.run(for (
      namePricePairs <- Items.filter(_.priceGold.isDefined).map(t => (t.name, t.priceGold)).result
    ) yield {
      val namePricePairsMap = namePricePairs.collect { case (name, Some(price)) => (name, price) }.toMap
      new ItemPrices {
        override def getPrice(itemName: String): Option[BigDecimal] = namePricePairsMap.get(itemName)
      }
    })
  }

  def getLatestPrices: Map[String, Int] = {
    latestPricesMap
  }

  private def handleDeal(dealMessage: CwApiMessage[DealsPayload]): Unit = {
    latestPricesMap = latestPricesMap.updated(dealMessage.payload.item, dealMessage.payload.price)

    db.run(for (
      itemId <- StockDao.findItemId(dealMessage.payload.item);
      _ <- DealsHistory += DealsHistoryRow(
        sellerId = dealMessage.payload.sellerId,
        buyerId = dealMessage.payload.buyerId,
        itemId = itemId,
        qty = dealMessage.payload.qty,
        price = dealMessage.payload.price,
        timestamp = new Timestamp(dealMessage.date.getTime)
      )
    ) yield ())
  }

  private def refreshPrices(): Unit = {
    val dayAgo = new Timestamp(System.currentTimeMillis() - 24L * 3600 * 1000)
    val latestPricesMapFuture = db.run(for (
      _ <- DealsHistory.filter(_.timestamp < dayAgo).delete;
      _ <- sql"""UPDATE items i SET price_gold = (select sum(price*qty)/sum(qty) from deals_history dh where dh.item_id = i.id);""".asUpdate;
      pricesPairs <- sql"""SELECT i.name, (SELECT price FROM deals_history dh WHERE dh.item_id = i.id ORDER BY dh.timestamp DESC LIMIT 1) FROM items i;""".as[(String, Option[Int])];
      latestPricesAsMap = pricesPairs.collect {
        case (name, Some(price)) => (name, price)
      }.toMap
    ) yield latestPricesAsMap)

    latestPricesMapFuture.onComplete {
      case Success(r) => latestPricesMap = r
      case Failure(exception) => logger.warn("Exception refreshing prices", exception)
    }
  }
}

object ItemPricesService {

  trait ItemPrices {
    def getPrice(itemName: String): Option[BigDecimal]

    def getPrice(itemType: StockItemType): Option[BigDecimal] = getPrice(itemType.name)
  }

  object ItemPrices {
    val empty: ItemPrices = (_: String) => None

    def fromDoubleMap(map: Map[String, Double]): ItemPrices = {
      itemName: String => map.get(itemName).map(BigDecimal.valueOf)
    }
  }

}