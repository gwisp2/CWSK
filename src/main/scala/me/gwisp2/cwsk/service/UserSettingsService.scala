package me.gwisp2.cwsk.service

import com.google.inject.Inject
import me.gwisp2.cwsk.model.Tables._
import me.gwisp2.cwsk.model.extension.TableQueryExtensions._
import slick.jdbc.MySQLProfile.api._

import scala.concurrent.Future

class UserSettingsService @Inject()(db: Database) extends GenericService {

  import UserSettingsService._

  val settingsKeyStockDiff: SettingsKey[Boolean] = new BooleanSettingsKey {
    override def set(value: Boolean, row: UserSettingsRow): UserSettingsRow = row.copy(stockDiffEnabled = value)

    override def get(row: UserSettingsRow): Boolean = row.stockDiffEnabled

    override val name: String = "stock_diff"
    override val displayName: String = "Изменение склада"
    override val description: String = "Присылать изменение склада после битвы"
  }
  val settingsSexNotify: SettingsKey[Boolean] = new BooleanSettingsKey {
    override def set(value: Boolean, row: UserSettingsRow): UserSettingsRow = row.copy(sexSellNotificationEnabled = value)

    override def get(row: UserSettingsRow): Boolean = row.sexSellNotificationEnabled

    override val name: String = "sex_notify"
    override val displayName: String = "Продажи на бирже"
    override val description: String = "Присылать уведомления о продажах на бирже"
  }

  val settingsKeys = Seq(
    settingsKeyStockDiff,
    settingsSexNotify
  )

  case class auth()(implicit sc: SecurityContext) {
    def getSettings: Future[SettingsSnapshot] = {
      val action = for (
        row <- getUserSettingsOrDefault(sc.effectiveUserId)
      ) yield fromRow(row)

      db.run(action)
    }

    def changeSettings[T](newValue: SettingsValue[T]): Future[SettingsSnapshot] = {
      val action = for (
        row <- getUserSettingsOrDefault(sc.effectiveUserId);
        mutatedRow = newValue.key.set(newValue.value, row);
        _ <- UserSettings.insertOrUpdate(mutatedRow)
      ) yield fromRow(mutatedRow)

      db.run(action.transactionally)
    }
  }

  def getSettingsValue[T](userId: Int, key: SettingsKey[T]): Future[T] = {
    val action = for (
      row <- getUserSettingsOrDefault(userId)
    ) yield key.get(row)

    db.run(action)
  }

  private def fromRow(row: UserSettingsRow) =
    SettingsSnapshot(settingsKeys.map(key => SettingsValue(key, key.get(row))))

  private val getUserSettings = UserSettings.findOneBy(_.userId)

  private def getUserSettingsOrDefault(userId: Int) =
    getUserSettings(userId).map(_.getOrElse(UserSettingsRow(userId = userId)))
}

object UserSettingsService {

  trait SettingsKey[T] {
    def set(value: T, row: UserSettingsRow): UserSettingsRow

    def get(row: UserSettingsRow): T

    def name: String

    def displayName: String

    def description: String

    def valueToHtml(value: T): String

    def commands(value: T): Seq[Command[T]]

    def allCommands: Seq[Command[T]]
  }

  trait Command[T] {
    def name: String

    def getValue(arg: String): T
  }

  case class SimpleCommand[T](name: String, value: T) extends Command[T] {
    override def getValue(arg: String): T = value
  }

  trait BooleanSettingsKey extends SettingsKey[Boolean] {
    def commandDisable = SimpleCommand(s"${name}_disable", false)

    def commandEnable = SimpleCommand(s"${name}_enable", true)

    override def commands(value: Boolean) = Seq(if (value) commandDisable else commandEnable)

    override def allCommands = Seq(commandDisable, commandEnable)

    override def valueToHtml(value: Boolean): String = if (value) "✅" else "🚫"
  }

  case class SettingsValue[T](key: SettingsKey[T], value: T)

  case class SettingsSnapshot(settings: Seq[SettingsValue[_]])

}
