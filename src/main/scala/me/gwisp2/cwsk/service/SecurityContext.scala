package me.gwisp2.cwsk.service

trait SecurityContext {
  def effectiveUserId: Int
}
