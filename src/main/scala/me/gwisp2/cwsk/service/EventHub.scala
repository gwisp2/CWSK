package me.gwisp2.cwsk.service

import com.typesafe.scalalogging.StrictLogging

import scala.collection.mutable.ArrayBuffer

class EventHub[T] extends StrictLogging {
  type Listener = PartialFunction[T, Unit]
  private val listeners = ArrayBuffer[Listener]()

  def registerListener(listener: Listener): Unit = {
    listeners += listener
  }

  def fire(event: T): Unit = {
    listeners.foreach(runListener(_, event))
  }

  private def runListener(listener: Listener, event: T) {
    try {
      if (listener.isDefinedAt(event)) {
        listener(event)
      }
    } catch {
      case e: Throwable => logger.warn("Exception while running response listener", e)
    }
  }
}
