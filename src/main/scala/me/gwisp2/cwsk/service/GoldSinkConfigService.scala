package me.gwisp2.cwsk.service

import com.google.inject.Inject
import com.typesafe.scalalogging.StrictLogging
import me.gwisp2.cwsk.dto.{GuildPermission, ServiceException}
import me.gwisp2.cwsk.model.types.GoldSinkRule
import me.gwisp2.cwsk.model.{CharsDao, GoldSinkRulesDao, GuildsDao, StockDao}
import slick.jdbc.JdbcBackend.Database
import slick.jdbc.MySQLProfile.api._

import scala.concurrent.Future


class GoldSinkConfigService @Inject()(db: Database) extends GenericService with StrictLogging {
  def getUsersWithAutosinkEnabled(timeslot: Option[Int]): Future[Traversable[Int]] = {
    db.run(GoldSinkRulesDao.getUsersWithAutosinkEnabled(timeslot))
  }

  def updateUserAutosinkStatus(userId: Int, status: Boolean): Future[Unit] = {
    val action = for (
      _ <- GoldSinkRulesDao.changeAutosinkStatus(userId, status)
    ) yield ()
    db.run(action)
  }

  def updateUserSinkLimit(userId: Int, newLimit: Option[Int]): Future[Unit] = {
    val action = for (
      _ <- GoldSinkRulesDao.changeSinkLimit(userId, newLimit)
    ) yield ()
    db.run(action)
  }

  def updateAutosinkTimeslot(userId: Int, newTimeslot: Option[Int]): Future[Unit] = {
    val action = for (
      _ <- GoldSinkRulesDao.changeSinkTimeslot(userId, newTimeslot)
    ) yield ()
    db.run(action)
  }

  def updateUserRule(userId: Int, ruleText: String): Future[Option[GoldSinkRule]] = {
    val action = (for (
      config <- parseRule(ruleText);
      ruleId <- DBIO.sequenceOption(config.map(GoldSinkRulesDao.addRule));
      _ <- GoldSinkRulesDao.setRuleForUser(userId, ruleId)
    ) yield config).transactionally
    db.run(action)
  }

  def updateGuildRule(userId: Int, configText: String): Future[Option[GoldSinkRule]] = {
    val action = (for (
      guildId <- CharsDao.getGuildId(userId).map(_.getOrElse(throw new ServiceException("Нет гильдии")));
      guildRole <- GuildsDao.getRole(guildId, userId);
      _ <- dbioFailIf(!guildRole.hasPermission(GuildPermission.GsGuild), "Недостаточно прав.");
      config <- parseRule(configText);
      ruleId <- DBIO.sequenceOption(config.map(GoldSinkRulesDao.addRule));
      _ <- GoldSinkRulesDao.setRuleForGuild(guildId, ruleId)
    ) yield config).transactionally
    db.run(action)
  }

  def getUserSettings(userId: Int): Future[(Boolean, Option[Int], Option[Int], Option[GoldSinkRule], Option[GoldSinkRule])] = {
    db.run(for (
      z <- GoldSinkRulesDao.getRuleForUserAndGuild(userId);
      autosinkEnabled <- GoldSinkRulesDao.getAutosinkStatus(userId)
    ) yield (autosinkEnabled, z._1, z._2, z._3, z._4))
  }

  private def parseRule(text: String) = {
    for (
      itemSearcher <- StockDao.createItemSearcher()
    ) yield {
      try {
        val rule = GoldSinkRule.parseText(text, itemSearcher.findType)
        if (rule.nonEmpty) Some(rule) else None
      } catch {
        case e: GoldSinkRule.ParserException =>
          val escapedErrorMsg = xml.Utility.escape(e.getMessage)
          val escapedLine = xml.Utility.escape(e.line)
          throw new ServiceException(s"<pre>$escapedLine</pre>$escapedErrorMsg")
      }
    }
  }
}

