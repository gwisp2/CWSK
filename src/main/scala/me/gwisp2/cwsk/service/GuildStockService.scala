package me.gwisp2.cwsk.service

import com.google.inject.Inject
import com.typesafe.scalalogging.StrictLogging
import me.gwisp2.cwsk.dto._
import me.gwisp2.cwsk.dto.items.{ItemSet, StockItem, StockItemType}
import me.gwisp2.cwsk.model._
import slick.jdbc.JdbcBackend.Database
import slick.jdbc.MySQLProfile.api._

import scala.concurrent.Future

class GuildStockService @Inject()(
                                   db: Database
                                 ) extends GenericService with StrictLogging {

  case class auth()(implicit sc: SecurityContext) {
    def getGuildStock: Future[ItemSet] = {
      val action = for (
        guildId <- findGuildId(sc.effectiveUserId);
        _ <- checkGuildPermission(guildId, GuildPermission.GuildStock);
        rows <-
          sql"""SELECT i.ingame_id, i.name, SUM(sc.amount)
                FROM (stock_contents sc INNER JOIN chars c on sc.user_id = c.id INNER JOIN items i on i.id = sc.item_id)
                WHERE c.guild_id = $guildId GROUP BY sc.item_id ORDER BY SUM(sc.amount) DESC""".as[(Option[String], String, Int)]
      ) yield new ItemSet(rows.map { case (ingameId, name, quantity) =>
        StockItem(StockItemType(ingameId, name), quantity)
      })
      db.run(action)
    }

    def findItems(itemName: String): Future[ReifiedGuild[ItemSet]] = {
      val action = for (
        guildId <- findGuildId(sc.effectiveUserId);
        _ <- checkGuildPermission(guildId, GuildPermission.GuildStock);
        itemSearcher <- StockDao.createItemSearcher();
        guild <- GuildsDao.reifiedGuildMembers(guildId, _ => ());
        itemIds = itemSearcher.findMany(itemName).map(_.id);
        _ <- dbioFailIf(itemIds.isEmpty, "Мне неизвестны ресурсы с таким названием или кодом");
        stocksMap <- StockDao.loadStocks(guild.memberIds, itemIds)
      ) yield {
        guild.replaceValues(rc => stocksMap(rc.char.userId))
      }

      db.run(action)
    }

    private def findGuildId(userId: Int): DBIO[Int] = {
      CharsDao.findCharById(userId).map(optRow => optRow.map { row =>
        row.guildId.getOrElse(throw new ServiceException("Пользователь не нахрдится в гильдии"))
      }.getOrElse(throw new ServiceException("Пользователь не найден")))
    }
  }

}

