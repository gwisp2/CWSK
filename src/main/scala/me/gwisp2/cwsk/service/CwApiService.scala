package me.gwisp2.cwsk.service

import java.util.concurrent.ConcurrentHashMap

import com.google.inject.Inject
import com.typesafe.scalalogging.StrictLogging
import kamon.Kamon
import me.gwisp2.cwsk.cwapi.messages.{CwApiMessage, DealsPayload}
import me.gwisp2.cwsk.cwapi.methods.OperationType.OperationType
import me.gwisp2.cwsk.cwapi.methods._
import me.gwisp2.cwsk.cwapi.{ApiRequest, ApiResponse, CwApi}
import me.gwisp2.cwsk.dto.UserCwapiInfo
import me.gwisp2.cwsk.model.Tables.CwapiTokens
import me.gwisp2.cwsk.model._
import slick.jdbc.JdbcBackend.Database
import slick.jdbc.MySQLProfile.api._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

class CwApiService @Inject()(
                              api: CwApi,
                              db: Database,
                            ) extends StrictLogging {
  type ResponseListener = PartialFunction[ApiResponse, Unit]
  type UserId = Int

  private val uuidToRequestedOp = new ConcurrentHashMap[String, OperationType]()
  private val sendCodeActions = new ConcurrentHashMap[Int, String => Unit]()

  val responseEvent = new EventHub[ApiResponse]
  val dealsEvent = new EventHub[CwApiMessage[DealsPayload]]
  val newTokenListenerEvent = new EventHub[UserId]
  val tokenInvalidatedEvent = new EventHub[UserId]
  val newPermissionEvent = new EventHub[(UserId, OperationType)]

  {
    responseEvent.registerListener {
      case ApiResponse.Simple(_, _, r: CreateAuthCode.Response) =>
        sendCodeActions.put(r.userId, code => send(GrantToken.Request(r.userId, code)))
      case ApiResponse.Simple(_, _, r: GrantToken.Response) => saveToken(r)
      case ApiResponse.WithUUID(_, _, uuid, r: AuthAdditionalOperation.Response) =>
        savePermissionRequest(uuid, r)
      case ApiResponse.Simple(_, _, r: GrantAdditionalOperation.Response) => savePermission(r)
      case ApiResponse.Simple(_, _, r: ApiMethod.InvalidToken) => invalidateToken(r)
    }

    responseEvent.registerListener {
      case r =>
        val actionName = r.actionOption.getOrElse("no-action")
        Kamon.counter(s"cwapi.response.$actionName").increment()
        Kamon.counter(s"cwapi.response.total").increment()
    }
    dealsEvent.registerListener {
      case _ =>
        Kamon.counter(s"cwapi.deals").increment()
    }
  }

  def start(): Unit = {
    api.listenForResponses(responseEvent.fire)
    api.listenForDeals(dealsEvent.fire)
  }

  def send(payload: ApiRequest.Payload.Simple) {
    Kamon.counter(s"cwapi.requests").increment()

    api.sendRequest(payload.toRequest)
  }

  def send(userId: UserId, payload: ApiRequest.Payload.WithToken) {
    Kamon.counter(s"cwapi.requests").increment()

    for (tokenOpt <- db.run(CwApiTokensDao.getToken(userId))) {
      tokenOpt match {
        case Some(token) => api.sendRequest(payload.toRequest(token))
        case _ => /* Do nothing */
      }
    }
  }

  def sendCode(userId: UserId, code: String): Unit = {
    val codeAction = sendCodeActions.remove(userId)
    if (codeAction != null) {
      codeAction(code)
    }
  }

  def getUserInfo(userId: UserId): Future[UserCwapiInfo] = {
    db.run(CwApiTokensDao.getUserInfo(userId))
  }

  def getUserIdByCwId(cwId: String): Future[Option[Int]] = {
    db.run(CwapiTokens.filter(_.cwId === cwId).map(_.userId).result.headOption)
  }

  def invalidateToken(r: ApiMethod.InvalidToken): Unit = {
    val f = db.run((
      for (
        token <- CwapiTokens.filter(_.token === r.token).forUpdate.result.headOption;
        _ <- CwapiTokens.filter(_.token === r.token).delete
      ) yield token.map(_.userId)
      ).transactionally)

    f.onComplete {
      case Success(Some(userId)) => tokenInvalidatedEvent.fire(userId)
      case Success(_) => /* Token was already invalidated */
      case Failure(e) => logger.warn("Error invalidating CW API token", e)
    }
  }

  private def saveToken(tokenResponse: GrantToken.Response): Unit = {
    db.run(CwApiTokensDao.saveToken(
      tokenResponse.userId, tokenResponse.id, tokenResponse.token
    )).onComplete {
      case Success(_) => newTokenListenerEvent.fire(tokenResponse.userId)
      case Failure(e) => logger.warn("Error saving CW API token", e)
    }
  }

  private def savePermissionRequest(uuid: String, r: AuthAdditionalOperation.Response): Unit = {
    sendCodeActions.put(r.userId, code => send(r.userId, GrantAdditionalOperation.Request(uuid, code)))
    uuidToRequestedOp.put(uuid, r.operation)
  }

  private def savePermission(grantResponse: GrantAdditionalOperation.Response): Unit = {
    val requestedOp = uuidToRequestedOp.get(grantResponse.requestId)

    if (requestedOp != null) {
      db.run(CwApiTokensDao.addPermission(grantResponse.userId, requestedOp)).onComplete {
        case Success(_) => newPermissionEvent.fire((grantResponse.userId, requestedOp))
        case Failure(e) => logger.warn("Error saving CW API permission", e)
      }
    }
  }
}