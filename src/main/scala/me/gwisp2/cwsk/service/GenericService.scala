package me.gwisp2.cwsk.service

import me.gwisp2.cwsk.dto.{GuildPermission, ServiceException}
import me.gwisp2.cwsk.model.{CharsDao, GuildsDao}
import slick.jdbc.MySQLProfile.api._

import scala.concurrent.ExecutionContextExecutor

abstract class GenericService {
  type PermissionCheckType = DBIO[Unit]

  implicit val executionContext: ExecutionContextExecutor = scala.concurrent.ExecutionContext.global

  protected def dbioFailIf(cond: Boolean, message: String): DBIOAction[Unit, NoStream, Effect] =
    if (cond) dbioFail(message)
    else DBIO.successful(())

  protected def dbioFail(message: String): DBIOAction[Nothing, NoStream, Effect] =
    DBIO.failed(new ServiceException(message))

  protected def checkInGuild(guildId: Int)(implicit sc: SecurityContext): PermissionCheckType =
    checkGuildPermission(guildId, GuildPermission.ValueSet())

  protected def checkGuildPermission(guildId: Int, permission: GuildPermission.Value)(implicit sc: SecurityContext): PermissionCheckType =
    checkGuildPermission(guildId, GuildPermission.ValueSet(permission))

  protected def checkGuildPermission(guildId: Int, requiredPermissions: GuildPermission.ValueSet)(implicit sc: SecurityContext): PermissionCheckType = {
    for (
      optGuildId <- CharsDao.findCharById(sc.effectiveUserId).map(_.flatMap(_.guildId));
      _ <- optGuildId match {
        case Some(gid) if guildId == gid =>
          for (
            role <- GuildsDao.getRole(gid, sc.effectiveUserId);
            _ <- dbioFailIf(!role.hasPermissions(requiredPermissions), "Недостаточно прав")
          ) yield ()
        case _ => dbioFail("Ты не находишься в этой гильдии")
      }
    ) yield ()
  }
}
