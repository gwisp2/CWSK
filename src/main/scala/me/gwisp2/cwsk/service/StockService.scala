package me.gwisp2.cwsk.service

import com.google.inject.Inject
import com.typesafe.scalalogging.StrictLogging
import me.gwisp2.cwsk.cwapi.ApiResponse
import me.gwisp2.cwsk.cwapi.methods.RequestStock
import me.gwisp2.cwsk.dto._
import me.gwisp2.cwsk.dto.items.{ItemSet, StockItemType}
import me.gwisp2.cwsk.model._
import me.gwisp2.cwsk.util.Emojis
import slick.jdbc.JdbcBackend.Database
import slick.jdbc.MySQLProfile.api._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

class StockService @Inject()(api: CwApiService, db: Database) extends StrictLogging {
  val stockUpdatedEvent = new EventHub[SaveStockResult]()

  case class auth()(implicit sc: SecurityContext) {
    def getStock: Future[ItemSet] = {
      for (
        items <- db.run(StockDao.loadStock(sc.effectiveUserId))
      ) yield ItemSet(items.toSet)
    }
  }

  api.responseEvent.registerListener {
    case ApiResponse.Simple(_, _, RequestStock.Response(userId, stock)) =>
      saveStock(userId, stock).onComplete {
        case Success(result) => stockUpdatedEvent.fire(result)
        case Failure(e) => logger.warn("Exception while saving stock", e)
      }
  }

  def updateItemIds(nameIdMap: Map[String, String]): Future[Traversable[String]] = {
    val itemsToSaveId = nameIdMap.filterKeys(shouldSaveItemId)
    db.run(StockDao.updateItemIngameIds(itemsToSaveId).transactionally)
  }

  def shouldSaveItemId(name: String): Boolean = {
    !name.startsWith(Emojis.HighVoltage)
  }

  def getKnownItems(): Future[Traversable[StockItemType]] = {
    db.run(StockDao.loadAllItems())
  }

  private def saveStock(userId: Int, stock: Map[String, Int]): Future[SaveStockResult] = {
    db.run((for (
      oldStock <- StockDao.loadStock(userId).map(ItemSet.apply);
      _ <- StockDao.saveStock(userId, stock);
      names <- StockDao.findItemNamesWithoutIngameId(stock.keySet);
      newStock <- StockDao.loadStock(userId).map(ItemSet.apply)
    ) yield SaveStockResult(userId, newStock, oldStock, names.filter(shouldSaveItemId))).transactionally)
  }
}