package me.gwisp2.cwsk.service

import akka.NotUsed
import akka.stream.scaladsl.Source
import com.google.inject.Inject
import com.typesafe.scalalogging.StrictLogging
import me.gwisp2.cwsk.cwapi.ApiResponse
import me.gwisp2.cwsk.cwapi.methods.RequestProfile.Profile
import me.gwisp2.cwsk.cwapi.methods.{RequestProfile, RequestStock, WantToBuy}
import me.gwisp2.cwsk.dto.items.ItemSet
import me.gwisp2.cwsk.model.types.GoldSinkRule
import me.gwisp2.cwsk.service.GoldSinkService.GoldSinkHistory
import me.gwisp2.cwsk.util.{ProgressSource, PromiseMap}
import me.gwisp2.scheduler.Scheduler

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

class GoldSinkService @Inject()(api: CwApiService, stockService: StockService,
                                goldSinkConfigService: GoldSinkConfigService,
                                itemPricesService: ItemPricesService,
                                scheduler: Scheduler)
  extends StrictLogging {
  private val profilePromises = new PromiseMap[Int, Profile]
  private val wtbPromises = new PromiseMap[Int, Either[ApiResponse.Code, Unit]]
  private val stockPromises = new PromiseMap[Int, ItemSet]

  api.responseEvent.registerListener {
    case ApiResponse.Simple(_, _, RequestProfile.Response(userId, userProfile)) =>
      profilePromises.fulfill(userId, userProfile)
    case ApiResponse.Simple(_, _, r: WantToBuy.Response) =>
      wtbPromises.fulfill(r.userId, Right(()))
    case ApiResponse.Simple(_, code, r: WantToBuy.ResponseFail) =>
      wtbPromises.fulfill(r.userId, Left(code))
  }

  stockService.stockUpdatedEvent.registerListener { case event =>
    stockPromises.fulfill(event.userId, event.newStock)
  }

  def startAutomaticGoldSink(timeslot: Option[Int]): Future[Map[Int, Source[GoldSinkHistory, NotUsed]]] = {
    for (userIds <- goldSinkConfigService.getUsersWithAutosinkEnabled(timeslot)) yield {
      (for (userId <- userIds) yield userId -> startGoldSink(userId)).toMap
    }
  }

  def startGoldSink(userId: Int): Source[GoldSinkHistory, NotUsed] = {
    ProgressSource.create { historyConsumer: ProgressSource.ProgressConsumer[GoldSinkHistory] =>
      var history = GoldSinkHistory(Vector())

      def pushHistory(line: String, completed: Boolean = false): Unit = {
        history = history.withNewLine(line)
        historyConsumer.consume(history)
        if (completed) {
          historyConsumer.complete()
        }
      }

      for ((autosink, sinkLimit, timeslot, userConfig, guildConfig) <- goldSinkConfigService.getUserSettings(userId)) {
        val combined = GoldSinkRule.concat(userConfig ++ guildConfig)

        if (combined.nonEmpty) {
          val realPrices = itemPricesService.getLatestPrices
          val withRealPrices = combined.items.flatMap { item =>
            val lastSellPrice = realPrices.getOrElse(item.name, item.price)
            if (lastSellPrice > item.price) {
              /* Real price is higher than desired, ignore this rule */
              Seq()
            } else if (lastSellPrice == item.price) {
              /* Real price is exactly the same as desired */
              Seq(item.copy(exactPrice = true))
            } else {
              /* Last sell price is less than desired */
              /* Real price can slightly more than the last sell price... */
              val newPricesToTry = lastSellPrice to Math.min(lastSellPrice + 3, item.price - 1)
              newPricesToTry.map(priceToTry => item.copy(price = priceToTry, exactPrice = true)) ++ Seq(item)
            }
          }.toList

          pushHistory("Обновляю сток и профиль")
          updateGold(userId).foreach { gold =>
            if (sinkLimit.exists(_ < gold)) {
              /* Sink limit is too low */
              pushHistory("Количество голды превышает лимит (<code>/gs_limit</code>)", completed = true)
            } else {
              updateStock(userId).foreach { stock =>
                continueGoldSink(userId, gold, withRealPrices, stock, Map.empty, pushHistory)
              }
            }
          }
        } else {
          pushHistory(
            """
              |Не задан список ресурсов для закупки. Используй /gs.
              |Пример:
              |<pre>/gs
              |Charcoal 4
              |Coal 2
              |Powder 1
              |</pre>
              |""".stripMargin, completed = true)
        }
      }
    }
  }

  private def continueGoldSink(
                                userId: Int,
                                goldLeft: Int,
                                itemsToBuy: List[GoldSinkRule.Item],
                                stock: ItemSet,
                                boughtItemsByCode: Map[String, Int],
                                pushHistory: (String, Boolean) => Unit
                              ): Unit = {

      pushHistory(s"Осталось $goldLeft💰", false)

    if (0 < goldLeft) {
        def amountNeeded(item: GoldSinkRule.Item) = item.desiredAmountInStock match {
          case Some(desiredAmount) =>
            val amountInStock = stock.quantityByItemCode(item.code) + boughtItemsByCode.getOrElse(item.code, 0)
            Some(desiredAmount - amountInStock)
          case None => None
        }

        def hasEnoughAmount(item: GoldSinkRule.Item) =
          amountNeeded(item).exists(_ <= 0)

        val (neededItems, notNeededItems) = itemsToBuy.partition { item => item.price <= goldLeft && !hasEnoughAmount(item) }
        val itemsWithEnoughAmount = notNeededItems.filter(hasEnoughAmount)

        itemsWithEnoughAmount.map(_.name).toSet.foreach { itemName: String =>
          pushHistory(s"\uD83D\uDCE6 $itemName: достигнуто требуемое количество", false)
        }

        neededItems match {
          case item :: itemsTail =>
            val neededQuantity = amountNeeded(item).getOrElse(Int.MaxValue)
            val quantity = Math.min(goldLeft / item.price, neededQuantity)
            pushHistory(s"⚖️ $quantity x ${item.name} по ${item.price}💰", false)

            issueWtb(userId, item.code, quantity, item.price, exactPrice = item.exactPrice).foreach {
              case Right(_) =>
                pushHistory(s"<code>/wtb</code>: успешно", false)

                scheduler.scheduleIn(5.seconds) {
                  updateGold(userId).foreach { newGoldLeft =>
                    if (newGoldLeft == goldLeft) {
                      // Gold has not changed... Some problem with buying this item.
                      // Continue with itemsTail
                      continueGoldSink(userId, newGoldLeft, itemsTail, stock, boughtItemsByCode, pushHistory)
                    } else {
                      // Gold has changed
                      // We bought at least some items...
                      // However their price is far less than we expected and we can buy them again
                      val newBoughtItems = boughtItemsByCode.updated(
                        item.code, boughtItemsByCode.getOrElse(item.code, 0) + quantity
                      )
                      val realPrice = Math.max((goldLeft - newGoldLeft) / quantity, 1)
                      if (realPrice <= newGoldLeft) {
                        // Try to buy at lower price
                        pushHistory(s"Похоже, что ${item.name} стоит дешевле, попробую ещё", false)
                        val itemRealPrice = item.copy(price = realPrice, exactPrice = false)
                        val itemGoldLeftPrice = item.copy(price = Math.min(item.price, newGoldLeft), exactPrice = false)
                        continueGoldSink(userId, newGoldLeft, itemRealPrice :: itemGoldLeftPrice :: itemsTail, stock, newBoughtItems, pushHistory)
                      } else {
                        // Skip completely
                        continueGoldSink(userId, newGoldLeft, itemsTail, stock, newBoughtItems, pushHistory)
                      }
                    }
                  }
                }
              case Left(ApiResponse.Code.NoOffersFoundByPrice) =>
                pushHistory(s"<code>/wtb</code>: нет предложений по такой цене", false)
                continueGoldSink(userId, goldLeft, itemsTail, stock, boughtItemsByCode, pushHistory)
              case Left(ApiResponse.Code.InsufficientFunds) =>
                pushHistory(s"<code>/wtb</code>: не хватает средств", false)
                // Continue with next item
                updateGold(userId).foreach { newGoldLeft =>
                  continueGoldSink(userId, newGoldLeft, itemsTail, stock, boughtItemsByCode, pushHistory)
                }
              case Left(ApiResponse.Code.UserIsBusy) =>
                pushHistory(s"Персонаж занят... ждём 30 секунд", false)
                scheduler.scheduleIn(30.seconds) {
                  updateGold(userId).foreach { newGoldLeft =>
                    continueGoldSink(userId, newGoldLeft, itemsToBuy, stock, boughtItemsByCode, pushHistory)
                  }
                }
              case Left(ApiResponse.Code.Forbidden) =>
                pushHistory(s"<code>/wtb</code>: нет доступа, а ты точно сделал /auth_wtb?", true)
              case Left(code) =>
                pushHistory(s"<code>/wtb</code>: $code", true)
            }
          case Nil => pushHistory(s"<b>Больше нечего покупать, слив завершён</b>", true)
        }
      } else {
      pushHistory(s"<b>Слив завершён</b>", true)
      }
  }

  private def issueWtb(userId: Int, itemCode: String, quantity: Int, price: Int, exactPrice: Boolean): Future[Either[ApiResponse.Code, Unit]] = {
    val future = wtbPromises.resultFuture(userId)
    api.send(userId, WantToBuy.Request(itemCode, quantity, price, exactPrice = exactPrice))
    future
  }

  private def updateStock(userId: Int): Future[ItemSet] = {
    val future = stockPromises.resultFuture(userId)
    api.send(userId, RequestStock.Request())
    future
  }

  private def updateGold(userId: Int): Future[Int] = {
    val future = profilePromises.resultFuture(userId)
    api.send(userId, RequestProfile.Request())
    future.map(_.gold)
  }
}

object GoldSinkService {

  case class GoldSinkHistory(lines: Vector[String]) {
    def withNewLine(str: String): GoldSinkHistory = {
      this.copy(lines = lines :+ str)
    }
  }

}