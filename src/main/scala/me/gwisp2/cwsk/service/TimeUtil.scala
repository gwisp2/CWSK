package me.gwisp2.cwsk.service

import java.sql.Timestamp

object TimeUtil {
  def now = new Timestamp(System.currentTimeMillis())
}
