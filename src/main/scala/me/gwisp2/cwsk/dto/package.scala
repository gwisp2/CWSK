package me.gwisp2.cwsk

package object dto {
  type FullGuildInfo = ReifiedGuild[ShortChar]
}
