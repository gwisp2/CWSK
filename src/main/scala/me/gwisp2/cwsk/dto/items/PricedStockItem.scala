package me.gwisp2.cwsk.dto.items

case class PricedStockItem(item: StockItem, priceInGold: Option[BigDecimal])

class PricedItemSet(val items: Traversable[PricedStockItem]) {
  val totalPriceInGold: BigDecimal = items.collect { case PricedStockItem(item, Some(price)) => price * item.quantity }.sum

  def format(): String = {
    items.map { case PricedStockItem(item, priceInGold) =>
      val code = item.`type`.ingameId.getOrElse("-")
      val name = item.`type`.name
      val quantity = item.quantity
      val priceSuffix = priceInGold match {
        case Some(price) => s"<code> ≈ 💰${price * item.quantity} ($price каждый)</code>"
        case None => ""
      }

      s"<code>$code</code> $name x $quantity$priceSuffix"
    }.mkString("", "\n", if (totalPriceInGold != 0) s"\n------------------\nИтого: 💰$totalPriceInGold" else "")
  }

  def sorted(ordering: Ordering[PricedStockItem]) = new PricedItemSet(items = items.toSeq.sorted(ordering))
}

object PricedStockItem {
  type PricedItemSortType = SortType[PricedStockItem]
  type PricedItemSort = Sort[PricedStockItem]
}
