package me.gwisp2.cwsk.dto.items

case class ItemCode(prefix: String, number: Option[Int])

object ItemCode {
  implicit val ord: Ordering[ItemCode] = Ordering.by(unapply)

  private val regex = """^(.*?)(\d*)$""".r

  def parse(str: String): ItemCode = {
    val Some(m) = regex.findFirstMatchIn(str)
    ItemCode(m.group(1), if (m.group(2).isEmpty) None else Some(m.group(2).toInt))
  }
}
