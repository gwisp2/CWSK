package me.gwisp2.cwsk.dto.items

import me.gwisp2.cwsk.dto.items.ItemSearcher.ItemTypeWithId


class ItemSearcher(itemTypes: Seq[ItemTypeWithId]) {
  def findType(searchString: String): Option[StockItemType] = find(searchString).map(_.itemType)

  def find(searchString: String): Option[ItemTypeWithId] = {
    val searchStringLowerCase = searchString.toLowerCase

    val byId = itemTypes.find(_.itemType.ingameId.contains(searchString))
    lazy val byNameExact = itemTypes.find(_.itemType.name.equalsIgnoreCase(searchStringLowerCase))
    lazy val byNamePart = itemTypes.filter(_.itemType.name.toLowerCase.contains(searchStringLowerCase)) match {
      case itemType +: Seq() => Some(itemType)
      case _ => None
    }

    byId.orElse(byNameExact).orElse(byNamePart)
  }

  def findMany(searchString: String): Set[ItemTypeWithId] = {
    val searchStringLowerCase = searchString.toLowerCase

    val byId = itemTypes.filter(_.itemType.ingameId.contains(searchString))
    val byNamePart = itemTypes.filter(_.itemType.name.toLowerCase.contains(searchStringLowerCase))

    (byId ++ byNamePart).toSet
  }
}

object ItemSearcher {

  case class ItemTypeWithId(id: Int, itemType: StockItemType)

}