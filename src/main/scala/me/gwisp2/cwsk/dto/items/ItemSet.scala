package me.gwisp2.cwsk.dto.items

import me.gwisp2.cwsk.service.ItemPricesService.ItemPrices

case class ItemSet(items: Traversable[StockItem]) {
  lazy val quantityByItemCode: Map[String, Int] = items.collect {
    case StockItem(StockItemType(Some(ingameId), _), quantity) => (ingameId, quantity)
  }.toMap.withDefaultValue(0)

  lazy val quantityByItemType: Map[StockItemType, Int] = items.map {
    item => (item.`type`, item.quantity)
  }.toMap.withDefaultValue(0)

  lazy val totalQuantity: Int = items.map(_.quantity).sum

  def nonEmpty: Boolean = items.nonEmpty

  def quantityOf(itemType: StockItemType) = quantityByItemType(itemType)

  def subtract(rhs: ItemSet): Traversable[StockItem] = {
    val allTypes = (this.items.map(_.`type`) ++ rhs.items.map(_.`type`)).toSet
    allTypes.collect {
      case t if this.quantityOf(t) != rhs.quantityOf(t) => StockItem(t, this.quantityOf(t) - rhs.quantityOf(t))
    }
  }

  def withPrices(prices: ItemPrices): PricedItemSet = {
    new PricedItemSet(this.items.map { item =>
      PricedStockItem(item, prices.getPrice(item.`type`))
    })
  }

  def format(): String = {
    /* Format as item set with prices that are not known */
    withPrices(ItemPrices.empty).format()
  }
}

object ItemSet {
  val empty = ItemSet(None)
}
