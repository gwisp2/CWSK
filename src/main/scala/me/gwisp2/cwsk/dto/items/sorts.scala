package me.gwisp2.cwsk.dto.items

trait SortType[T] {
  def name: String

  def ascOrdering: Ordering[T]

  def descOrdering: Ordering[T] = ascOrdering.reverse

  def isDescDefault: Boolean

  def toSort: Sort[T] = Sort[T](this, isDescDefault)
}

case class Sort[T](`type`: SortType[T], desc: Boolean) {

  import Sort._

  override def toString: String = (if (desc) descIcon else ascIcon) + `type`.name

  def ordering: Ordering[T] = if (desc) `type`.descOrdering else `type`.ascOrdering

  def reverse: Sort[T] = copy(desc = !desc)
}

object Sort {
  private val ascIcon = "\uD83D\uDD3A"
  private val descIcon = "\uD83D\uDD3B"
}