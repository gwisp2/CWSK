package me.gwisp2.cwsk.dto.items

class ItemCategory(val name: String) {
  override def toString: String = name
}

object ItemCategory {
  val equipment = new ItemCategory("equipment")
  val craft = new ItemCategory("craft")
  val potions = new ItemCategory("potions")
  val resources = new ItemCategory("resources")
  val halloween = new ItemCategory("halloween")
  val event = new ItemCategory("event")
  val trophy = new ItemCategory("trophy")
  val other = new ItemCategory("other")

  val allCategories = Seq(equipment, craft, potions, resources, halloween, event, trophy, other)

  def of(`type`: StockItemType): ItemCategory = {
    byItemName(`type`.name)
      .orElse(byIngameId(`type`.ingameId))
      .getOrElse(other)
  }

  private def byItemName(name: String): Option[ItemCategory] = {
    if (name.startsWith("\uD83C\uDFC6")) {
      // Example: The Most Attentive Guardian Неделя #12
      Some(ItemCategory.trophy)
    } else if (name.startsWith("\uD83E\uDDDF\u200D♂")) {
      // Example: 🧟‍♂️ Walker Boots
      Some(event)
    } else if (
      name.endsWith(" Ring") || name.endsWith(" Necklace") || name.endsWith("Amulet") || name.endsWith("Orb") ||
        name.equals("Timeless Jade") || name.equals("Shadow Bloodstone") || name.equals("Void Emerald") || name.equals("Mystery Obsidian")
    ) {
      Some(halloween)
    } else {
      None
    }
  }

  private def byIngameId(ingameId: Option[String]): Option[ItemCategory] = {
    ingameId match {
      case Some(id) =>
        val code = ItemCode.parse(id)
        code.prefix match {
          case "" => Some(resources)
          case "a" | "w" | "tch" => Some(equipment)
          case "e" | "ch" => Some(event)
          case "k" | "r" => Some(craft)
          case "p" => Some(potions)
          case "hw" => Some(potions)
          case _ => None
        }
      case None => None
    }
  }

  def byName(name: String): Option[ItemCategory] = allCategories.find(_.name == name)
}