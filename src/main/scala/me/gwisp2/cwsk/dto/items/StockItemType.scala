package me.gwisp2.cwsk.dto.items

case class StockItemType(ingameId: Option[String], name: String) {
  lazy val code: Option[ItemCode] = ingameId.map(ItemCode.parse)

  def category: ItemCategory = ItemCategory.of(this)
}

object StockItemType {
  implicit val ordering: Ordering[StockItemType] = Ordering.by(t => (t.code, t.name))
}
