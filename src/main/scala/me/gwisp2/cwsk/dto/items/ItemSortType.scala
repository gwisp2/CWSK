package me.gwisp2.cwsk.dto.items

object ItemSortType {

  object byName extends SortType[StockItem] {
    override def name: String = "Имя"

    override def ascOrdering: Ordering[StockItem] = Ordering.by(_.`type`.name)

    override def isDescDefault: Boolean = false
  }

  object byCode extends SortType[StockItem] {
    override def name: String = "Код"

    override def ascOrdering: Ordering[StockItem] =
      Ordering.Option(ItemCode.ord).on(_.`type`.code)

    override def isDescDefault: Boolean = false
  }

  object byQuantity extends SortType[StockItem] {
    override def name: String = "Кол-во"

    override def ascOrdering: Ordering[StockItem] = Ordering.by(i => (i.quantity, i.`type`.name))

    override def isDescDefault: Boolean = true
  }

  def all: Seq[SortType[StockItem]] = Seq(byName, byCode, byQuantity)

  def byId(id: Int): Option[SortType[StockItem]] = all.lift(id - 1)

  def toId(`type`: SortType[StockItem]): Int = all.indexOf(`type`) + 1
}