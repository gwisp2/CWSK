package me.gwisp2.cwsk.dto.items

object PricedItemSortType {
  case class FromItemSortType(itemSortType: SortType[StockItem]) extends SortType[PricedStockItem] {
    override def name: String = itemSortType.name

    override def ascOrdering: Ordering[PricedStockItem] = itemSortType.ascOrdering.on(_.item)

    override def isDescDefault: Boolean = itemSortType.isDescDefault
  }
  case class ByTotalPrice() extends SortType[PricedStockItem] {
    override def name: String = "Цена"

    override def ascOrdering: Ordering[PricedStockItem] =
      Ordering.by { i => i.priceInGold.map(price => price * i.item.quantity).getOrElse(BigDecimal(0)) }

    override def isDescDefault: Boolean = true
  }

  def all: Seq[SortType[PricedStockItem]] = ItemSortType.all.map(FromItemSortType.apply) ++ Some(ByTotalPrice())
}