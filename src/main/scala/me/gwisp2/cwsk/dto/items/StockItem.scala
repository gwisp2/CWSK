package me.gwisp2.cwsk.dto.items

case class StockItem(`type`: StockItemType, quantity: Int) {
  def category: ItemCategory = `type`.category
}

object StockItem {
  type ItemSortType = SortType[StockItem]
  type ItemSort = Sort[StockItem]
}