package me.gwisp2.cwsk.dto

import java.time.Instant

import me.gwisp2.cwsk.bot.parsed.EquipmentItem
import me.gwisp2.cwsk.cwapi.methods.OperationType.OperationType
import me.gwisp2.cwsk.dto.items.{ItemSet, StockItem}

case class UserCwapiInfo(hasToken: Boolean, permissions: Set[OperationType])

trait BaseChar {
  val userId: Int
  val name: String
  val gameClass: String
}

case class PetInfo(icon: String, fullName: String, level: Int)

case class SimpleChar(userId: Int, name: String, gameClass: String) extends BaseChar

case class ReifiedChar[T](char: SimpleChar, value: T)

case class ReifiedGuild[T](guild: GuildInfo, members: Seq[ReifiedChar[T]]) {
  def replaceValues[R](f: ReifiedChar[T] => R): ReifiedGuild[R] = {
    ReifiedGuild(guild, members.map(rc => rc.copy(value = f(rc))))
  }

  def memberIds: Traversable[Int] = members.map(_.char.userId)
}

case class FullChar(
                     userId: Int,
                     guild: Option[GuildInfo],
                     name: String,
                     gameClass: String,
                     castle: String,
                     level: Int,
                     exp: Int,
                     atk: Int,
                     `def`: Int,
                     equipment: Seq[EquipmentItem],
                     equipmentIsValid: Boolean,
                     pet: Option[PetInfo],
                     updateInstant: Instant
                   ) extends BaseChar

case class GuildInfo(castle: String, tag: Option[String], name: String)


object GuildPermission extends Enumeration {
  /* Set of permissions is persisted using bit masks! Don't change the order of permissions. */
  type Permission = Value
  val GsGuild: Permission = Value("gs_guild")
  val GuildPog: Permission = Value("guild_pog")
  val PermissionsEdit: Permission = Value("изменение прав")
  val GuildStock: Permission = Value("guild_stock")

  def byId(id: Int): Option[GuildPermission.Value] = values.find(_.id == id)
}

case class GuildRole(isLeader: Boolean, permissions: GuildPermission.ValueSet) {
  def hasPermission(permission: GuildPermission.Value): Boolean = isLeader || permissions.contains(permission)

  def hasPermissions(permissionSet: GuildPermission.ValueSet): Boolean = isLeader || permissionSet.forall(permissions.contains)
}

case class ShortChar(
                      userId: Int,
                      name: String,
                      gameClass: String,
                      level: Int,
                      exp: Int,
                      isUpToDate: Boolean,
                      role: GuildRole
                    ) extends BaseChar

case class SaveStockResult(
                            userId: Int,
                            newStock: ItemSet,
                            oldStock: ItemSet,
                            itemsWithoutKnownIds: Traversable[String]
                          ) {
  def delta: Traversable[StockItem] = newStock.subtract(oldStock)
}




