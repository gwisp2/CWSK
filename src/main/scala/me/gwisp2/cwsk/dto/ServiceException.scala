package me.gwisp2.cwsk.dto

class ServiceException(val displayMessage: String) extends Exception(displayMessage)
