package me.gwisp2.cwsk.dto

import me.gwisp2.cwsk.dto.items.{StockItem, StockItemType}

import scala.util.Try

class ItemsParser(itemTypes: Traversable[StockItemType]) {
  def findItemTypes(codeOrNamePart: String): Traversable[StockItemType] = {
    val searchedText = codeOrNamePart.toLowerCase

    itemTypes.filter { itemType =>
      itemType.name.toLowerCase.contains(searchedText) || itemType.ingameId.contains(searchedText)
    }
  }

  private val rItemWithQuantity = """^(.*?)\s+(\d+)$""".r

  def parseItem(str: String): Traversable[StockItem] = {
    rItemWithQuantity.findFirstMatchIn(str) match {
      case Some(m) =>
        val itemNamePart = m.group(1)
        val itemQuantity = Try(m.group(2).toInt).toOption.getOrElse(1)
        findItemTypes(itemNamePart).map(StockItem(_, itemQuantity))
      case None =>
        Seq.empty
    }
  }
}
