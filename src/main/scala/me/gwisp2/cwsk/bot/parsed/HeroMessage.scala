package me.gwisp2.cwsk.bot.parsed

import me.gwisp2.cwsk.dto.PetInfo

case class EquipmentItem(line: String, name: String, enchantmentLevel: Int = 0, bonusAtk: Int = 0,
                         bonusDef: Int = 0, bonusStamina: Int = 0, bonusBagSlots: Int = 0)

case class HeroMessage(name: String, equipment: Seq[EquipmentItem], pet: Option[PetInfo])

object HeroMessage {
  private val regexStart = "^([^\n]+)\n\uD83C\uDFC5Уровень".r
  private val regexEquip = """(?m)^🎽Экипировка""".r
  private val regexPet = """(?m)^Питомец:\n([^\s]+)\s+(.*?)\s+\((\d+) lvl\).*/pet$""".r

  implicit val parser: MessageTextParser[HeroMessage] = { (text: String) =>
    for (
      mName <- regexStart.findFirstMatchIn(text);
      mEquip <- regexEquip.findFirstMatchIn(text);
      lines = text.substring(mEquip.start).linesIterator.drop(1).takeWhile(!_.trim.isEmpty);
      pet = regexPet.findFirstMatchIn(text).map { m =>
        PetInfo(m.group(1), m.group(2), m.group(3).toInt)
      }
    ) yield HeroMessage(mName.group(1), lines.map(this.parseEquipmentItem).toSeq, pet)
  }

  private val regexEquipmentLine = """^((?:⚡|⚡️)\+(\d+)\s+)?([^+]*)(.*)$""".r
  private val regexBonus = """\+(\d+)([^\s]+)""".r

  def parseEquipmentItem(line: String): EquipmentItem = {
    val m = regexEquipmentLine.findFirstMatchIn(line).getOrElse(
      throw new AssertionError("Regex must always match"))
    val enchantmentLevel = if (m.group(2) != null) m.group(2).toInt else 0
    val name = m.group(3).trim

    val itemWithoutBonus = EquipmentItem(line, name, enchantmentLevel)
    val bonusMatches = regexBonus.findAllMatchIn(m.group(4)).map(m => (m.group(1).toInt, m.group(2)))
    val itemWithBonus = bonusMatches.foldLeft(itemWithoutBonus) {
      case (item, (amount, bonusName)) => bonusName match {
        case "⚔️" => item.copy(bonusAtk = amount)
        case "⚔" => item.copy(bonusAtk = amount)
        case "\uD83D\uDEE1" => item.copy(bonusDef = amount)
        case "\uD83D\uDD0B" => item.copy(bonusStamina = amount)
        case "\uD83C\uDF92" => item.copy(bonusBagSlots = amount)
        case _ => item
      }
    }
    itemWithBonus
  }
}
