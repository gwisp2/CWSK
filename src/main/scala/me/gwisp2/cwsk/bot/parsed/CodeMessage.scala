package me.gwisp2.cwsk.bot.parsed

case class CodeMessage(code: String)

object CodeMessage {
  private val regex = "^Code (\\d+) to authorize".r

  implicit val parser: MessageTextParser[CodeMessage] =
    (text: String) => regex.findFirstMatchIn(text).map(m => CodeMessage(m.group(1)))
}
