package me.gwisp2.cwsk.bot.parsed

import info.mukel.telegrambot4s.models.Message

trait MessageParser[T] {
  def parse(message: Message): Option[T]
}

trait MessageTextParser[T] extends MessageParser[T] {
  def parse(text: String): Option[T]

  override def parse(message: Message): Option[T] = message.text.flatMap(this.parse)

  def ||(rhs: MessageTextParser[T]): MessageTextParser[T] = m => this.parse(m).orElse(rhs.parse(m))
}