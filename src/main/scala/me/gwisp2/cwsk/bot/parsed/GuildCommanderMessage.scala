package me.gwisp2.cwsk.bot.parsed

case class GuildCommanderMessage(tag: Option[String], name: String, commanderName: String)

object GuildCommanderMessage {
  private val regexWithTag = """(?U)^.*\[([\w]+)\]\s+(.*)\nCommander:\s+(.*)\n""".r
  private val regexWithoutTag = """(?U)^.*?([\w\s]+)\nCommander:\s+(.*)\n""".r

  private val parserWithTag: MessageTextParser[GuildCommanderMessage] =
    (text: String) => regexWithTag.findFirstMatchIn(text).map(m => GuildCommanderMessage(
      Some(m.group(1)), m.group(2), m.group(3)
    ))

  private val parserWithoutTag: MessageTextParser[GuildCommanderMessage] =
    (text: String) => regexWithoutTag.findFirstMatchIn(text).map(m => GuildCommanderMessage(
      None, m.group(1), m.group(2)
    ))

  implicit val parser: MessageTextParser[GuildCommanderMessage] = parserWithTag || parserWithoutTag
}




