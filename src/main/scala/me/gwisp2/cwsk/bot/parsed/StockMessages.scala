package me.gwisp2.cwsk.bot.parsed

case class GuildWarehouse(items: Seq[Item])

case class Item(id: String, name: String, amount: Int)

object GuildWarehouse {
  private val regexStart = "^Guild Warehouse:".r
  private val regexLine = """(?m)^([^\s]+)\s+(.*)\s+x\s+(\d+)$""".r
  implicit val parser: MessageTextParser[GuildWarehouse] = { (text: String) =>
    for (
      _ <- regexStart.findFirstMatchIn(text);
      matches = regexLine.findAllMatchIn(text)
    ) yield GuildWarehouse(matches.map(m => Item(m.group(1), m.group(2), m.group(3).toInt)).toList)
  }
}


case class AuctionItemList(items: Seq[Item])

object AuctionItemList {
  private val regexStart = "^You have for sale:".r
  private val regexLine = """(?m)^/lot_([^\s]+)\s+(.*)\s+\((\d+)\)$""".r
  implicit val parser: MessageTextParser[AuctionItemList] = { (text: String) =>
    for (
      _ <- regexStart.findFirstMatchIn(text);
      matches = regexLine.findAllMatchIn(text)
    ) yield AuctionItemList(matches.map(m => Item(m.group(1), m.group(2), m.group(3).toInt)).toList)
  }
}


case class CraftItemList(items: Seq[Item])

object CraftItemList {
  private val regexStart = "^Рецепты:".r
  private val regexLine = """(?m)^/craft_([^\s]+)\s+(.+)$""".r
  implicit val parser: MessageTextParser[CraftItemList] = { text: String =>
    for (
      _ <- regexStart.findFirstMatchIn(text);
      matches = regexLine.findAllMatchIn(text)
    ) yield CraftItemList(matches.map(m => Item(m.group(1), m.group(2), 1)).toList)
  }
}

case class NpcShopItemList(items: Seq[Item])

case class PlayerShopItemList(items: Seq[Item])

object NpcShopItemList {
  private val regexStart = "^На прилавке ты видишь следующие товары:".r
  private val regexLine = """(?mu)^([^+\n]+).*\n(?:.*\n)?\d+[💰💎]\n/buy_([^\s]+)""".r
  implicit val parser: MessageTextParser[NpcShopItemList] = { text: String =>
    for (
      _ <- regexStart.findFirstMatchIn(text);
      matches = regexLine.findAllMatchIn(text)
    ) yield NpcShopItemList(matches.map(m => Item(m.group(2), m.group(1).trim(), 1)).toList)
  }
}

object PlayerShopItemList {
  private val regexStart = """(?m)^Добро пожаловать в .* #\d+\.$""".r
  private val regexLine = """(?m)^(.*), \d+💧 \d+💰 /ws_.*_([\w\d]+)$""".r
  implicit val parser: MessageTextParser[PlayerShopItemList] = { text: String =>
    for (
      _ <- regexStart.findFirstMatchIn(text);
      matches = regexLine.findAllMatchIn(text)
    ) yield PlayerShopItemList(matches.map(m => Item(m.group(2), m.group(1).trim(), 1)).toList)
  }
}

case class ExchangeTMessage(itemId: String, itemName: String)

object ExchangeTMessage {
  private val regexStart = "^Предложения (.*) сейчас:\n".r
  private val regexLine = """(?m)^Купить 1: /wtb_(.*)$""".r
  implicit val parser: MessageTextParser[ExchangeTMessage] = { text: String =>
    for (
      mName <- regexStart.findFirstMatchIn(text);
      mId <- regexLine.findFirstMatchIn(text)
    ) yield ExchangeTMessage(mId.group(1), mName.group(1))
  }
}

case class StockMessage(items: Seq[Item])

object StockMessage {
  private val regexLine1 = """(?m)^(.*)\s+\((\d+)\)(?:\s+/(?:view|use|bind)_([^\s]*))?\s*$""".r
  private val regexLine2 = """(?m)^/a_([^\s]+)\s+(.*)\s+x\s+(\d+)$""".r
  private val regexStart = """^📦Склад.*$""".r
  private val regexWorkshop =
    """(?s)^⚒На верстаке ты видишь:
      |.*
      |
      |(.*)$""".stripMargin.r

  implicit val parser: MessageTextParser[StockMessage] = { textWithPossiblePrefix: String =>
    val text =
      regexWorkshop.findFirstMatchIn(textWithPossiblePrefix).map(_.group(1))
        .getOrElse(textWithPossiblePrefix)

    for (
      firstLine <- text.linesIterator.find(_ => true);
      // Check that first line is 'Склад' or item
      _ <- regexLine1.findFirstIn(firstLine).orElse(regexStart.findFirstIn(firstLine))
    ) yield {
      val items1 = regexLine1.findAllMatchIn(text).collect {
        case m if m.group(3) != null => Item(m.group(3), stripNamePrefixes(m.group(1)), m.group(2).toInt)
      }
      val items2 = regexLine2.findAllMatchIn(text).map {
        m => Item(m.group(1), stripNamePrefixes(m.group(2)), m.group(3).toInt)
      }
      StockMessage((items1 ++ items2).toSeq)
    }
  }

  private def stripNamePrefixes(itemName: String) =
    itemName.stripPrefix("\uD83D\uDCC3").stripPrefix("\uD83C\uDFF7")
}
