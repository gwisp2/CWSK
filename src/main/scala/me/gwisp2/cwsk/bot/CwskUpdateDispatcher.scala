package me.gwisp2.cwsk.bot

import info.mukel.telegrambot4s.methods.GetMe
import info.mukel.telegrambot4s.models.Update
import kamon.Kamon
import me.gwisp2.cwsk.bot.CwskUpdateDispatcher.CwskUpdateContext
import me.gwisp2.cwsk.bot.declarative.{AbstractUpdateDispatcher, TelegramBotEx, UpdateContext}
import me.gwisp2.cwsk.bot.inline.InlineData
import me.gwisp2.cwsk.dto.ServiceException
import me.gwisp2.cwsk.service.SecurityContext
import me.gwisp2.tbot.{Reply, Result, ToResult}

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

class CwskUpdateDispatcher(
                            val bot: TelegramBotEx,
                          ) extends AbstractUpdateDispatcher[CwskUpdateContext] {
  private val mDispatchTime = Kamon.timer("dispatch-time")

  private lazy val botUsername: String =
    Await.result(bot.request(GetMe).map(_.username match {
      case Some(username) => username
      //This case is not possible, since any bot must have a defined unique username.
      case None => throw new RuntimeException("The current bot does not have a defined username")
    }), 10.seconds)

  override def createUpdateContext(update: Update): CwskUpdateContext = {
    new CwskUpdateContext(update, botUsername)
  }

  def onCommand[R](commandName: String)(action: CwskUpdateContext => R)(implicit ev: ToResult[R]): Unit = {
    onMessage { implicit uc =>
      for (command <- uc.command if command.name == commandName) yield {
        ev.toResult(action(uc))
      }
    }
  }

  def onInlineButton[T: Manifest, R](action: CwskUpdateContext => T => R)(implicit ev: ToResult[R]): Unit = {
    onInlineKeyboardButton { implicit uc =>
      for (data <- uc.inlineData;
           dataAsT <- data match {
             case t: T => Some(t)
             case _ => None
           }) yield {
        ev.toResult(action(uc)(dataAsT))
      }
    }
  }


  override def dispatchUpdate(u: Update): Result = {
    val timer = mDispatchTime.start()
    val result = super.dispatchUpdate(u)
    result match {
      case Result.Empty | _: Result.Ready =>
        timer.stop()
      case Result.InFuture(future) =>
        future.onComplete { _ =>
          timer.stop()
        }
    }
    result
  }

  def handleReply(reply: Reply): Unit = {
    try {
      reply.send(bot, None)
    } catch {
      case t: Throwable =>
        logger.warn("Exception while sending reply", t)
    }
  }
}

object CwskUpdateDispatcher {
  private val commandPattern = """(?sm)^/(\w*)(?:@(\w*))?(?:\s+(.*))?$""".r

  case class Command(name: String, args: String)

  class CwskUpdateContext(val update: Update, botUsername: String) extends UpdateContext with SecurityContext {
    val command: Option[Command] = {
      for (message <- update.message; text <- message.text;
           commandMatch <- commandPattern.findFirstMatchIn(text);
           commandName = commandMatch.group(1);
           commandReceiverUsername = commandMatch.group(2)
           if commandReceiverUsername == null || commandReceiverUsername == botUsername;
           commandArgsOrNull = commandMatch.group(3);
           commandArgs = if (commandArgsOrNull == null) "" else commandArgsOrNull
      ) yield Command(commandName, commandArgs)
    }

    val inlineData: Option[InlineData] = {
      for (callbackQuery <- update.callbackQuery;
           data <- callbackQuery.data;
           inlineData <- InlineData.fromString(data)
      ) yield inlineData
    }

    override def effectiveUserId: Int = sender.map(_.id).getOrElse(throw new ServiceException("Пользователь не определён"))
  }
}
