package me.gwisp2.cwsk.bot.formatter

import me.gwisp2.cwsk.dto.items.StockItem
import me.gwisp2.cwsk.service.ItemPricesService.ItemPrices

class StockDeltaFormatter(short: Boolean) {
  def format(delta: Traversable[StockItem], itemPrices: ItemPrices): String = {
    val header = "<b>Изменения в стоке:</b>\n\n"

    if (delta.isEmpty) {
      return if (!short) header + "[изменений нет]\n" else ", <b>изменений нет</b>"
    }

    val (newItems, lostItems) = delta.partition(_.quantity > 0)

    val newItemsHtml = formatItems(newItems, "➕Приобретено", "+", itemPrices)
    val lostItemsHtml = formatItems(lostItems, "➖Потеряно", "", itemPrices)

    if (!short) {
      (newItemsHtml ++ lostItemsHtml).mkString(header, "\n\n", "\n")
    } else {
      (newItemsHtml ++ lostItemsHtml).mkString("\n", "\n", "\n")
    }
  }

  private def formatItems(items: Traversable[StockItem], headerName: String, prefix: String, prices: ItemPrices): Option[String] = {
    if (items.nonEmpty) {
      val sumPrice = items
        .flatMap(i => prices.getPrice(i.`type`).map(price => price * Math.abs(i.quantity)))
        .sum
      val header = s"<b>$headerName (</b><code>≈💰$sumPrice</code><b>):</b>\n"
      Some(items.map(prefix + formatItem(_, prices)).mkString(header, "\n", ""))
    } else {
      None
    }
  }

  private def formatItem(item: StockItem, prices: ItemPrices): String = {
    val costSuffix = prices.getPrice(item.`type`.name) match {
      case Some(price) => s"<code> ≈💰${price * Math.abs(item.quantity)}</code>"
      case None => ""
    }
    s"${item.quantity} ${item.`type`.name}$costSuffix"
  }
}
