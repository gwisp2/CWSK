package me.gwisp2.cwsk.bot.declarative

import info.mukel.telegrambot4s.models.{Update, User}

trait UpdateContext {
  val sender: Option[User] =
    update.message.flatMap(_.from)
      .orElse(update.callbackQuery.map(_.from))
      .orElse(update.inlineQuery.map(_.from))
      .orElse(update.chosenInlineResult.map(_.from))

  def update: Update
}
