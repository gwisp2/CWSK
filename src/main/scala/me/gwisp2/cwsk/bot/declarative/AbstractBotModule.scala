package me.gwisp2.cwsk.bot.declarative

import me.gwisp2.tbot.Reply

abstract class AbstractBotModule {
  def defaultKeyboard: Option[Seq[Seq[String]]] = None

  def extractButton(buttonText: String)(implicit uc: UpdateContext): Option[String] = {
    for (message <- uc.update.message; text <- message.text if text == buttonText) yield text
  }

  def reply(text: String): Reply = {
    reply(text, defaultKeyboard)
  }

  def replyCq(text: String, alert: Boolean = false): Reply = {
    Reply.ReplyCq(text, alert)
  }

  def reply[K: ToReplyMarkup](text: String, keyboard: Option[K]): Reply = {
    val replyMarkup = keyboard.map(k => ToReplyMarkup.convert(k))
    Reply.ReplyToMessage(text, replyMarkup = replyMarkup)
  }

  def replyEdit[K: ToReplyMarkup](text: String, keyboard: Option[K]): Reply = {
    val replyMarkup = keyboard.map(k => ToReplyMarkup.convert(k))
    Reply.ReplyOrEdit(text, replyMarkup = replyMarkup)
  }

  def combineReplies(replies: Reply*): Reply.Combine = {
    Reply.Combine(replies)
  }
}
