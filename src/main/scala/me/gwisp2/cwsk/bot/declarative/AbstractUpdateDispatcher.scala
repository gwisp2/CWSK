package me.gwisp2.cwsk.bot.declarative

import com.typesafe.scalalogging.StrictLogging
import info.mukel.telegrambot4s.models.{ChosenInlineResult, InlineQuery, Update}
import me.gwisp2.tbot.{Reply, Result, ToResult}

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success, Try}

abstract class AbstractUpdateDispatcher[U <: UpdateContext] extends StrictLogging {
  final type Action = U => Result
  final type FailureAction = (Update, Throwable) => Reply

  private val messageHandlers = ArrayBuffer[Action]()
  private val inlineQueryHandlers = ArrayBuffer[Action]()
  private val chosenInlineResultHandlers = ArrayBuffer[Action]()
  private val inlineKeyboardButtonHandlers = ArrayBuffer[Action]()
  private val failureHandlers = ArrayBuffer[FailureAction]()

  def onMessage[R](action: U => R)(implicit ev: ToResult[R]): Unit = {
    val handler = { uc: U => ev.toResult(action(uc)) }
    messageHandlers += handler
  }

  def onInlineKeyboardButton[R](action: U => R)(implicit ev: ToResult[R]): Unit = {
    val handler = { uc: U => ev.toResult(action(uc)) }
    inlineKeyboardButtonHandlers += handler
  }

  def onInlineQuery[R](action: U => InlineQuery => R)(implicit ev: ToResult[R]): Unit = {
    val handler = { uc: U => ev.toResult(action(uc)(uc.update.inlineQuery.get)) }
    inlineQueryHandlers += handler
  }

  def onChosenInlineResult[R](action: U => ChosenInlineResult => R)(implicit ev: ToResult[R]): Unit = {
    val handler = { uc: U => ev.toResult(action(uc)(uc.update.chosenInlineResult.get)) }
    chosenInlineResultHandlers += handler
  }

  def onFailure(action: FailureAction): Unit = {
    failureHandlers += action
  }

  def dispatchUpdate(u: Update): Result = {
    u.message.map { _ => runHandlers(u, messageHandlers) }
      .orElse(u.callbackQuery.map { _ => runHandlers(u, inlineKeyboardButtonHandlers) })
      .orElse(u.inlineQuery.map { _ => runHandlers(u, inlineQueryHandlers) })
      .orElse(u.chosenInlineResult.map { _ => runHandlers(u, chosenInlineResultHandlers) })
      .getOrElse(Result.Empty)
  }

  private def runHandlers[T](update: Update, actions: Seq[Action]): Result = {
    val updateContext = createUpdateContext(update)

    // Important! We use laziness here to avoid calling actions after the first successful
    val tryResults = actions.view.map(action => Try(action(updateContext)))
    val results = tryResults.map {
      case Success(Result.InFuture(future)) => Result.InFuture(future.recover {
        case throwable => handleFailure(update, throwable)
      })
      case Success(result) => result
      case Failure(e) =>
        handleFailure(update, e)
        Result.Empty
    }

    results.find(_ != Result.Empty).getOrElse(Result.Empty)
  }

  private def handleFailure(u: Update, t: Throwable): Reply = {
    // Important! We use laziness here to avoid calling actions after the first successful
    val tryReplies = failureHandlers.view.map(handler => Try(handler(u, t)))
    val results = tryReplies.map {
      case Success(reply) => reply
      case Failure(e) =>
        e.addSuppressed(t)
        logger.warn("Exception while handling exception", e)
        Reply.Empty
    }

    results.find(_ != Reply.Empty).getOrElse(Reply.Empty)
  }

  def bot: TelegramBotEx
  def createUpdateContext(update: Update): U
}
