package me.gwisp2.cwsk.bot.declarative

import info.mukel.telegrambot4s.models._
import me.gwisp2.cwsk.bot.inline.InlineData

trait ToReplyMarkup[-T] {
  def apply(v: T): ReplyMarkup
}

object ToReplyMarkup {
  implicit val strings: ToReplyMarkup[Seq[Seq[String]]] = k => ReplyKeyboardMarkup(
    k.map(_.map(KeyboardButton(_))), resizeKeyboard = Some(true), selective = Some(true)
  )

  implicit def inline: ToReplyMarkup[Seq[Seq[(String, InlineData)]]] = k => InlineKeyboardMarkup(
    k.map(_.map(pair => InlineKeyboardButton(pair._1, callbackData = Some(InlineData.toString(pair._2)))))
  )

  implicit def inlineButtons: ToReplyMarkup[Seq[Seq[InlineKeyboardButton]]] = k => InlineKeyboardMarkup(
    k
  )

  def convert[T: ToReplyMarkup](v: T): ReplyMarkup = {
    val converter = implicitly[ToReplyMarkup[T]]
    converter(v)
  }
}
