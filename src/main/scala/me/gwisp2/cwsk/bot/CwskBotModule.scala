package me.gwisp2.cwsk.bot

import com.typesafe.scalalogging.StrictLogging
import me.gwisp2.cwsk.bot.CwskUpdateDispatcher.CwskUpdateContext
import me.gwisp2.cwsk.bot.declarative.{AbstractBotModule, UpdateContext}
import me.gwisp2.cwsk.bot.modules._
import me.gwisp2.cwsk.bot.parsed.MessageParser
import me.gwisp2.cwsk.dto.{BaseChar, GuildInfo}
import me.gwisp2.cwsk.util.FutureLogger

import scala.concurrent.{ExecutionContext, Future}

abstract class CwskBotModule extends AbstractBotModule with StrictLogging {
  implicit val executionContext: ExecutionContext = scala.concurrent.ExecutionContext.global

  def cwBotUsername: String = "ChatWarsBot"

  override def defaultKeyboard: Option[Seq[Seq[String]]] = Some(
    Seq(Seq(MeModule.ButtonMe, MeModule.ButtonGuild, StockCommandsModule.ButtonStock),
      Seq(GoldSinkModule.ButtonMoney, SettingsModule.ButtonSettings, MiscActionsModule.ButtonMisc)
    )
  )

  def forwardIsRecent(implicit uc: UpdateContext): Boolean = {
    val now = System.currentTimeMillis() / 1000
    val twoMinutes = 120
    uc.update.message.flatMap(_.forwardDate).exists(now - _ < twoMinutes)
  }

  def extractCwForward[T: MessageParser](implicit uc: CwskUpdateContext): Option[T] = {
    for (
      message <- uc.update.message
      if message.forwardFrom.flatMap(_.username).contains(cwBotUsername);
      parsed <- implicitly[MessageParser[T]].parse(message)
    ) yield parsed
  }

  def extractCommand(name: String)(implicit uc: CwskUpdateContext): Option[String] = {
    uc.command.filter(_.name == name).map(_.args)
  }

  def extractCommand(implicit uc: CwskUpdateContext): Option[String] = {
    uc.command.map(_.args)
  }

  def extractInlineData[T: Manifest](implicit uc: CwskUpdateContext): Option[T] = {
    uc.inlineData match {
      case Some(data: T) => Some(data)
      case _ => None
    }
  }

  def logErrors[T](f: Future[T]): Unit = {
    FutureLogger.logFailure(logger, f)
  }

  def plural(n: Long, w1: String, w2: String, w3: String): String = {
    if (n % 10 == 1 && n % 100 != 11) w1
    else if (n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20)) w2
    else w3
  }

  def sanitize(str: String): String = xml.Utility.escape(str)

  def formatGuildName(g: GuildInfo): String = {
    s"<b>${g.castle} " + sanitize(g.tag.map(tag => s"[$tag] ").getOrElse("") + g.name) + "</b>\n"
  }

  def formatCharClassAndName(c: BaseChar): String = {
    sanitize(c.gameClass) + " " + formatCharName(c)
  }

  def formatCharName(c: BaseChar): String = {
    s"""<a href="tg://user?id=${c.userId}">${sanitize(c.name)}</a>"""
  }
}
