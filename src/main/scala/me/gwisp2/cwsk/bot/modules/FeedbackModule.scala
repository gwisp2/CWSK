package me.gwisp2.cwsk.bot.modules

import java.time.format.DateTimeFormatter
import java.time.{Duration, Instant, ZoneId}

import com.google.inject.Inject
import com.typesafe.config.Config
import info.mukel.telegrambot4s.methods.ForwardMessage
import info.mukel.telegrambot4s.models.{ChatId, Message}
import me.gwisp2.cwsk.BuildInfo
import me.gwisp2.cwsk.bot.modules.FeedbackModule.FeedbackConfig
import me.gwisp2.cwsk.bot.{CwskBotModule, CwskUpdateDispatcher}
import me.gwisp2.tbot.Reply
import me.gwisp2.tbot.Reply.ReplyRaw

class FeedbackModule @Inject()(
                                config: Config,
                                dispatcher: CwskUpdateDispatcher
                              ) extends CwskBotModule {
  private val zone: ZoneId = ZoneId.of("Europe/Moscow")
  private val dateTimeFormat = DateTimeFormatter.ofPattern("MMM dd, yyyy HH:mm:ss").withZone(zone)
  private val buildInstant: Instant = Instant.ofEpochMilli(BuildInfo.builtAtMillis)
  private val buildAtStr = dateTimeFormat.format(buildInstant)

  private val feedbackConfig = pureconfig.loadConfigOrThrow[FeedbackConfig](config, "feedback")

  dispatcher.onCommand("build_info") { implicit uc =>
    val timeSinceLastUpdate = Duration.between(buildInstant, Instant.now())

    val message =
      s"""<b>${sanitize(BuildInfo.name)} ${sanitize(BuildInfo.version)}</b>
         |<pre>${BuildInfo.commitHash} ${sanitize(BuildInfo.commitMessage)}</pre>
         |
         |Номер сборки: ${BuildInfo.buildInfoBuildNumber}
         |Время сборки: ${sanitize(buildAtStr)}
         |
         |Обновлений не было уже ${sanitize(formatDuration(timeSinceLastUpdate))}
         |Назвать разработчика ленивой жопой: /feedback
      """.stripMargin
    reply(message)
  }

  dispatcher.onCommand("about") { implicit uc =>
    reply(feedbackConfig.aboutMessage)
  }

  dispatcher.onCommand("feedback") { implicit uc =>
    for (message <- uc.update.message if message.forwardFrom.isEmpty; cmd <- uc.command) yield {
      message.replyToMessage match {
        case Some(content) => Reply.Combine(
          Seq(forwardFeedback(message), forwardFeedback(content), reply("<code>/feedback</code> отправлен вместе с вложением"))
        )
        case None if cmd.args.nonEmpty => Reply.Combine(
          Seq(forwardFeedback(message), reply("<code>/feedback</code> отправлен"))
        )
        case None => reply(
          """<pre>/feedback твоё сообщение</pre>
            |А ещё можно кинуть боту сообщение со стикером, фотографией или чем-то другим, а потом написать /feedback в ответ на него.
            |""".stripMargin
        )
      }
    }
  }

  /* Answer feedback: either feedbackConfig.receiverUserId replies or another user replies to him */
  dispatcher.onMessage { implicit uc =>
    for (message <- uc.update.message; replyTo <- message.replyToMessage
         // Don't forward /feedback with reply to developer's answer twice
         if !uc.command.exists(_.name == "feedback");
         replyToForwardFrom <- replyTo.forwardFrom;
         sender <- uc.sender
         if List(sender.id, replyToForwardFrom.id).contains(feedbackConfig.receiverUserId)) yield {
      Reply.Combine(Seq(
        forwardToChat(ChatId(replyToForwardFrom.id), message),
        reply("Ответ отправлен")
      ))
    }
  }

  private def forwardFeedback(message: Message): Reply = {
    forwardToChat(ChatId(feedbackConfig.receiverUserId), message)
  }

  private def forwardToChat(chatId: ChatId, message: Message): Reply = {
    ReplyRaw(ForwardMessage(
      chatId, message.chat.id,
      disableNotification = Some(true), messageId = message.messageId
    ))
  }

  private def formatDuration(d: Duration): String = {
    val parts = Seq(
      durationPart(d.toDaysPart, "день", "дня", "дней"),
      durationPart(d.toHoursPart, "час", "часа", "часов"),
      durationPart(d.toMinutesPart, "минуту", "минуты", "минут"),
      durationPart(d.toSecondsPart, "секунду", "секунды", "секунд")
    )
    val nonemptyParts = parts.collect { case Some(x) => x }
    nonemptyParts match {
      case Seq() => "0 секунд"
      case Seq() :+ last => last
      case p :+ last => p.mkString(", ") + " и " + last
    }
  }

  private def durationPart(n: Long, w1: String, w2: String, w3: String): Option[String] = {
    if (n != 0) {
      Some(n + " " + plural(n, w1, w2, w3))
    } else {
      None
    }
  }
}

object FeedbackModule {
  val feedbackCommand = "/feedback"
  val buildInfoCommand = "/build_info"

  case class FeedbackConfig(receiverUserId: Int, aboutMessage: String)

}