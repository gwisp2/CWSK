package me.gwisp2.cwsk.bot.modules

import com.google.inject.Inject
import me.gwisp2.cwsk.bot.inline.GuildStockReq
import me.gwisp2.cwsk.bot.modules.helper.StockMessageHelper
import me.gwisp2.cwsk.bot.{CwskBotModule, CwskUpdateDispatcher}
import me.gwisp2.cwsk.dto.items.PricedItemSortType.FromItemSortType
import me.gwisp2.cwsk.dto.items.{ItemCategory, ItemSet, ItemSortType}
import me.gwisp2.cwsk.service.{GuildStockService, ItemPricesService, SecurityContext}
import me.gwisp2.tbot.Reply

import scala.concurrent.Future

class GuildStockModule @Inject()(
                                  dispatcher: CwskUpdateDispatcher,
                                  guildStockService: GuildStockService,
                                  itemPricesService: ItemPricesService
                                ) extends CwskBotModule {
  dispatcher.onCommand("guild_stock") { implicit uc =>
    val fCategoriesList = ItemCategory.allCategories.map(c => "/guild_stock_" + c.name).mkString("\n")
    reply(fCategoriesList + "\n\n" + "Поиск: /guild_stock_find <code>название или код</code>")
  }

  for (itemCategory <- ItemCategory.allCategories) {
    dispatcher.onCommand("guild_stock_" + itemCategory.name) { implicit uc =>
      showStock(GuildStockReq(itemCategory, FromItemSortType(ItemSortType.byQuantity).toSort))
    }
  }

  dispatcher.onCommand("guild_stock_find") { implicit uc =>
    for (args <- extractCommand) yield {
      if (args.isEmpty) {
        Future.successful(reply("Не указано название ресурса"))
      } else {
        for (result <- guildStockService.auth().findItems(args)) yield {
          val membersWithSearchedItems = result.members
            .filter(_.value.nonEmpty)
            .sortBy(-_.value.totalQuantity)

          val searchResults = if (membersWithSearchedItems.nonEmpty) {
            membersWithSearchedItems.map { rc =>
              val fName = formatCharClassAndName(rc.char)
              val fItems = rc.value.format()
              s"$fName\n$fItems"
            }.mkString("\n\n")
          } else {
            "[пусто]"
          }

          reply("<b>Результаты поиска:</b>\n" + searchResults)
        }
      }
    }
  }

  dispatcher.onInlineKeyboardButton { implicit uc =>
    for (guildStockReq <- extractInlineData[GuildStockReq]) yield showStock(guildStockReq)
  }

  private def showStock(req: GuildStockReq)(implicit sc: SecurityContext): Future[Reply] = {
    for (
      prices <- itemPricesService.getPrices;
      stock <- guildStockService.auth().getGuildStock
    ) yield {
      val itemsWithRequiredCategory = stock.items.filter(t => t.category == req.category)
      val pricedItems = ItemSet(itemsWithRequiredCategory).withPrices(prices)

      val helper = new StockMessageHelper(
        "<b>На складе у членов гильдии:</b>\n",
        req.sort,
        sort => req.copy(sort = sort)
      )
      replyEdit(helper.generateText(pricedItems), helper.generateButtons())
    }
  }
}
