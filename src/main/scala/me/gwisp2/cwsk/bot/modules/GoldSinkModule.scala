package me.gwisp2.cwsk.bot.modules

import akka.NotUsed
import akka.stream.scaladsl.Source
import com.google.inject.Inject
import me.gwisp2.cwsk.Schedules
import me.gwisp2.cwsk.bot.declarative.UpdateContext
import me.gwisp2.cwsk.bot.modules.GoldSinkModule._
import me.gwisp2.cwsk.bot.{CwskBotModule, CwskUpdateDispatcher}
import me.gwisp2.cwsk.dto.ServiceException
import me.gwisp2.cwsk.model.types.GoldSinkRule
import me.gwisp2.cwsk.service.GoldSinkService.GoldSinkHistory
import me.gwisp2.cwsk.service.{GoldSinkConfigService, GoldSinkService}
import me.gwisp2.scheduler.{Schedule, ScheduledAction, Scheduler}
import me.gwisp2.tbot.Reply
import me.gwisp2.tbot.Reply.ReplyStreaming

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.Try

class GoldSinkModule @Inject()(
                                dispatcher: CwskUpdateDispatcher,
                                goldSinkService: GoldSinkService,
                                goldSinkConfigService: GoldSinkConfigService,
                                scheduler: Scheduler
                              ) extends CwskBotModule {
  private val possibleAutosinkTimeslots = Seq(30, 15, 10, 9)
  private val defaultAutosinkTimeslot = 15

  {
    /* Automatic gold sink */
    for (t <- possibleAutosinkTimeslots) {
      scheduler.runOnSchedule(new ScheduledAction {
        override def name: String = s"Gold sink ($t)"

        override def schedule: Schedule = Schedules.battle.shift((-t).minutes)

        override def run(): Unit = runAutomaticGoldSink(Some(t))
      })
    }
    scheduler.runOnSchedule(new ScheduledAction {
      override def name: String = s"Gold sink (default)"

      override def schedule: Schedule = Schedules.battle.shift((-defaultAutosinkTimeslot).minutes)

      override def run(): Unit = runAutomaticGoldSink(None)
    })
  }

  private def runAutomaticGoldSink(timeslot: Option[Int]): Unit = {
    for (progresses <- goldSinkService.startAutomaticGoldSink(timeslot)) {
      for ((userId, progress) <- progresses) {
        val reply = replyGoldSinkProgress(userId, progress)
        dispatcher.handleReply(reply)
      }
    }
  }

  dispatcher.onMessage { implicit uc =>
    for (_ <- extractButton(ButtonMoney); sender <- uc.sender) yield {
      for ((autosinkEnabled, sinkLimit, timeslot, userRules, guildRules) <- goldSinkConfigService.getUserSettings(sender.id)) yield {
        val fAutosinkStatus = if (autosinkEnabled) "✅ Слив голды перед битвой в :45 включён, /gs_disable"
        else "\uD83D\uDEAB Слив голды перед битвой отключен, /gs_enable"
        val fSinkLimit = f"Ограничение на количество голды: ${sinkLimit.getOrElse("[отсутствует]")}, /gs_limit"
        val possibleTimeslotCommands = possibleAutosinkTimeslots.map(t => s"/gst_$t").mkString(", ")
        val fAutosinkTimeslot = f"Автослив за ${timeslot.getOrElse(defaultAutosinkTimeslot)} минут до битвы, $possibleTimeslotCommands"

        reply(s"<b>Слив голды.</b>\n\nЛичные правила:\n${formatGsRules(userRules)}\n\n" +
          s"Правила гильдии:\n${formatGsRules(guildRules)}\n\nСначала выполняются личные правила, потом," +
          " если осталось золото, выполняются правила гильдии.\nДля задания" +
          s" правил используй команду <code>/gs</code> или <code>/gs_guild</code>.\n" +
          s"Пример: <pre>/gs\nCoke 15\nCharcoal 4</pre>Покупать Coke не более, чем за 15💰, а Charcoal за 4💰.\n\n$fSinkLimit\n$fAutosinkStatus\n$fAutosinkTimeslot",
          Some(Seq(Seq(ButtonStartMoneySink), Seq(ButtonBack))))
      }
    }
  }

  dispatcher.onMessage { implicit uc =>
    for (_ <- extractButton(ButtonStartMoneySink); r <- sinkMoney) yield r
  }

  for (t <- possibleAutosinkTimeslots) {
    dispatcher.onCommand(s"gst_$t") { implicit uc =>
      for (sender <- uc.sender) yield {
        changeAutosinkTimeslot(sender.id, t)
      }
    }
  }

  dispatcher.onCommand("gs_enable") { implicit uc =>
    for (sender <- uc.sender) yield {
      changeAutosinkStatus(sender.id, newStatus = true)
    }
  }

  dispatcher.onCommand("gs_disable") { implicit uc =>
    for (sender <- uc.sender) yield {
      changeAutosinkStatus(sender.id, newStatus = false)
    }
  }

  private def sinkMoney(implicit uc: UpdateContext) = {
    for (sender <- uc.sender) yield {
      val goldSinkProgress = goldSinkService.startGoldSink(sender.id)
      Future.successful(replyGoldSinkProgress(sender.id, goldSinkProgress))
    }
  }

  private def replyGoldSinkProgress(chatId: Long, progress: Source[GoldSinkHistory, NotUsed]): Reply = {
    ReplyStreaming(chatId, progress.map(
      _.lines.mkString("<b>Слив голды начат</b>\n", "\n", "")
    ))
  }

  private def changeAutosinkStatus(userId: Int, newStatus: Boolean) = {
    for (_ <- goldSinkConfigService.updateUserAutosinkStatus(userId, newStatus)) yield {
      val newStatusText = if (newStatus) "✅активирован" else "\uD83D\uDEABотменён"
      reply(s"<b>Слив голды перед битвой $newStatusText</b>")
    }
  }

  private def changeAutosinkTimeslot(userId: Int, timeslot: Int) = {
    for (_ <- goldSinkConfigService.updateAutosinkTimeslot(userId, Some(timeslot))) yield {
      reply(s"<b>Время слива голды изменено на $timeslot ${plural(timeslot, "минуту", "минуты", "минут")} до битвы</b>")
    }
  }

  dispatcher.onCommand("gs_limit") { implicit uc =>
    for (sender <- uc.sender; args <- extractCommand) yield {
      val sinkLimit = if (args.isEmpty) None
      else Some(Try(args.toInt).getOrElse(throw new ServiceException("Лимит голды должен быть числом")))
      goldSinkConfigService.updateUserSinkLimit(sender.id, sinkLimit).map { _ =>
        val fSinkLimit = sinkLimit.map(_.toString).getOrElse("[отсутствует]")
        reply(s"<b>Лимит количества голды обновлён: </b>$fSinkLimit<pre>Бот не будет сливать голду, если её количество превысит заданное.</pre>")
      }
    }
  }

  dispatcher.onCommand("gs") { implicit uc =>
    for (sender <- uc.sender; args <- extractCommand) yield {
      goldSinkConfigService.updateUserRule(sender.id, args).map { newRule =>
        reply(s"<b>Личные правила слива обновлены</b>\n${formatGsRules(newRule)}")
      }
    }
  }

  dispatcher.onCommand("gs_guild") { implicit uc =>
    for (sender <- uc.sender; args <- extractCommand) yield {
      goldSinkConfigService.updateGuildRule(sender.id, args).map { newRule =>
        reply(s"<b>Правила слива гильдии обновлены</b>\n${formatGsRules(newRule)}")
      }
    }
  }

  private def formatGsRules(rules: Option[GoldSinkRule]): String = {
    val rulesText = rules.map(_.toText).getOrElse("[не заданы]")
    s"<pre>$rulesText</pre>"
  }
}

object GoldSinkModule {
  val ButtonMoney = "\uD83D\uDCB0Слив голды"
  val ButtonStartMoneySink = "▶Начать слив голды"
  val ButtonBack: String = MeModule.ButtonBack
}