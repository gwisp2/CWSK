package me.gwisp2.cwsk.bot.modules

import com.google.inject.Inject
import me.gwisp2.cwsk.bot.{CwskBotModule, CwskUpdateDispatcher}
import me.gwisp2.cwsk.service.UserSettingsService.{SettingsSnapshot, SettingsValue}
import me.gwisp2.cwsk.service.{SecurityContext, UserSettingsService}
import me.gwisp2.tbot.Reply

class SettingsModule @Inject()(dispatcher: CwskUpdateDispatcher, userSettingsService: UserSettingsService) extends CwskBotModule {

  import SettingsModule._

  userSettingsService.settingsKeys.foreach { key =>
    key.allCommands.foreach { cmd =>
      dispatcher.onCommand(cmd.name) { implicit uc =>
        for (args <- extractCommand) yield {
          val value = cmd.getValue(args)
          for (newSettings <- userSettingsService.auth().changeSettings(SettingsValue(key, value))) yield {
            val comment = Some(s"[[ <b>${key.displayName}:</b> ${key.valueToHtml(value)} ]]")
            showSettings(newSettings, comment)
          }
        }
      }
    }
  }

  dispatcher.onMessage { implicit uc =>
    for (_ <- extractButton(ButtonSettings)) yield {
      showUserSettings(uc)
    }
  }

  private def showUserSettings(implicit sc: SecurityContext) = {
    for (settings <- userSettingsService.auth().getSettings) yield {
      showSettings(settings, None)
    }
  }

  private def showSettings(snapshot: SettingsSnapshot, comment: Option[String]): Reply = {
    val commentPrefix = comment match {
      case Some(c) => c + "\n\n"
      case _ => ""
    }
    val settingsHtml = snapshot.settings.map { v =>
      formatSettingsValue(v)
    }.mkString("\n")

    reply(commentPrefix + settingsHtml)
  }

  private def formatSettingsValue[T](kv: SettingsValue[T]): String = {
    val keyHtml = s"<b>${kv.key.displayName}: </b>"
    val valueHtml = kv.key.valueToHtml(kv.value)
    val commands = kv.key.commands(kv.value).map("/" + _.name).mkString(", ")
    val descriptionHtml = kv.key.description

    s"$keyHtml$valueHtml, $commands\n$descriptionHtml\n"
  }
}

object SettingsModule {
  val ButtonSettings = "⚙️ Настройки"
}
