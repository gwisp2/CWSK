package me.gwisp2.cwsk.bot.modules.helper

import me.gwisp2.cwsk.dto.items.PricedStockItem.PricedItemSort
import me.gwisp2.cwsk.dto.items._

class StockMessageHelper[B](prefix: String, currentSort: PricedItemSort, buttonGen: PricedItemSort => B) {
  def generateText(stock: PricedItemSet): String = {
    val sortedItems = stock.sorted(currentSort.ordering)
    prefix + sortedItems.format()
  }

  def generateButtons(): Some[Seq[Seq[(String, B)]]] = {
    val row = PricedItemSortType.all.map { t =>
      if (t == currentSort.`type`) {
        (s"($currentSort)", buttonGen(currentSort.reverse))
      } else {
        val sort = t.toSort
        (sort.toString, buttonGen(sort))
      }
    }

    Some(Seq(row))
  }
}
