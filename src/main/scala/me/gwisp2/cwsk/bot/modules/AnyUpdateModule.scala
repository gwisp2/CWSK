package me.gwisp2.cwsk.bot.modules

import com.google.inject.Inject
import info.mukel.telegrambot4s.models.{Update, User}
import me.gwisp2.cwsk.bot.CwskUpdateDispatcher.CwskUpdateContext
import me.gwisp2.cwsk.bot.{CwskBotModule, CwskUpdateDispatcher}
import me.gwisp2.cwsk.service.UserService
import me.gwisp2.tbot.Result

class AnyUpdateModule @Inject()(dispatcher: CwskUpdateDispatcher, userService: UserService) extends CwskBotModule {
  dispatcher.onMessage { implicit uc =>
    logUpdate(uc)
    updateSenderUser(uc.update)

    /* Continue processing update */
    Result.Empty
  }
  dispatcher.onInlineQuery { implicit uc =>
    _ =>
      logUpdate(uc)
      Result.Empty
  }
  dispatcher.onInlineKeyboardButton { implicit uc =>
    logUpdate(uc)
    Result.Empty
  }

  private def logUpdate(uc: CwskUpdateContext): Unit = {
    val update = uc.update

    for (message <- update.message; sender <- message.from) {
      val name = formatSender(sender)
      val text = message.text.map(_.take(30).takeWhile(_ != '\n')).getOrElse("[no text]")

      logger.info(s"(message) $name: $text")
    }
    for (inlineQuery <- update.inlineQuery; sender = inlineQuery.from) {
      val name = formatSender(sender)
      val text = inlineQuery.query.take(30)

      logger.info(s"(inlineq) $name: $text")
    }
    for (callbackQuery <- update.callbackQuery; sender = callbackQuery.from) {
      val name = formatSender(sender)
      val text = uc.inlineData.map(_.toString).getOrElse("[unknown]")

      logger.info(s"(ibutton) $name: $text")
    }
  }

  private def formatSender(sender: User) = {
    sender.username.map(u => s"@$u").getOrElse(sender.firstName)
  }

  private def updateSenderUser(update: Update): Unit = {
    for (message <- update.message; sender <- message.from) {
      logErrors(userService.updateUserAndMarkAsActive(sender))
    }
  }
}
