package me.gwisp2.cwsk.bot.modules

import com.google.inject.Inject
import me.gwisp2.cwsk.bot.inline.{EditPermissions, ShowPermissions}
import me.gwisp2.cwsk.bot.parsed.GuildCommanderMessage
import me.gwisp2.cwsk.bot.{CwskBotModule, CwskUpdateDispatcher}
import me.gwisp2.cwsk.dto._
import me.gwisp2.cwsk.service.{GuildRolesService, SecurityContext, UserService}
import me.gwisp2.tbot.Reply

import scala.concurrent.Future

class GuildRolesModule @Inject()(dispatcher: CwskUpdateDispatcher,
                                 guildRolesService: GuildRolesService,
                                 userService: UserService
                                ) extends CwskBotModule {
  dispatcher.onMessage { implicit uc =>
    for (message <- extractCwForward[GuildCommanderMessage] if forwardIsRecent) yield {
      for (_ <- guildRolesService.transferLeadership(message.name, message.commanderName)) yield {
        reply("Лидер гильдии изменён")
      }
    }
  }

  dispatcher.onCommand("guild_roles") { implicit uc =>
    implicit val ordering: Ordering[ShortChar] = Ordering.by((c: ShortChar) => (c.role.isLeader, c.role.permissions.size)).reverse

    for (guildInfo <- userService.auth().getFullGuildInfo()) yield {
      val leaderIsUnknown = !guildInfo.members.exists(_.value.role.isLeader)
      val footer = Seq(
        Some("\n\nНажми на одну из кнопок для изменения прав"),
        if (leaderIsUnknown) Some(
          s"""Для того, чтобы я знал лидера, сделай форвард такого сообщения из игры:
             |<pre>⚱️[URN] Urn Warriors
             |Commander: hydroumicro</pre>
          """.stripMargin
        ) else None
      ).flatten.mkString("\n\n")

      replyPermissionsEdit(guildInfo, (sc: ShortChar) => {
        val role = sc.role
        if (role.isLeader) "\uD83D\uDC51"
        else role.permissions match {
          case GuildPermission.ValueSet.empty => "[пусто]"
          case nonEmpty => nonEmpty.mkString(", ")
        }
      }, footer)
    }
  }

  private def replyPermissionsEdit[T](g: ReifiedGuild[T], toHtml: T => String, footer: String)(implicit tOrdering: Ordering[T]): Reply = {
    val ordering: Ordering[ReifiedChar[T]] = Ordering.by(c => (c.value, c.char.userId))
    val members = g.members.sorted(ordering)

    val fGuildInfo = formatGuildName(g.guild)
    val fMembersInfo = members.zip(Stream.from(1)).map { case (member, index) =>
      val fChar = formatCharClassAndName(member.char)
      s"<b>#$index</b> $fChar — ${toHtml(member.value)}"
    }.mkString("\n")
    val buttons = members.zip(Stream.from(1)).map { case (member, index) =>
      (index.toString, ShowPermissions(member.char.userId))
    }.grouped(8).toSeq

    reply(s"$fGuildInfo\n$fMembersInfo$footer", Some(buttons))
  }

  dispatcher.onInlineKeyboardButton { implicit uc =>
    for (data <- extractInlineData[ShowPermissions]) yield {
      showPermissions(data.userId)
    }
  }

  dispatcher.onInlineKeyboardButton { implicit uc =>
    for (data <- extractInlineData[EditPermissions]) yield {
      editPermissions(data.userId, data.permissionId, data.allowed)
    }
  }

  private def showPermissions(userId: Int)(implicit sc: SecurityContext): Future[Reply] = {
    for (rc <- guildRolesService.auth().getGuildRole(userId)) yield {
      permissionsReply(rc)
    }
  }

  private def editPermissions(userId: Int, permission: GuildPermission.Permission, allowed: Boolean)(implicit sc: SecurityContext): Future[Reply] = {
    val modification = if (allowed) {
      permissions: GuildPermission.ValueSet => permissions + permission
    } else {
      permissions: GuildPermission.ValueSet => permissions - permission
    }
    for (rc <- guildRolesService.auth().modifyPermissions(userId, modification)) yield {
      permissionsReply(rc)
    }
  }

  private def permissionsReply(rc: ReifiedChar[GuildRole]) = {
    val name = formatCharClassAndName(rc.char)
    val buttons = GuildPermission.values.map { permission =>
      val permissionPresent = rc.value.permissions.contains(permission)
      val buttonIcon = if (permissionPresent) "✅" else "\uD83D\uDEAB"
      val buttonText = buttonIcon + permission
      val buttonData = EditPermissions(rc.char.userId, permission, !permissionPresent)
      (buttonText, buttonData)
    }.map(Seq(_)).toSeq
    replyEdit(name, Some(buttons))
  }
}
