package me.gwisp2.cwsk.bot.modules

import java.util.concurrent.ConcurrentHashMap

import com.google.inject.Inject
import info.mukel.telegrambot4s.models.{InlineKeyboardButton, InlineQueryResultArticle, InputTextMessageContent}
import me.gwisp2.cwsk.bot.inline.{InlineData, NoData, UseItem, UserStockReq}
import me.gwisp2.cwsk.bot.modules.helper.StockMessageHelper
import me.gwisp2.cwsk.bot.{CwskBotModule, CwskUpdateDispatcher}
import me.gwisp2.cwsk.dto.ItemsParser
import me.gwisp2.cwsk.dto.items.{PricedItemSortType, StockItem, StockItemType}
import me.gwisp2.cwsk.service.{ItemPricesService, SecurityContext, StockService}
import me.gwisp2.tbot.Reply

import scala.concurrent.Future

class StockCommandsModule @Inject()(
                                     dispatcher: CwskUpdateDispatcher,
                                     stockService: StockService,
                                     itemPricesService: ItemPricesService
                                   ) extends CwskBotModule {

  import StockCommandsModule._

  private val stocks = new ConcurrentHashMap[Int, Vector[StockItem]]()
  private val lastUsedModes = new ConcurrentHashMap[Int, InlineMode]()

  {
    stockService.stockUpdatedEvent.registerListener { case event =>
      /* Clear outdated stock data */
      stocks.remove(event.userId)
    }

    dispatcher.onCommand("stock") { implicit uc =>
      handleStock(UserStockReq(PricedItemSortType.ByTotalPrice().toSort))
    }

    dispatcher.onInlineButton[UserStockReq, Future[Reply]] { implicit uc =>
      req =>
        handleStock(req)
    }

    dispatcher.onInlineQuery { implicit uc =>
      inlineQuery =>
        val query = inlineQuery.query
        val defaultMode = Option(lastUsedModes.get(inlineQuery.from.id))
        val decomposedQuery = InlineMode.decomposeQuery(query).orElse(
          defaultMode.map(mode => (mode, query))
        )

        decomposedQuery match {
          case Some((mode, q)) =>
            lastUsedModes.put(inlineQuery.from.id, mode)
            mode match {
              case InlineMode.Gd => handleGd(q)
              case InlineMode.Hide => handleHide(q)
              case InlineMode.ToCraftTable => handleCraftTable(q)
              case InlineMode.Withdraw => handleWithdraw(q)
            }
          case None => handleDefault()
        }
    }

    dispatcher.onChosenInlineResult { implicit uc =>
      chosenResult =>
        val result = InlineData.fromString(chosenResult.resultId)
        result match {
          case Some(UseItem(_, ingameId, amount)) =>
            stocks.compute(chosenResult.from.id,
              (_: Int, oldItems: Vector[StockItem]) => Option(oldItems).map(removeItem(_, ingameId, amount)).orNull
            )
          case _ => /* Ignore */
        }
        Reply.Empty
    }

    dispatcher.onMessage { implicit uc =>
      for (_ <- extractButton(ButtonStock)) yield {
        reply(
          """
            | Можно удобно закидывать ресурсы в гильдию и из неё, а также прятать их на бирже.
            |
            | Нажми на одну из кнопок ниже и выбери <code>@ChatWarsBot</code>.
            | <b>Не забудь перед этим обновить сток:</b> /update.
          """.stripMargin, Some(InlineMode.all.map(mode =>
            InlineKeyboardButton(mode.buttonText, switchInlineQuery = Some(mode.prefix))
          ).grouped(2).toSeq))
      }
    }
  }

  private def handleStock(req: UserStockReq)(implicit sc: SecurityContext): Future[Reply] = {
    for (
      stock <- stockService.auth().getStock;
      prices <- itemPricesService.getPrices
    ) yield {
      val helper = new StockMessageHelper[UserStockReq](
        "<b>Твой склад:</b>\n",
        req.sort,
        sort => req.copy(sort = sort)
      )
      val stockWithPrices = stock.withPrices(prices)
      replyEdit(helper.generateText(stockWithPrices), helper.generateButtons())
    }
  }

  private def handleCraftTable(query: String)(implicit sc: SecurityContext): Future[Reply] = {
    handleStockAction(query, { item =>
      s"/a_${item.`type`.ingameId.get} ${item.quantity}"
    }, { item =>
      s"Положить на верстак ${item.quantity} шт. (ID: ${item.`type`.ingameId.get})"
    })
  }

  private def handleGd(query: String)(implicit sc: SecurityContext): Future[Reply] = {
    handleStockAction(query, { item =>
      s"/g_deposit ${item.`type`.ingameId.get} ${item.quantity}"
    }, { item =>
      s"Отдать в гильдию ${item.quantity} шт. (ID: ${item.`type`.ingameId.get})"
    })
  }

  private def handleHide(query: String)(implicit sc: SecurityContext): Future[Reply] = {
    handleStockAction(query, { item =>
      s"/wts_${item.`type`.ingameId.get}_${item.quantity}_1000"
    }, { item =>
      s"Спрятать на бирже ${item.quantity} шт. (ID: ${item.`type`.ingameId.get})"
    })
  }

  private def handleStockAction(query: String,
                                commandFormat: StockItem => String,
                                descriptionFormat: StockItem => String
                               )(implicit sc: SecurityContext): Future[Reply] = {
    for (stock <- getStock) yield {
      val filteredStock = stock.filter(item => query.isEmpty || item.`type`.name.toLowerCase.contains(query.toLowerCase))

      val results = filteredStock.zipWithIndex.collect {
        case (item@StockItem(StockItemType(Some(ingameId), name), quantity), index) => InlineQueryResultArticle(
          InlineData.toString(UseItem(index, ingameId, quantity)),
          s"${index + 1}. $name",
          InputTextMessageContent(commandFormat(item)),
          description = Some(descriptionFormat(item))
        )
      }.take(50)
      Reply.ReplyInlineQuery(results)
    }
  }

  private def handleWithdraw(query: String): Future[Reply] = {
    for (allItemTypes <- stockService.getKnownItems()) yield {
      val itemsParser = new ItemsParser(allItemTypes)

      val itemTypes = itemsParser.findItemTypes(query)
      val items = itemsParser.parseItem(query)

      val itemsInResult = itemTypes.map(StockItem(_, 1)) ++ items
      val results = itemsInResult.take(50).toSeq.zipWithIndex.collect {
        case (StockItem(StockItemType(Some(ingameId), name), quantity), index) => InlineQueryResultArticle(
          InlineData.toString(NoData(index)),
          s"${index + 1}. $name x $quantity",
          InputTextMessageContent(s"/g_withdraw $ingameId $quantity"),
          description = Some(s"Достать из гильдии $quantity шт. (ID: $ingameId)")
        )
      }

      Reply.ReplyInlineQuery(results)
    }
  }

  private def handleDefault(): Future[Reply] = {
    Future.successful(Reply.ReplyInlineQuery(Seq()))
  }

  private def getStock(implicit sc: SecurityContext): Future[Vector[StockItem]] = {
    Option(stocks.get(sc.effectiveUserId)) match {
      case Some(stock) => Future.successful(stock)
      case None => loadStock
    }
  }

  private def loadStock(implicit sc: SecurityContext): Future[Vector[StockItem]] = {
    for (stock <- stockService.auth().getStock) yield {
      val sortedStock = stock.items.toVector.sortBy(_.`type`.name)
      stocks.put(sc.effectiveUserId, sortedStock)
      sortedStock
    }
  }

  private def removeItem(items: Vector[StockItem], itemIngameId: String, quantity: Int): Vector[StockItem] = {
    items.flatMap { item =>
      item.`type`.ingameId match {
        case Some(`itemIngameId`) if item.quantity > quantity => Some(item.copy(quantity = item.quantity - quantity))
        case Some(`itemIngameId`) if item.quantity <= quantity => None
        case _ => Some(item)
      }
    }
  }

  private class InlineMode(val buttonText: String, val prefix: String)

  private object InlineMode {
    val Gd: InlineMode = new InlineMode("В гильдию", "d")
    val Hide: InlineMode = new InlineMode("На биржу", "h")
    val Withdraw: InlineMode = new InlineMode("Из гильдии", "w")
    val ToCraftTable: InlineMode = new InlineMode("На верстак", "a")

    val all = Seq(Gd, Hide, Withdraw, ToCraftTable)

    def decomposeQuery(query: String): Option[(InlineMode, String)] = {
      all.collectFirst {
        case mode if query == mode.prefix || query.startsWith(mode.prefix + " ")
        => (mode, query.stripPrefix(mode.prefix).trim)
      }
    }
  }

}

object StockCommandsModule {
  val ButtonStock = "\uD83D\uDCE6Склад"
}