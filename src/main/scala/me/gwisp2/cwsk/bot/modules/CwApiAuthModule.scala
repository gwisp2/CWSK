package me.gwisp2.cwsk.bot.modules

import com.google.inject.Inject
import me.gwisp2.cwsk.bot.parsed.CodeMessage
import me.gwisp2.cwsk.bot.{CwskBotModule, CwskUpdateDispatcher}
import me.gwisp2.cwsk.cwapi.methods.OperationType.OperationType
import me.gwisp2.cwsk.cwapi.methods.{AuthAdditionalOperation, CreateAuthCode, OperationType}
import me.gwisp2.cwsk.service.CwApiService
import me.gwisp2.tbot.Reply.ReplyNotifyUser

class CwApiAuthModule @Inject()(dispatcher: CwskUpdateDispatcher, cwApi: CwApiService) extends CwskBotModule {
  for (perm <- CwApiAuthModule.permissions) {
    /* /auth_X command to ask for permissions */

    dispatcher.onMessage { implicit uc =>
      for (sender <- uc.sender; _ <- extractCommand(perm.command)) yield {
        cwApi.send(sender.id, AuthAdditionalOperation.Request(perm.op))
        reply("В @ChatWarsBot был отправлен код. Сделай форвард сообщения с кодом сюда.")
      }
    }
  }

  dispatcher.onMessage { implicit uc =>
    for (sender <- uc.sender; _ <- extractCommand("auth")) yield {
      cwApi.send(CreateAuthCode.Request(sender.id))
      reply("В @ChatWarsBot был отправлен код. Сделай форвард сообщения с кодом сюда.")
    }
  }

  dispatcher.onMessage { implicit uc =>
    for (sender <- uc.sender; codeMessage <- extractCwForward[CodeMessage]) yield {
      cwApi.sendCode(sender.id, codeMessage.code)
      reply(s"Код получен: ${sanitize(codeMessage.code)}")
    }
  }

  cwApi.newTokenListenerEvent.registerListener {
    case userId =>
      dispatcher.handleReply(ReplyNotifyUser(userId, "Доступ к API получен. Осталось выдать разрешения."))
  }

  cwApi.tokenInvalidatedEvent.registerListener {
    case userId =>
      dispatcher.handleReply(ReplyNotifyUser(userId, "Доступ к API пропал. Есть хочешь пользоваться ботом, пройди /auth."))
  }

  cwApi.newPermissionEvent.registerListener {
    case (userId, opType) =>
      val opName = CwApiAuthModule.permissions.find(_.op == opType).map(_.name).getOrElse("???")
      dispatcher.handleReply(ReplyNotifyUser(userId, s"Разрешение получено: $opName"))
  }
}

object CwApiAuthModule {

  case class PermInfo(command: String, op: OperationType, name: String)

  val permissions = Array(
    PermInfo("auth_profile", OperationType.GetUserProfile, "доступ к профилю"),
    PermInfo("auth_stock", OperationType.GetStock, "доступ к складу"),
    PermInfo("auth_wtb", OperationType.TradeTerminal, "доступ к автозакупке"),
    PermInfo("auth_guild_info", OperationType.GuildInfo, "доступ к информации о гильдии [пока бесполезно]")
  )
}
