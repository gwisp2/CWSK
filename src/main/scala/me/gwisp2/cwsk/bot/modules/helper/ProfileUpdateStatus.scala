package me.gwisp2.cwsk.bot.modules.helper

import me.gwisp2.cwsk.bot.modules.helper.ProfileUpdateDelta.{DeltaProfile, DeltaStock}

case class ProfileUpdateStatus(
                                profile: Option[String] = None,
                                stock: Option[String] = None,
                              ) {
  def format(): String = {
    "\uD83D\uDE0EПрофиль: " + profile.getOrElse("[в процессе]") + "\n" +
      "\uD83D\uDCE6Склад: " + stock.getOrElse("[в процессе]")
  }

  def applyChange(delta: ProfileUpdateDelta): ProfileUpdateStatus = delta match {
    case DeltaProfile(text) => this.copy(profile = Some(text))
    case DeltaStock(text) => this.copy(stock = Some(text))
  }

  def isCompleted: Boolean = profile.nonEmpty && stock.nonEmpty
}

sealed abstract class ProfileUpdateDelta

object ProfileUpdateDelta {

  case class DeltaProfile(text: String) extends ProfileUpdateDelta

  case class DeltaStock(text: String) extends ProfileUpdateDelta

}
