package me.gwisp2.cwsk.bot.modules

import com.google.inject.Inject
import me.gwisp2.cwsk.bot.modules.MiscActionsModule._
import me.gwisp2.cwsk.bot.parsed._
import me.gwisp2.cwsk.bot.{CwskBotModule, CwskUpdateDispatcher}
import me.gwisp2.cwsk.dto.items.ItemCategory
import me.gwisp2.cwsk.service.StockService
import me.gwisp2.tbot.Reply

import scala.concurrent.Future

class MiscActionsModule @Inject()(
                                      dispatcher: CwskUpdateDispatcher,
                                      stockService: StockService
                                    ) extends CwskBotModule {

  dispatcher.onCommand("show_items") { implicit uc =>
    reply(ItemCategory.allCategories.map(c => "/show_items_" + c.name).mkString("\n"))
  }

  dispatcher.onMessage { implicit uc =>
    for (_ <- extractButton(ButtonMisc)) yield {
      reply(
        """<b>Всякие полезные и не очень штучки.</b>
          |
          |/show_items - все известные боту предметы вместе с их id.
          |<pre>Пополни знания бота с помощью форвардов из игры (/t, /more, /ws, мастерская, аукцион, свой сток и сток гильдии)</pre>
          |
          |/build_info - информация о сборке
          |<pre>Узнай, как давно разработчик не занимался ботом</pre>
          |
          |/feedback - обратная связь
          |<pre>Посмотрел /build_info и увидел, что обновлений давно нет? Пни разработчика! Нашёл баг, есть идеи по улучшению или просто хочешь оставить благодарность? Пиши!</pre>
          |
          |/about - информация о боте
          |<pre>Эта команда появилась после многократных запросов в /feedback, из какого замка разработчик</pre>
          |
          |""".stripMargin)
    }
  }

  for (itemCategory <- ItemCategory.allCategories) {
    dispatcher.onCommand("show_items_" + itemCategory.name) { implicit uc =>
      showItems(itemCategory)
    }
  }

  private def showItems(category: ItemCategory): Future[Reply] = {
    for (items <- stockService.getKnownItems()) yield {
      // Sort by (<prefix>, <number>) pairs (thus 100 is not after 10)
      val sortedItems = items
        .view
        .filter(t => ItemCategory.of(t) == category)
        .filterNot(t => t.name.startsWith("⚡"))
        .toSeq.sorted

      val message = sortedItems.map { itemType =>
        val code = itemType.ingameId.getOrElse("-")
        val name = itemType.name
        s"<code>$code</code> $name"
      }.mkString("<b>Список известных предметов:</b>\n", "\n", "\n")

      reply(message)
    }
  }

  dispatcher.onMessage { implicit uc =>
    for (shopItems <- extractCwForward[PlayerShopItemList]) yield {
      updateItemIds(shopItems.items.map(item => (item.name, item.id)))
    }
  }

  dispatcher.onMessage { implicit uc =>
    for (shopItems <- extractCwForward[NpcShopItemList]) yield {
      updateItemIds(shopItems.items.map(item => (item.name, item.id)))
    }
  }

  dispatcher.onMessage { implicit uc =>
    for (warehouse <- extractCwForward[GuildWarehouse]) yield {
      updateItemIds(warehouse.items.map(item => (item.name, item.id)))
    }
  }

  dispatcher.onMessage { implicit uc =>
    for (auctionList <- extractCwForward[AuctionItemList]) yield {
      updateItemIds(auctionList.items.map(item => (item.name, item.id)))
    }
  }

  dispatcher.onMessage { implicit uc =>
    for (tMessage <- extractCwForward[ExchangeTMessage]) yield {
      updateItemIds(Some((tMessage.itemName, tMessage.itemId)))
    }
  }

  dispatcher.onMessage { implicit uc =>
    for (stockMessage <- extractCwForward[StockMessage]) yield {
      updateItemIds(stockMessage.items.map(item => (item.name, item.id)))
    }
  }

  dispatcher.onMessage { implicit uc =>
    for (craftItemList <- extractCwForward[CraftItemList]) yield {
      updateItemIds(craftItemList.items.map(item => (item.name, item.id)))
    }
  }

  private def updateItemIds(nameIdPairs: Traversable[(String, String)]): Future[Reply] = {
    for (newItems <- stockService.updateItemIds(nameIdPairs.toMap)) yield {
      val itemsListStr = if (newItems.nonEmpty)
        newItems.map(sanitize).mkString("\n")
      else
        "[пусто]"

      reply(s"<b>У следующих предметов добавлен id:</b>\n$itemsListStr")
    }
  }
}

object MiscActionsModule {
  val ButtonMisc = "\uD83C\uDF88Всякое"
}