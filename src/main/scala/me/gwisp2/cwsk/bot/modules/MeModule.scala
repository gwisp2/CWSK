package me.gwisp2.cwsk.bot.modules

import java.time.{Duration, Instant}

import com.google.inject.Inject
import me.gwisp2.cwsk.bot.declarative.UpdateContext
import me.gwisp2.cwsk.bot.inline.ShowUser
import me.gwisp2.cwsk.bot.modules.MeModule._
import me.gwisp2.cwsk.bot.{CwskBotModule, CwskUpdateDispatcher}
import me.gwisp2.cwsk.cwapi.methods.OperationType
import me.gwisp2.cwsk.dto._
import me.gwisp2.cwsk.service.ExpHistoryService.LevelUpPrediction
import me.gwisp2.cwsk.service._
import me.gwisp2.cwsk.util.DateFormat

class MeModule @Inject()(
                          dispatcher: CwskUpdateDispatcher,
                          userService: UserService,
                          cwApi: CwApiService,
                          expHistoryService: ExpHistoryService
                        ) extends CwskBotModule {


  dispatcher.onCommand("start") { implicit uc =>
    showMe(uc)
  }
  dispatcher.onMessage { implicit uc =>
    for (_ <- extractButton(ButtonMe).orElse(extractButton(ButtonBack)); r <- showMe(uc)) yield r
  }
  dispatcher.onMessage { implicit uc =>
    for (_ <- extractButton(ButtonGuild); r <- showGuild(uc)) yield r
  }


  private def showMe(implicit uc: UpdateContext) = {
    for (sender <- uc.sender) yield {
      for (
        charInfo <- userService.getChar(sender.id);
        permissionsInfo <- cwApi.getUserInfo(sender.id);
        levelUpPrediction <- expHistoryService.getLevelUpPrediction(sender.id)
      ) yield {
        val formattedChar = charInfo match {
          case Some(c) => formatChar(c, levelUpPrediction)
          case None if permissionsInfo.permissions.contains(OperationType.GetUserProfile) =>
            "Тут могла быть информация о тебе, но её нет. Попробуй сделать /update.\n"
          case None =>
            "Тут могла быть информация о тебе, но её нет.\n"
        }
        val permissions = formatPermissions(permissionsInfo)
        reply(s"$formattedChar\n📦 /stock\n$permissions")
      }
    }
  }

  private def showGuild(implicit uc: UpdateContext) = {
    for (sender <- uc.sender) yield {
      for (
        guildInfo <- userService.getGuildInfoForUser(sender.id)
      ) yield {
        guildInfo match {
          case Some(g) =>
            val members = g.members.map(_.value).sortBy(-_.exp)
            val list = formatGuildName(g.guild) + members.zipWithIndex.map { case (c, index) =>
              val utdIcon = if (c.isUpToDate) "\uD83D\uDE0E" else "\uD83D\uDE34"
              val additionIcons =
                Some("\uD83D\uDC51").filter(_ => c.role.isLeader)
                  .mkString("")
              s"<b>#${index + 1}</b> ${sanitize(c.gameClass)}${c.level} [$utdIcon$additionIcons] ${formatCharName(c)}"
            }.mkString("\n", "\n", "\n") + "\nУправление: /guild_roles\n\uD83D\uDC5D /guild_pog \uD83D\uDCE6 /guild_stock"
            val buttons = members.zipWithIndex.map { case (c, index) =>
              (s"${index + 1}", ShowUser(c.userId))
            }.grouped(8).toSeq
            reply(list, Some(buttons))
          case None =>
            reply("Ты не состоишь в гильдии, или мне пока об этом неизвестно.")
        }
      }
    }
  }

  dispatcher.onCommand("guild_pog") { implicit uc =>
    for (guildPog <- userService.getGuildPog(None)) yield {
      reply(formatReifiedGuild(guildPog, (pog: Int) => pog.toString)(Ordering.Int.reverse))
    }
  }

  private def formatReifiedGuild[T](g: ReifiedGuild[T], toHtml: T => String, footer: String = "")(implicit tOrdering: Ordering[T]): String = {
    val ordering: Ordering[ReifiedChar[T]] = Ordering.by(c => (c.value, c.char.userId))
    val members = g.members.sorted(ordering)

    val fGuildInfo = formatGuildName(g.guild)
    val fMembersInfo = members.zip(Stream.from(1)).map { case (member, index) =>
      val fChar = formatCharClassAndName(member.char)
      s"<b>#$index</b> $fChar — ${toHtml(member.value)}"
    }.mkString("\n")

    s"$fGuildInfo\n$fMembersInfo$footer"
  }

  dispatcher.onInlineKeyboardButton { implicit uc =>
    for (showUser <- extractInlineData[ShowUser]; sender <- uc.sender) yield {
      for (optFullChar <- userService.getCharWithPermissionCheck(sender.id, showUser.userId))
        yield optFullChar match {
          case Some(fullChar) => combineReplies(
            replyCq("Готово"),
            reply(formatChar(fullChar, None))
          )
          case None => replyCq("Нет такого игрока", alert = true)
        }
    }
  }

  private def formatChar(fullChar: FullChar, levelUpPrediction: Option[LevelUpPrediction]): String = {
    val className = formatCharClassAndName(fullChar) + "\n"
    val guild = fullChar.guild.map(formatGuildName).getOrElse("")
    val id = s"🆔 ${
      fullChar.userId
    }\n"

    val prediction = levelUpPrediction.map { p =>
      val daysToLevelUp = Duration.between(Instant.now(), p.instant).toDays + 1
      val daysToLevelUpStr = if (daysToLevelUp > 0)
        s"~$daysToLevelUp ${plural(daysToLevelUp, "день", "дня", "дней")}"
      else
        "скоро"

      s"/${p.exp}, ${p.dailyExp} в сутки, $daysToLevelUpStr [/exp]"
    }.getOrElse("")

    val levelExp = s"\uD83C\uDFC5${fullChar.level} \uD83D\uDD25${fullChar.exp}$prediction\n"

    val equipmentAtk = fullChar.equipment.map(_.bonusAtk).sum
    val equipmentDef = fullChar.equipment.map(_.bonusDef).sum
    val baseAtk = fullChar.atk - equipmentAtk
    val baseDef = fullChar.`def` - equipmentDef
    val atkDef = if (fullChar.equipment.isEmpty)
      s"⚔️$baseAtk \uD83D\uDEE1$baseDef"
    else
      s"⚔️$baseAtk+$equipmentAtk \uD83D\uDEE1$baseDef+$equipmentDef"
    val updateDate = "\uD83D\uDD59 " + DateFormat.format(fullChar.updateInstant)

    val pet = fullChar.pet match {
      case Some(p) => s"${p.icon} ${p.fullName} (${p.level} lvl)"
      case None => "[питомца нет]"
    }

    val equipment = fullChar.equipment match {
      case e if e.nonEmpty =>
        val invalidEquipmentWarning = if (fullChar.equipmentIsValid) ""
        else "\n\uD83D\uDEAB <b>Данные устарели, сделай форвард </b><code>/hero</code>\n"
        e.map(item => sanitize(item.line)).mkString(
          s"\uD83C\uDFBD<b>Экипировка +$equipmentAtk⚔️+$equipmentDef🛡:</b>\n", "\n", s"\n$invalidEquipmentWarning"
        )
      case _ => "[пусто, сделай форвард <code>/hero</code>]\n"
    }
    s"$className$guild$id\n$levelExp$atkDef $updateDate\n$pet\n\n$equipment"
  }

  private def formatPermissions(info: UserCwapiInfo): String = {
    if (info.hasToken) {
      CwApiAuthModule.permissions.collect {
        case perm if !info.permissions.contains(perm.op) =>
          s"🚫 ${
            perm.name.capitalize
          }: /${
            perm.command
          }"
      }.mkString("\n")
    } else {
      "\uD83D\uDEAB Нет доступа к API: /auth"
    }
  }
}

object MeModule {
  val ButtonMe = "\uD83D\uDE0EЯ"
  val ButtonGuild = "\uD83D\uDC65Гильдия"
  val ButtonBack = "Назад"
}

