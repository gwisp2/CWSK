package me.gwisp2.cwsk.bot.modules

import java.time._

import com.google.inject.Inject
import me.gwisp2.cwsk.bot.{CwskBotModule, CwskUpdateDispatcher}
import me.gwisp2.cwsk.service.ExpHistoryService
import me.gwisp2.cwsk.util.DateFormat

class ExpModule @Inject()(
                           dispatcher: CwskUpdateDispatcher,
                           expHistoryService: ExpHistoryService
                         ) extends CwskBotModule {
  private val zoneId = ZoneId.of("Europe/Moscow")

  dispatcher.onCommand("exp") { implicit uc =>
    val end = Instant.now()
    val start = Instant.now().minus(Duration.ofDays(14)).atZone(zoneId).`with`(LocalTime.MIDNIGHT).toInstant

    for (expHistory <- expHistoryService.auth().getExpHistory(start, end)) yield {
      val firstItemByDate = expHistory.items
        .map { item => (item.at.atZone(zoneId).toLocalDate, item) }
        .groupBy { case (date, _) => date }
        .mapValues {
          _.head._2
        }

      val fExpHistory = firstItemByDate
        .values
        .toSeq
        .sortBy {
          _.at
        }
        .sliding(2)
        .map {
          case Seq(prev, cur) =>
            val curZoned = cur.at.atZone(zoneId)
            val fWeekExp = if (curZoned.getDayOfWeek == DayOfWeek.MONDAY) {
              firstItemByDate.get(curZoned.toLocalDate.minusWeeks(1)) match {
                case Some(weekAgoItem) => s", +🔥${cur.exp - weekAgoItem.exp} за неделю"
                case _ => ""
              }
            } else ""
            val fDateTime = DateFormat.formatLong(cur.at)
            val fDelta = s"+🔥${cur.exp - prev.exp}"
            s"<b>${sanitize(fDateTime)}</b>: $fDelta$fWeekExp"
          case _ => "Мало данных для анализа."
        }
        .mkString("\n")

      reply(fExpHistory)
    }
  }
}
