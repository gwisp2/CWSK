package me.gwisp2.cwsk.bot.modules

import com.google.inject.Inject
import me.gwisp2.cwsk.Schedules
import me.gwisp2.cwsk.bot.formatter.StockDeltaFormatter
import me.gwisp2.cwsk.bot.modules.helper.{ProfileUpdateDelta, ProfileUpdateStatus}
import me.gwisp2.cwsk.bot.parsed.HeroMessage
import me.gwisp2.cwsk.bot.{CwskBotModule, CwskUpdateDispatcher}
import me.gwisp2.cwsk.cwapi.ApiResponse
import me.gwisp2.cwsk.cwapi.ApiResponse.Code
import me.gwisp2.cwsk.cwapi.methods.{ApiMethod, OperationType, RequestProfile, RequestStock}
import me.gwisp2.cwsk.dto.SaveStockResult
import me.gwisp2.cwsk.model.CwApiTokensDao
import me.gwisp2.cwsk.service.UserService.ProfileUpdated
import me.gwisp2.cwsk.service._
import me.gwisp2.cwsk.util.{FutureLogger, ProgressSource, PromiseMap}
import me.gwisp2.scheduler.{Schedule, ScheduledAction, Scheduler}
import me.gwisp2.tbot.Reply
import me.gwisp2.tbot.Reply.ReplyStreaming
import slick.jdbc.JdbcBackend.Database

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.{Failure, Success}

class ProfileStockUpdateModule @Inject()(
                                          dispatcher: CwskUpdateDispatcher,
                                          userService: UserService,
                                          userSettingsService: UserSettingsService,
                                          itemPricesService: ItemPricesService,
                                          stockService: StockService,
                                          cwApi: CwApiService,
                                          scheduler: Scheduler,
                                          db: Database
                                        ) extends CwskBotModule {
  private val promisesProfileUpdated = new PromiseMap[Int, ProfileUpdated]()
  private val promisesStockUpdated = new PromiseMap[Int, SaveStockResult]()

  {
    /* Stock changes report */
    scheduler.runOnSchedule(new ScheduledAction {
      override def name: String = "Stock diffs"

      override def schedule: Schedule = Schedules.battle.shift(1.minute)
      override def run(): Unit = calculateAndSendStockDiffs()
    })
  }

  stockService.stockUpdatedEvent.registerListener { case event =>
    promisesStockUpdated.fulfill(event.userId, event)
  }

  cwApi.responseEvent.registerListener {
    case ApiResponse.Simple(RequestProfile.action, Code.Forbidden, r: ApiMethod.Forbidden) =>
      promisesProfileUpdated.reject(r.userId, new RuntimeException("Forbidden"))
    case ApiResponse.Simple(RequestStock.action, Code.Forbidden, r: ApiMethod.Forbidden) =>
      promisesStockUpdated.reject(r.userId, new RuntimeException("Forbidden"))
  }

  userService.profileUpdatedEvent.registerListener { case event =>
    promisesProfileUpdated.fulfill(event.userId, event)
    if (event.equipmentInvalidated) {
      dispatcher.handleReply(Reply.ReplyNotifyUser(
        event.userId, "❗️ Обнаружено изменение атаки или защиты. Обнови <code>/hero</code>."
      ))
    }
  }

  /* /update command */
  dispatcher.onMessage { implicit uc =>
    for (sender <- uc.sender; _ <- extractCommand("update")) yield {

      val deltaSource = ProgressSource.create[ProfileUpdateDelta] { progress =>
        /* On stock updated */
        promisesStockUpdated.resultFuture(sender.id).onComplete {
          case Success(event) =>
            val stockUpdateFuture = for (
              prices <- itemPricesService.getPrices
            ) yield {
              val newItems = event.itemsWithoutKnownIds
              val itemsWithUnknownIdList =
                if (newItems.nonEmpty)
                  newItems.mkString("Будет неплохо, если ты мне подскажешь id следующих предметов: ", ", ",
                    ".\n<pre>Делать это необязательно, но если хочешь помочь, можешь кинуть форварды сообщений, где виден id этих предметов.</pre>\n")
                else
                  ""

              val formattedDelta = new StockDeltaFormatter(short = true).format(event.delta, prices)
              val message = s"<b>обновлён</b> $formattedDelta\n\n$itemsWithUnknownIdList\n"
              progress.consume(ProfileUpdateDelta.DeltaStock(message))
            }
            FutureLogger.logFailure(logger, stockUpdateFuture)
          case _ =>
            progress.consume(ProfileUpdateDelta.DeltaStock("<b>нет доступа</b>"))
        }

        /* On profile updated */
        promisesProfileUpdated.resultFuture(sender.id).onComplete {
          case Success(_) => progress.consume(ProfileUpdateDelta.DeltaProfile("<b>обновлён</b>"))
          case _ => progress.consume(ProfileUpdateDelta.DeltaProfile("<b>нет доступа</b>"))
        }

        /* Send API request */
        cwApi.send(sender.id, RequestProfile.Request())
        cwApi.send(sender.id, RequestStock.Request())
      }

      val textSource = deltaSource
        .scan(ProfileUpdateStatus())((s, d) => s.applyChange(d))
        .takeWhile(!_.isCompleted, inclusive = true)
        .map(_.format())

      ReplyStreaming(sender.id, textSource)
    }
  }

  /* Update /hero */
  dispatcher.onMessage { implicit uc =>
    for (sender <- uc.sender; heroMessage <- extractCwForward[HeroMessage]) yield {
      for (_ <- userService.updateHero(sender.id, heroMessage))
        yield reply("<b>Снаряжение обновлено.</b>")
    }
  }

  /* Update all profiles before battle */
  {
    scheduler.runOnSchedule(new ScheduledAction {
      override def name: String = "Update profiles"

      override def schedule: Schedule = Schedules.battle.shift((-5).minutes)
      override def run(): Unit = updateAll()
    })
  }

  /* /update_all command */
  dispatcher.onMessage { implicit uc =>
    for (_ <- extractCommand("update_all")) yield {
      updateAll()
      reply("Обновление начато")
    }
  }

  /* /send_stock_diffs command */
  dispatcher.onMessage { implicit uc =>
    for (_ <- extractCommand("send_stock_diffs")) yield {
      calculateAndSendStockDiffs()
      reply("Обновление стоков начато")
    }
  }

  private def updateAll(): Unit = {
    for (
      userIdsToUpdateProfile <- db.run(CwApiTokensDao.getActiveUserIdsWithPermission(OperationType.GetUserProfile));
      userIdsToUpdateStock <- db.run(CwApiTokensDao.getActiveUserIdsWithPermission(OperationType.GetStock))
    ) yield {
      userIdsToUpdateProfile.foreach {
        cwApi.send(_, RequestProfile.Request())
      }
      userIdsToUpdateStock.foreach {
        cwApi.send(_, RequestStock.Request())
      }
    }
  }

  private def calculateAndSendStockDiffs(): Unit = {
    for (
      userIdsCanUpdateStock <- db.run(CwApiTokensDao.getActiveUserIdsWithPermission(OperationType.GetStock));
      itemPrices <- itemPricesService.getPrices
    ) {
      userIdsCanUpdateStock.foreach { userId =>
        val f = for (
          stockDiffEnabled <- userSettingsService.getSettingsValue(userId, userSettingsService.settingsKeyStockDiff);
          r <- if (stockDiffEnabled) {
            val stockUpdateFuture = promisesStockUpdated.resultFuture(userId)
            cwApi.send(userId, RequestStock.Request())
            for (
              event <- stockUpdateFuture
            ) yield {
              val formattedStockDelta = new StockDeltaFormatter(short = false).format(event.delta, itemPrices)
              dispatcher.handleReply(Reply.ReplyNotifyUser(event.userId, formattedStockDelta))
            }
          } else {
            Future.successful(())
          }
        ) yield r

        f.onComplete {
          case Success(_) => /* Everything is OK */
          case Failure(exception) => logger.warn("Error in calculateAndSendStockDiffs", exception)
        }
      }
    }
  }
}
