package me.gwisp2.cwsk.bot.modules

import com.google.inject.Inject
import me.gwisp2.cwsk.bot.{CwskBotModule, CwskUpdateDispatcher}
import me.gwisp2.cwsk.cwapi.messages.{CwApiMessage, DealsPayload}
import me.gwisp2.cwsk.service.{CwApiService, UserSettingsService}
import me.gwisp2.tbot.Reply

import scala.concurrent.Future
import scala.util.{Failure, Success}

class SexModule @Inject()(
                           dispatcher: CwskUpdateDispatcher,
                           userSettings: UserSettingsService,
                           cwApi: CwApiService
                         ) extends CwskBotModule {
  cwApi.dealsEvent.registerListener {
    case CwApiMessage(_, payload) => handleDeal(payload).onComplete {
      case Success(reply) => dispatcher.handleReply(reply)
      case Failure(exception) => logger.warn("Error handling deal", exception)
    }
  }

  private def handleDeal(payload: DealsPayload): Future[Reply] = {
    for (
      optUserId <- cwApi.getUserIdByCwId(payload.sellerId);
      r <- optUserId match {
        case Some(userId) => handleDealFromUser(payload, userId)
        case None => Future.successful(Reply.Empty)
      }
    ) yield r
  }

  private def handleDealFromUser(payload: DealsPayload, userId: Int) = {
    for (notificationEnabled <- userSettings.getSettingsValue(userId, userSettings.settingsSexNotify)) yield {
      if (notificationEnabled) {
        val sum = payload.price * payload.qty
        val text =
          s"""⚖️Ты продал <b>${sanitize(payload.item)}</b> за $sum💰 (${payload.qty} x ${payload.price}💰)
             |Покупатель: ${payload.buyerCastle}${payload.buyerName}""".stripMargin
        Reply.ReplyNotifyUser(userId, text)
      } else {
        Reply.Empty
      }
    }
  }
}
