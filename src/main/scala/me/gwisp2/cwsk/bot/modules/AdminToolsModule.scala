package me.gwisp2.cwsk.bot.modules

import com.google.inject.Inject
import info.mukel.telegrambot4s.methods.{ParseMode, SendMessage}
import info.mukel.telegrambot4s.models.ChatId
import me.gwisp2.cwsk.bot.{CwskBotModule, CwskUpdateDispatcher}
import me.gwisp2.cwsk.model.Tables._
import me.gwisp2.cwsk.service.UserService
import me.gwisp2.cwsk.util.ProgressSource
import me.gwisp2.cwsk.util.ProgressSource.ProgressConsumer
import me.gwisp2.tbot.Reply
import slick.jdbc.JdbcBackend.Database
import slick.jdbc.MySQLProfile.api._

import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

class AdminToolsModule @Inject()(
                                  dispatcher: CwskUpdateDispatcher,
                                  userService: UserService,
                                  db: Database
                                ) extends CwskBotModule {
  dispatcher.onCommand("broadcast") { implicit uc =>
    for (sender <- uc.sender; message <- extractCommand if message != "") yield {
      for (_ <- userService.checkIsAdmin) yield {
        val source = ProgressSource.create[String] { progress =>
          db.run(Chars.map(_.id).result).onComplete {
            case Success(ids) =>
              broadcastMessage(progress, message, ids)
            case Failure(exception) =>
              logger.warn("Error loading list of character ids", exception)
              progress.complete()
          }
        }

        Reply.ReplyStreaming(sender.id, source)
      }
    }
  }

  private def broadcastMessage(progress: ProgressConsumer[String], message: String, userIds: Seq[Int]): Unit = {
    var countTried = 0
    var countOk = 0
    var countFailure = 0

    def updateProgress(result: Try[Unit]): Unit = {
      countTried += 1

      if (result.isSuccess) {
        countOk += 1
      }
      if (result.isFailure) {
        countFailure += 1
      }

      progress.consume(
        s"""Отправлено: $countTried/${userIds.length}
           |Успешно: $countOk
           |С ошибкой: $countFailure
           |""".stripMargin)
    }

    val future = userIds.foldLeft(Future.successful(())) {
      case (prev, next) => prev.transformWith { result =>
        updateProgress(result)
        sendMessage(next, message)
      }
    }

    future.onComplete { result =>
      progress.complete()
    }
  }

  private def sendMessage(userId: Int, message: String) = {
    dispatcher.bot.request(SendMessage(
      ChatId(userId),
      message,
      parseMode = Some(ParseMode.HTML),
      disableWebPagePreview = Some(true)
    )).map(_ => ())
  }
}
