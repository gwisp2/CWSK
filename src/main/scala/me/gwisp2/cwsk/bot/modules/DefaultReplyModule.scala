package me.gwisp2.cwsk.bot.modules

import com.google.inject.Inject
import me.gwisp2.cwsk.bot.{CwskBotModule, CwskUpdateDispatcher}

class DefaultReplyModule @Inject()(dispatcher: CwskUpdateDispatcher) extends CwskBotModule {
  dispatcher.onMessage { implicit uc =>
    reply("\uD83D\uDE43 Что-то непонятненькое")
  }
}
