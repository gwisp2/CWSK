package me.gwisp2.cwsk.bot

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.google.inject.{AbstractModule, Inject, Injector}
import com.typesafe.scalalogging.StrictLogging
import info.mukel.telegrambot4s.api.RequestHandler
import info.mukel.telegrambot4s.models.Update
import me.gwisp2.cwsk.bot.CwskBot.BotConfig
import me.gwisp2.cwsk.bot.declarative.TelegramBotEx
import me.gwisp2.cwsk.dto._
import me.gwisp2.scheduler.Scheduler
import me.gwisp2.tbot._
import net.codingwell.scalaguice.InjectorExtensions.ScalaInjector
import net.codingwell.scalaguice.ScalaModule

import scala.concurrent.ExecutionContextExecutor

class CwskBot @Inject()(
                         val system: ActorSystem,
                         val materializer: ActorMaterializer,
                         config: BotConfig,
                         injector: Injector,
                         scheduler: Scheduler,
                         telegramErrorHandler: TelegramErrorHandler
                       ) extends TelegramBotEx with StrictLogging {
  override val executionContext: ExecutionContextExecutor = system.dispatcher
  override val request: RequestHandler = {
    val akkaClient = new ErrorHookableAkkaClient(token)(system, materializer, executionContext)
    akkaClient.onErrorResponse { case response => telegramErrorHandler.handle(response) }
    new ThrottlingRequestHandler(new ReliableRequestHandler(akkaClient)(system))(executionContext, scheduler)
  }

  /* It have to be lazy to avoid initialization order issues */
  lazy val token: String = config.token

  private val dispatcher = new CwskUpdateDispatcher(this)
  private val childInjector = injector.createChildInjector(new AbstractModule with ScalaModule {
    override def configure(): Unit = {
      bind[CwskUpdateDispatcher].toInstance(dispatcher)
    }
  })

  def addModule[T <: CwskBotModule : Manifest](): Unit = {
    /* Instance creation is enough  */
    childInjector.instance[T]
  }

  dispatcher.onFailure { (u, throwable) =>
    throwable match {
      case e: ServiceException =>
        if (u.callbackQuery.isDefined)
          Reply.ReplyCq(e.displayMessage, alert = true)
        else
          Reply.ReplyToMessage(s"\uD83D\uDD25 ${e.displayMessage}")
      case _ =>
        logger.warn("Exception while handling update", throwable)
        Reply.ReplyToMessage(s"Что-то пошло не так")
    }
  }

  override def dispatchUpdate(update: Update): Result = dispatcher.dispatchUpdate(update)
}

object CwskBot {

  case class BotConfig(token: String)

}

