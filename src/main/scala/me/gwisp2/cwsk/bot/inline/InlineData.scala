package me.gwisp2.cwsk.bot.inline

import java.nio.ByteBuffer
import java.util.Base64

import boopickle.Default._
import me.gwisp2.cwsk.dto.GuildPermission
import me.gwisp2.cwsk.dto.items.PricedStockItem.{PricedItemSort, PricedItemSortType}
import me.gwisp2.cwsk.dto.items.StockItem.ItemSortType
import me.gwisp2.cwsk.dto.items.{ItemCategory, ItemSortType, PricedItemSortType}

sealed trait InlineData

case class ShowUser(userId: Int) extends InlineData

case class UseItem(index: Int, ingameId: String, amount: Int) extends InlineData

case class NoData(index: Int) extends InlineData

case class ShowPermissions(userId: Int) extends InlineData

case class EditPermissions(userId: Int, permissionId: GuildPermission.Value, allowed: Boolean) extends InlineData

case class GuildStockReq(category: ItemCategory, sort: PricedItemSort) extends InlineData

case class UserStockReq(sort: PricedItemSort) extends InlineData

object InlineData {

  implicit val itemSortTypePickler: Pickler[ItemSortType] =
    implicitly[Pickler[Int]].xmap(ItemSortType.byId(_).get)(ItemSortType.toId)
  implicit val pricedItemSortTypePickler: Pickler[PricedItemSortType] = compositePickler[PricedItemSortType]
    .addConcreteType[PricedItemSortType.FromItemSortType]
    .addConcreteType[PricedItemSortType.ByTotalPrice]
  implicit val itemCategoryPickler: Pickler[ItemCategory] =
    implicitly[Pickler[String]].xmap(ItemCategory.byName(_).get)(_.name)
  implicit val guildPermissionPicker: Pickler[GuildPermission.Value] =
    implicitly[Pickler[Int]].xmap(GuildPermission.byId(_).get)(_.id)

  private val inlineDataPickler = compositePickler[InlineData]
    .addConcreteType[ShowUser]
    .addConcreteType[UseItem]
    .addConcreteType[NoData]
    .addConcreteType[ShowPermissions]
    .addConcreteType[EditPermissions]
    .addConcreteType[GuildStockReq]
    .addConcreteType[UserStockReq]

  def toString(data: InlineData): String = {
    Base64.getEncoder.encodeToString(toBytes(data))
  }

  def fromString(str: String): Option[InlineData] = {
    fromBytes(Base64.getDecoder.decode(str))
  }

  def toBytes(data: InlineData): Array[Byte] = {
    val buffer = Pickle.intoBytes(data)(implicitly, inlineDataPickler)
    val arr = new Array[Byte](buffer.remaining)
    buffer.get(arr)
    arr
  }

  def fromBytes(bytes: Array[Byte]): Option[InlineData] = {
    val byteBuffer = ByteBuffer.wrap(bytes)
    Unpickle[InlineData](inlineDataPickler).tryFromBytes(byteBuffer).toOption
  }
}