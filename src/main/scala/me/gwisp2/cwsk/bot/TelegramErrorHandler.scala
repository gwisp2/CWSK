package me.gwisp2.cwsk.bot

import com.google.inject.Inject
import com.typesafe.scalalogging.StrictLogging
import info.mukel.telegrambot4s.methods.{ApiRequest, EditMessageText, SendMessage}
import info.mukel.telegrambot4s.models.ChatId
import me.gwisp2.cwsk.service.UserService
import me.gwisp2.cwsk.util.FutureLogger
import me.gwisp2.tbot.RequestResponse

class TelegramErrorHandler @Inject()(
                                      userService: UserService
                                    ) extends AnyRef with StrictLogging {
  private val ignoredErrorDescriptions = Vector(
    "Bad Request: query is too old and response timeout expired"
  )

  def handle[R](reqResp: RequestResponse[R]): Unit = {
    val requestDescription = reqResp.request.toString

    reqResp.response.description match {
      case Some(description)
        if ignoredErrorDescriptions.exists(ignoredEd => description.contains(ignoredEd)) => /* Ignore */
      case Some(description) if description.contains("Bad gateway") =>
        logger.warn("Telegram bot api has some problems: Bad gateway")
      case Some(description) if description.contains("Forbidden: bot was blocked by the user") =>
        extractUserId(reqResp.request) match {
          case Some(userId) =>
            val action = userService.markThatUserBlockedTheBot(userId)
            FutureLogger.logFailure(logger, action)
          case None => /* ignore */
        }
      case Some(description) if description.contains("Forbidden: user is deactivated") =>
        extractUserId(reqResp.request) match {
          case Some(userId) =>
            val action = userService.markThatUserIsDeactivated(userId)
            FutureLogger.logFailure(logger, action)
          case None => /* ignore */
        }
      case Some(description) =>
        val messageFormat = "Exception making telegram request.\nRequest: {}\nResponse: {}"
        logger.warn(messageFormat, requestDescription, description)
      case _ =>
        val messageFormat = "Exception making telegram request. Unable to parse response. \nRequest: {}"
        logger.warn(messageFormat, requestDescription)
    }
  }

  private def extractUserId[R](req: ApiRequest[R]): Option[Int] = {
    req match {
      case s: SendMessage => s.chatId match {
        case ChatId.Chat(id) if id > 0 => Some(id.toInt)
        case _ => None
      }
      case s: EditMessageText => s.chatId match {
        case Some(ChatId.Chat(id)) if id > 0 => Some(id.toInt)
        case _ => None
      }
      case _ => None
    }
  }
}
