package me.gwisp2.scheduler

trait ScheduledAction {
  def name: String

  def schedule: Schedule

  def run(): Unit
}
