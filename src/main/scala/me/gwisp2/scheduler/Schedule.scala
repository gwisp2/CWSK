package me.gwisp2.scheduler

import java.time.{Instant, LocalTime, ZoneId}

import scala.concurrent.duration.FiniteDuration

trait Schedule {
  def start: Option[Instant]

  def next(instant: Instant): Instant
}

object Schedule {

  case class EveryDay(localTimes: Seq[LocalTime], zoneId: ZoneId) extends Schedule {
    override def start: None.type = None

    override def next(after: Instant): Instant = {
      localTimes.map { time =>
        val afterZoned = after.atZone(zoneId)
        if (afterZoned.toLocalTime.compareTo(time) < 0) {
          // The same day
          afterZoned.`with`(time).toInstant
        } else {
          // The next day
          afterZoned.plusDays(1).`with`(time).toInstant
        }
      }.min
    }

    def shift(duration: FiniteDuration): EveryDay = {
      val newLocalTimes = localTimes.map(_.plusNanos(duration.toNanos))
      this.copy(localTimes = newLocalTimes)
    }
  }

  case class Periodically(from: Instant, rate: FiniteDuration) extends Schedule {
    override def start: Option[Instant] = Some(from)

    override def next(after: Instant): Instant = {
      if (from.isAfter(after)) {
        return from
      }

      val javaRate = java.time.Duration.ofNanos(rate.toNanos)
      val duration = java.time.Duration.between(from, after)
      val timesSinceStart = duration.dividedBy(javaRate)

      from.plus(javaRate.multipliedBy(timesSinceStart + 1))
    }
  }

}