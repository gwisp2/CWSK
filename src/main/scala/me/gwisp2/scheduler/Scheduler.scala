package me.gwisp2.scheduler

import java.time.Instant
import java.util.concurrent.{Executors, TimeUnit}

import com.typesafe.scalalogging.StrictLogging

import scala.concurrent.duration.FiniteDuration
import scala.concurrent.{Future, Promise}
import scala.util.Try

class Scheduler extends StrictLogging {
  private val executor = Executors.newSingleThreadScheduledExecutor()

  def runOnSchedule(action: ScheduledAction): Unit = {
    new ScheduledTask(action).submit()
  }

  def scheduleIn[T](duration: FiniteDuration)(action: => T): Future[T] = {
    scheduleInMillis(duration.toMillis)(action)
  }

  private def scheduleInMillis[T](millis: Long)(action: => T): Future[T] = {
    val promise = Promise[T]()
    executor.schedule(new Runnable {
      override def run(): Unit = {
        promise.complete(Try(action))
      }
    }, millis, TimeUnit.MILLISECONDS)
    promise.future
  }

  private class ScheduledTask(action: ScheduledAction) {
    def submit(): Unit = {
      action.schedule.start match {
        case Some(start) => scheduleExecutionAt(start)
        case None => scheduleExecutionAfter(Instant.now())
      }
    }

    private def scheduleExecutionAfter(after: Instant): Unit = {
      val nextEventInstant = action.schedule.next(after)
      scheduleExecutionAt(nextEventInstant)
    }

    private def scheduleExecutionAt(nextEventInstant: Instant) = {
      val now = Instant.now()
      val timeLeftToEvent = java.time.Duration.between(now, nextEventInstant)
      val timeToWait = if (!timeLeftToEvent.isNegative) timeLeftToEvent else java.time.Duration.ZERO

      logger.debug("Scheduling {} at {}", action.name, nextEventInstant)
      scheduleInMillis(timeToWait.toMillis) {
        /* Run task */
        val actionResult = Try(action.run())
        actionResult.failed.foreach {
          throwable => logger.warn("Task {} failed", action.name, throwable)
        }
        /* Schedule next */
        scheduleExecutionAfter(nextEventInstant)
      }
    }
  }

}
