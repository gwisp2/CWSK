package me.gwisp2.tbot

import akka.actor.ActorSystem
import info.mukel.telegrambot4s.api.{RequestHandler, TelegramApiException}
import info.mukel.telegrambot4s.methods.ApiRequest

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future, Promise}

class ReliableRequestHandler(base: RequestHandler)(implicit system: ActorSystem) extends RequestHandler {
  override def apply[R: Manifest](req: ApiRequest[R]): Future[R] = {
    implicit val ec: ExecutionContext = system.dispatcher

    def retry(backoff: Int): Future[R] = {
      val future = base(req)
      future.recoverWith {
        case e: TelegramApiException =>
          /* Don't retry on telegram api exceptions */
          Future.failed(e)
        case _ =>
          /* Network error */
          val p = Promise[R]()
          system.scheduler.scheduleOnce(backoff.seconds) {
            p.completeWith(retry(Math.min(backoff * 2, 120)))
          }
          p.future
      }
    }

    retry(5)
  }
}
