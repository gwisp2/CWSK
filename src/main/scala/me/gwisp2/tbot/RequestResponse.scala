package me.gwisp2.tbot

import info.mukel.telegrambot4s.methods.{ApiRequest, ApiResponse}

case class RequestResponse[R](request: ApiRequest[R], response: ApiResponse[R])