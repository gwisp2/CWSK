package me.gwisp2.tbot

import akka.stream.OverflowStrategy
import akka.stream.scaladsl.{Keep, Sink, Source}
import info.mukel.telegrambot4s.methods._
import info.mukel.telegrambot4s.models.{ChatId, InlineQueryResult, ReplyMarkup, Update}

import scala.concurrent.Future

trait Reply {
  def send(bot: TelegramBot, update: Option[Update]): Unit
}

/* We need manifest and ApiRequest object to perform request */
case class WrappedRequest[R: Manifest](request: ApiRequest[R])

trait RequestSeqReply extends Reply {
  def asRequestSeq(bot: TelegramBot, update: Option[Update]): Seq[WrappedRequest[_]]

  protected def wrap[R: Manifest](req: ApiRequest[R]) = WrappedRequest(req)

  protected def wrap[R: Manifest](optReq: Option[ApiRequest[R]]): Option[WrappedRequest[R]] = optReq.map(r => WrappedRequest(r))

  override def send(bot: TelegramBot, update: Option[Update]): Unit = {
    asRequestSeq(bot, update).foreach { wr =>
      bot.request(wr.request)
    }
  }
}

trait SingleRequestReply extends RequestSeqReply {
  def asRequest(bot: TelegramBot, update: Option[Update]): Option[WrappedRequest[_]]

  override def asRequestSeq(bot: TelegramBot, update: Option[Update]): Seq[WrappedRequest[_]] = asRequest(bot, update).toSeq
}

object Reply {

  object Empty extends SingleRequestReply {
    def asRequest(bot: TelegramBot, update: Option[Update]): Option[WrappedRequest[_]] = None
  }

  case class ReplyToMessage(text: String, replyMarkup: Option[ReplyMarkup] = None) extends SingleRequestReply {
    override def asRequest(bot: TelegramBot, update: Option[Update]): Option[WrappedRequest[_]] = {
      val optRequest = for (
        u <- update; m <- u.message.orElse(u.callbackQuery.flatMap(_.message))
      ) yield SendMessage(
        ChatId(m.chat.id),
        text,
        parseMode = Some(ParseMode.HTML),
        disableWebPagePreview = Some(true),
        disableNotification = Some(true),
        replyToMessageId = Some(m.messageId),
        replyMarkup = replyMarkup
      )

      wrap(optRequest)
    }
  }

  case class ReplyEdit(text: String, replyMarkup: Option[ReplyMarkup] = None) extends SingleRequestReply {
    override def asRequest(bot: TelegramBot, update: Option[Update]): Option[WrappedRequest[_]] = {
      val optRequest = for (
        u <- update; m <- u.callbackQuery.flatMap(_.message)
      ) yield EditMessageText(
        chatId = Some(ChatId(m.chat.id)),
        messageId = Some(m.messageId),
        text = text,
        parseMode = Some(ParseMode.HTML),
        disableWebPagePreview = Some(true),
        replyMarkup = replyMarkup
      )

      wrap(optRequest)
    }
  }

  case class ReplyOrEdit(text: String, replyMarkup: Option[ReplyMarkup] = None) extends SingleRequestReply {
    override def asRequest(bot: TelegramBot, update: Option[Update]): Option[WrappedRequest[_]] = {
      ReplyEdit(text, replyMarkup).asRequest(bot, update)
        .orElse(ReplyToMessage(text, replyMarkup).asRequest(bot, update))
    }
  }

  case class ReplyNotifyUser(userId: Int, text: String) extends SingleRequestReply {
    override def asRequest(bot: TelegramBot, update: Option[Update]): Option[WrappedRequest[_]] = {
      val req = SendMessage(
        ChatId(userId),
        text,
        parseMode = Some(ParseMode.HTML),
        disableWebPagePreview = Some(true),
        disableNotification = Some(true)
      )

      Some(wrap(req))
    }
  }

  case class ReplyRaw[R: Manifest](method: ApiRequest[R]) extends SingleRequestReply {
    override def asRequest(bot: TelegramBot, update: Option[Update]): Option[WrappedRequest[_]] = Some(wrap(method))
  }

  case class ReplyInlineQuery(results: Seq[InlineQueryResult]) extends SingleRequestReply {
    override def asRequest(bot: TelegramBot, update: Option[Update]): Option[WrappedRequest[_]] = {
      for (queryId <- update.flatMap(_.inlineQuery).map(_.id)) yield {
        wrap(AnswerInlineQuery(queryId, results, cacheTime = Some(0), isPersonal = Some(true)))
      }
    }
  }

  case class ReplyStreaming(chatId: Long, source: Source[String, _]) extends Reply {
    override def send(bot: TelegramBot, update: Option[Update]): Unit = {
      import bot.{executionContext, materializer}

      var messageIdFuture: Option[Future[Int]] = None

      val messageIdSource = source
        .buffer(1, OverflowStrategy.dropHead)
        .mapAsync(1) { element =>
          val content = if (element.nonEmpty) element else "..."
          messageIdFuture match {
            case Some(future) =>
              /* SendMessage is already issued */
              val newFuture = future.flatMap { messageId =>
                bot.request(EditMessageText(
                  chatId = Some(ChatId(chatId)),
                  messageId = Some(messageId),
                  text = content,
                  parseMode = Some(ParseMode.HTML),
                  disableWebPagePreview = Some(true)
                )).map(_ => messageId)
              }
              messageIdFuture = Some(newFuture)
              newFuture
            case None =>
              /* Send message */
              val newFuture = bot.request(SendMessage(
                ChatId(chatId),
                content,
                parseMode = Some(ParseMode.HTML),
                disableWebPagePreview = Some(true),
                disableNotification = Some(true)
              )).map(_.messageId)
              messageIdFuture = Some(newFuture)
              newFuture
          }
        }

      val runnableGraph = messageIdSource.toMat(Sink.ignore)(Keep.none)
      runnableGraph.run()
    }
  }

  case class ReplyCq(cqText: String, alert: Boolean = false) extends SingleRequestReply {
    override def asRequest(bot: TelegramBot, update: Option[Update]): Option[WrappedRequest[_]] = {
      val optRequest = for (
        u <- update; q <- u.callbackQuery
      ) yield AnswerCallbackQuery(
        q.id,
        text = Some(cqText),
        showAlert = Some(alert)
      )

      wrap(optRequest)
    }
  }

  case class Combine(replies: Traversable[Reply]) extends Reply {
    override def send(bot: TelegramBot, update: Option[Update]): Unit = replies.foreach(_.send(bot, update))
  }

}
