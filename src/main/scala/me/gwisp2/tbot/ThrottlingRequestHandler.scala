package me.gwisp2.tbot

import info.mukel.telegrambot4s.api.RequestHandler
import info.mukel.telegrambot4s.methods.{ApiRequest, EditMessageText, SendMessage}
import info.mukel.telegrambot4s.models.ChatId
import me.gwisp2.scheduler.Scheduler
import me.gwisp2.throttler.{KeyBasedThrottler, Throttler}

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

class ThrottlingRequestHandler(base: RequestHandler)(implicit ec: ExecutionContext, scheduler: Scheduler) extends RequestHandler {
  private val throttlerAllRequests = new Throttler(30, 1.second)
  private val throttlerPerChat = new KeyBasedThrottler[ChatId](1, 1.second)
  private val throttlerPerGroup = new KeyBasedThrottler[ChatId](20, 1.minute)

  override def apply[R: Manifest](req: ApiRequest[R]): Future[R] = {
    val chatId = req match {
      case r: SendMessage => Some(r.chatId)
      case r: EditMessageText => r.chatId
      case _ => None
    }
    val isGroup = chatId.exists {
      case ChatId.Chat(id) => id < 0
      case _ => true
    }

    chatId match {
      case Some(id) if isGroup =>
        throttlerPerChat.throttle(id) {
          throttlerPerGroup.throttle(id) {
            throttlerAllRequests.throttle {
              base(req)
            }
          }
        }
      case Some(id) =>
        throttlerPerChat.throttle(id) {
          throttlerAllRequests.throttle {
            base(req)
          }
        }
      case _ =>
        throttlerAllRequests.throttle {
          base(req)
        }
    }
  }
}
