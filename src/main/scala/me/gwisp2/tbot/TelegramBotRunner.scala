package me.gwisp2.tbot

import scala.concurrent.Future

abstract class TelegramBotRunner {
  def run(): Unit

  def shutdown(): Future[Unit]
}
