package me.gwisp2.tbot

import scala.concurrent.Future

sealed abstract class Result

object Result {

  case object Empty extends Result

  case class Ready(reply: Reply) extends Result

  case class InFuture(reply: Future[Reply]) extends Result

}

trait ToResult[-T] {
  def toResult(v: T): Result
}

object ToResult {
  implicit val result: ToResult[Result] = v => v
  implicit val reply: ToResult[Reply] = v => Result.Ready(v)

  implicit val optionResult: ToResult[Option[Result]] = {
    case Some(r) => r
    case None => Result.Empty
  }

  implicit val optionReply: ToResult[Option[Reply]] = {
    case Some(r) => Result.Ready(r)
    case None => Result.Empty
  }

  implicit val future: ToResult[Future[Reply]] = future => Result.InFuture(future)

  implicit val optionFuture: ToResult[Option[Future[Reply]]] = {
    case Some(f) => Result.InFuture(f)
    case None => Result.Empty
  }
}
