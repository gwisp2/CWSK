package me.gwisp2.tbot

import info.mukel.telegrambot4s.api.{AkkaImplicits, BotExecutionContext, RequestHandler}
import info.mukel.telegrambot4s.models.Update

trait TelegramBot extends AkkaImplicits with BotExecutionContext {
  def request: RequestHandler

  def dispatchUpdate(update: Update): Result
}
