package me.gwisp2.tbot

import akka.NotUsed
import akka.stream.scaladsl.Source
import com.typesafe.scalalogging.StrictLogging
import info.mukel.telegrambot4s.methods.{DeleteWebhook, GetUpdates}
import info.mukel.telegrambot4s.models.Update
import me.gwisp2.tbot.Result.Empty

import scala.concurrent.Future
import scala.util.control.NonFatal
import scala.util.{Failure, Success, Try}

class PollingBotRunner(bot: TelegramBot) extends TelegramBotRunner with StrictLogging {

  import bot.{executionContext, materializer}

  /**
    * Defines long-polling request interval, by default 30 seconds.
    *
    * @return
    */
  def pollingInterval: Int = 30

  private val updatesSource: Source[Update, NotUsed] = {
    type Offset = Long
    type Updates = Seq[Update]
    type OffsetUpdates = Future[(Offset, Updates)]

    val seed: OffsetUpdates = Future.successful((0L, Seq.empty[Update]))

    val iterator = Iterator.iterate(seed) {
      _ flatMap {
        case (offset, newUpdates) =>
          val maxOffset = newUpdates.map(_.updateId).fold(offset)(_ max _)
          bot.request(GetUpdates(Some(maxOffset + 1), timeout = Some(pollingInterval)))
            .recover {
              case NonFatal(e) =>
                logger.error("GetUpdates failed", e)
                Seq.empty[Update]
            }
            .map {
              (maxOffset, _)
            }
      }
    }

    val parallelism = Runtime.getRuntime.availableProcessors()

    val updateGroups =
      Source.fromIterator(() => iterator)
        .mapAsync(parallelism)(
          _ map {
            case (_, updates) => updates
          })

    updateGroups.mapConcat(_.to) // unravel groups
  }

  override def run(): Unit = {
    bot.request(DeleteWebhook).onComplete {
      case Success(true) =>
        logger.info(s"Starting polling: interval = $pollingInterval")

        // Updates are executed synchronously by default to preserve order.
        // To make it asynchronous, just wrap the update handler in a Future
        // or mix AsyncUpdates.
        updatesSource
          .runForeach {
            update =>
              try {
                val result = bot.dispatchUpdate(update)
                handleResult(update, result)
              } catch {
                case NonFatal(e) =>
                  logger.error("Caught exception in update handler", e)
              }
          }

      case Success(false) =>
        logger.error("Failed to clear webhook")

      case Failure(e) =>
        logger.error("Failed to clear webhook", e)
    }
  }

  override def shutdown(): Future[Unit] = {
    logger.info("Shutting down polling")
    bot.system.terminate() map (_ => ())
  }

  private def handleResult(update: Update, result: Result): Unit = {
    result match {
      case Empty => /* Do nothing */
      case Result.Ready(reply) => sendReplySafe(reply, update)
      case Result.InFuture(replyFuture) => replyFuture.foreach(sendReplySafe(_, update))
    }
  }

  private def sendReplySafe(reply: Reply, update: Update): Unit = {
    Try(reply.send(bot, Some(update))).failed.foreach(e => logger.warn("Exception sending reply ", e))
  }
}
