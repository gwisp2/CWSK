CREATE TABLE `deals_history` (
  `seller_id` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `buyer_id` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `deals_history_item_id_fk` (`item_id`),
  CONSTRAINT `deals_history_item_id_fk` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE items ADD COLUMN price_gold DECIMAL(8,1) DEFAULT NULL;