ALTER TABLE `chars` ADD COLUMN `pet_icon` VARCHAR(8) NULL AFTER guild_id;
ALTER TABLE `chars` ADD COLUMN `pet_full_name` VARCHAR(60) NULL AFTER pet_icon;
ALTER TABLE `chars` ADD COLUMN `pet_level` INTEGER NULL AFTER pet_full_name;