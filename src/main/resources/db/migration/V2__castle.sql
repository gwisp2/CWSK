-- Add castle to guild and chars
ALTER TABLE `chars` ADD COLUMN `castle` VARCHAR(8) COLLATE utf8mb4_bin NOT NULL DEFAULT "?" AFTER `name`;
ALTER TABLE `guilds` ADD COLUMN `castle` VARCHAR(8) COLLATE utf8mb4_bin NOT NULL DEFAULT "?" AFTER `tag`;

-- Make guild tag optional
ALTER TABLE guilds CHANGE `tag` `tag` varchar(8) COLLATE utf8mb4_unicode_ci NULL;



