-- Add column for guild permissions
ALTER TABLE guild_roles ADD COLUMN (permissions_bitmask INT NOT NULL DEFAULT 0);