CREATE TABLE `guilds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `guilds_tag_uindex` (`tag`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'USER',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `chars` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `game_class` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `atk` int(11) NOT NULL DEFAULT '0',
  `exp` int(11) NOT NULL DEFAULT '1',
  `level` int(11) NOT NULL DEFAULT '1',
  `pog` int(11) NOT NULL DEFAULT '1',
  `def` int(11) NOT NULL DEFAULT '0',
  `guild_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `equipment_is_valid` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `chars_guild_id_index` (`guild_id`),
  CONSTRAINT `chars_guilds_id_fk` FOREIGN KEY (`guild_id`) REFERENCES `guilds` (`id`),
  CONSTRAINT `chars_users_id_fk` FOREIGN KEY (`id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `equipment_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `line` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enchantment_level` int(11) NOT NULL DEFAULT '0',
  `atk_bonus` int(11) NOT NULL DEFAULT '0',
  `def_bonus` int(11) NOT NULL DEFAULT '0',
  `stamina_bonus` int(11) NOT NULL DEFAULT '0',
  `bag_bonus` int(11) NOT NULL DEFAULT '0',
  `slot_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `chars_equipment` (
  `id` int(11) NOT NULL,
  `e1` int(11) DEFAULT NULL,
  `e2` int(11) DEFAULT NULL,
  `e3` int(11) DEFAULT NULL,
  `e4` int(11) DEFAULT NULL,
  `e5` int(11) DEFAULT NULL,
  `e6` int(11) DEFAULT NULL,
  `e7` int(11) DEFAULT NULL,
  `e8` int(11) DEFAULT NULL,
  `e9` int(11) DEFAULT NULL,
  `e10` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `chars_equipment_equipment_items_id1_fk` (`e1`),
  KEY `chars_equipment_equipment_items_id2_fk` (`e2`),
  KEY `chars_equipment_equipment_items_id3_fk` (`e3`),
  KEY `chars_equipment_equipment_items_id4_fk` (`e4`),
  KEY `chars_equipment_equipment_items_id5_fk` (`e5`),
  KEY `chars_equipment_equipment_items_id6_fk` (`e6`),
  KEY `chars_equipment_equipment_items_id7_fk` (`e7`),
  KEY `chars_equipment_equipment_items_id8_fk` (`e8`),
  KEY `chars_equipment_equipment_items_id9_fk` (`e9`),
  KEY `chars_equipment_equipment_items_id10_fk` (`e10`),
  CONSTRAINT `chars_equipment_chars_id_fk` FOREIGN KEY (`id`) REFERENCES `chars` (`id`),
  CONSTRAINT `chars_equipment_equipment_items_id10_fk` FOREIGN KEY (`e10`) REFERENCES `equipment_items` (`id`),
  CONSTRAINT `chars_equipment_equipment_items_id1_fk` FOREIGN KEY (`e1`) REFERENCES `equipment_items` (`id`),
  CONSTRAINT `chars_equipment_equipment_items_id2_fk` FOREIGN KEY (`e2`) REFERENCES `equipment_items` (`id`),
  CONSTRAINT `chars_equipment_equipment_items_id3_fk` FOREIGN KEY (`e3`) REFERENCES `equipment_items` (`id`),
  CONSTRAINT `chars_equipment_equipment_items_id4_fk` FOREIGN KEY (`e4`) REFERENCES `equipment_items` (`id`),
  CONSTRAINT `chars_equipment_equipment_items_id5_fk` FOREIGN KEY (`e5`) REFERENCES `equipment_items` (`id`),
  CONSTRAINT `chars_equipment_equipment_items_id6_fk` FOREIGN KEY (`e6`) REFERENCES `equipment_items` (`id`),
  CONSTRAINT `chars_equipment_equipment_items_id7_fk` FOREIGN KEY (`e7`) REFERENCES `equipment_items` (`id`),
  CONSTRAINT `chars_equipment_equipment_items_id8_fk` FOREIGN KEY (`e8`) REFERENCES `equipment_items` (`id`),
  CONSTRAINT `chars_equipment_equipment_items_id9_fk` FOREIGN KEY (`e9`) REFERENCES `equipment_items` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `cwapi_tokens` (
  `user_id` int(11) NOT NULL,
  `cw_id` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `allowed_get_stock` tinyint(1) NOT NULL DEFAULT '0',
  `allowed_wtb` tinyint(1) NOT NULL DEFAULT '0',
  `allowed_get_profile` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `exp_history` (
  `user_id` int(11) NOT NULL,
  `exp` int(11) NOT NULL,
  `at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `exp_history_index` (`user_id`,`at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `gold_sink_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rule` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `gold_sink_guild_settings` (
  `guild_id` int(11) NOT NULL,
  `sink_rule_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`guild_id`),
  KEY `gold_sink_guild_settings_gold_sink_rules_id_fk` (`sink_rule_id`),
  CONSTRAINT `gold_sink_guild_settings_gold_sink_rules_id_fk` FOREIGN KEY (`sink_rule_id`) REFERENCES `gold_sink_rules` (`id`),
  CONSTRAINT `gold_sink_guild_settings_guilds_id_fk` FOREIGN KEY (`guild_id`) REFERENCES `guilds` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `gold_sink_user_settings` (
  `user_id` int(11) NOT NULL,
  `sink_rule_id` int(11) DEFAULT NULL,
  `autosink_enabled` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  KEY `gold_sink_user_settings_gold_sink_rules_id_fk` (`sink_rule_id`),
  CONSTRAINT `gold_sink_user_settings_gold_sink_rules_id_fk` FOREIGN KEY (`sink_rule_id`) REFERENCES `gold_sink_rules` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `guild_roles` (
  `guild_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `is_leader` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`guild_id`,`user_id`),
  KEY `guild_roles_chars_id_fk` (`user_id`),
  CONSTRAINT `guild_roles_chars_id_fk` FOREIGN KEY (`user_id`) REFERENCES `chars` (`id`),
  CONSTRAINT `guild_roles_guilds_id_fk` FOREIGN KEY (`guild_id`) REFERENCES `guilds` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ingame_id` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `items_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=218 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `stock_contents` (
  `user_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  UNIQUE KEY `stock_contents_user_id_item_id_uindex` (`user_id`,`item_id`),
  KEY `stock_contents_items_id_fk` (`item_id`),
  CONSTRAINT `stock_contents_items_id_fk` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;