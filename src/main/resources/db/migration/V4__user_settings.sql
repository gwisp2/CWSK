CREATE TABLE user_settings (
    user_id INT NOT NULL PRIMARY KEY,
    stock_diff_enabled BOOLEAN NOT NULL DEFAULT 0
);
