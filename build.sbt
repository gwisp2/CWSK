lazy val configFile = taskKey[String]("configFile")
lazy val root = (project in file("."))
  .enablePlugins(SlickGenPlugin)
  .enablePlugins(DockerPlugin)
  .enablePlugins(BuildInfoPlugin)
  .settings(
    name := "CWSwissKnife",

    version := "0.1",

    scalaVersion := "2.12.8",
    scalacOptions ++= Seq("-deprecation", "-feature", "-Ywarn-unused", "-Ypartial-unification"),

    libraryDependencies += "io.kamon" %% "kamon-core" % "1.1.0",
    libraryDependencies += "io.kamon" %% "kamon-statsd" % "1.0.0",
    libraryDependencies += "io.kamon" %% "kamon-system-metrics" % "1.0.0",
    libraryDependencies += "net.codingwell" %% "scala-guice" % "4.2.1",

    libraryDependencies += "info.mukel" %% "telegrambot4s" % "3.0.16",
    libraryDependencies += "io.suzaku" %% "boopickle" % "1.3.0",

    libraryDependencies += "org.flywaydb" % "flyway-core" % "5.2.4",
    libraryDependencies += "com.typesafe.slick" %% "slick" % "3.2.3",
    libraryDependencies += "com.typesafe.slick" %% "slick-hikaricp" % "3.2.3",
    libraryDependencies += "org.mariadb.jdbc" % "mariadb-java-client" % "2.2.6",

    libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.9.0",
    libraryDependencies += "com.github.pureconfig" %% "pureconfig" % "0.9.1",

    libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3",

    libraryDependencies += "com.rabbitmq" % "amqp-client" % "5.3.0",

    libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "1.1.1",
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % "test",
    libraryDependencies += "org.testng" % "testng" % "6.14.3" % "test",

    Compile / mainClass := Some("me.gwisp2.cwsk.Main"),

    Test / fork := true,
    configFile := "conf/dev.conf",
    javaOptions += "-Dconfig.file=" + configFile.value,

    imageNames in docker := Seq(ImageName(s"cwswissknife:${version.value}")),
    dockerfile in docker := {
      val jarFile: File = sbt.Keys.`package`.in(Compile, packageBin).value
      val classpath = (managedClasspath in Compile).value
      val mainclass = mainClass.in(Compile, packageBin).value.getOrElse(sys.error("Expected exactly one main class"))
      val jarTarget = s"/app/${jarFile.getName}"
      // Make a colon separated classpath with the JAR file
      val classpathString = classpath.files.map("/app/" + _.getName).mkString(":") + ":" + jarTarget

      new Dockerfile {
        // Base image
        from("openjdk:11")
        // Add all files on the classpath
        add(classpath.files, "/app/")
        // Add the JAR file
        add(jarFile, jarTarget)
        // On launch run Java with the classpath and the main class
        entryPoint("java", "-Xmx128m", "-Xms64m", "-Dconfig.file=/app/application.conf", "-cp", classpathString, mainclass)
      }
    },

    /* Configuration that is used for generation of Tables.scala (by slickGen task) */
    slickGenConf := "work/application.conf",

    /* Configuration for BuildInfoPlugin */
    buildInfoKeys := Seq[BuildInfoKey](
      name, version, buildInfoBuildNumber,
      BuildInfoKey.action("commitHash") {
        import sys.process._;
        """git log -1  --pretty=format:%h""".!!.trim
      },
      BuildInfoKey.action("commitMessage") {
        import sys.process._;
        """git log -1  --pretty=format:%s""".!!.trim
      }
    ),
    buildInfoOptions += BuildInfoOption.BuildTime,
    buildInfoPackage := "me.gwisp2.cwsk"
  )