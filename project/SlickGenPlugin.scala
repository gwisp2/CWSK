import com.typesafe.config.ConfigFactory
import sbt._
import slick.codegen.SourceCodeGenerator
import slick.jdbc.JdbcBackend.Database
import slick.jdbc.MySQLProfile

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration


object SlickGenPlugin extends AutoPlugin {

  object autoImport {
    val slickGenConf = settingKey[String]("configuration of Slick code generator")
    val slickGen = taskKey[Unit]("Slick code generator")
  }

  import autoImport._

  override lazy val projectSettings = Seq(
    slickGen := slickGenCommand.value
  )

  lazy val slickGenCommand =
    Def.task {
      val config = ConfigFactory.parseFile(new File(slickGenConf.value))
      val db = Database.forConfig("db", config)
      val modelAction = MySQLProfile.createModel()
      val modelFuture = db.run(modelAction)

      val codegenFuture = modelFuture.map(model => new SourceCodeGenerator(model) {
        override def Table = new Table(_) {
          override def TableClass = new TableClass() {
            override def code: String = {
              /* Copied from slick/codegen/AbstractGenerator.scala */
              val prns = parents.map(" with " + _).mkString("")
              /* No schema name should be generated */
              val args = Seq("\"" + model.name.table + "\"")
              s"""
class $name(_tableTag: Tag) extends profile.api.Table[$elementType](_tableTag, ${args.mkString(", ")})$prns {
  ${indent(body.map(_.mkString("\n")).mkString("\n\n"))}
}
        """.trim()
            }
          }

          // override contained column generator
          override def Column = new Column(_) {
            // use the data model member of this column to change the Scala type,
            // e.g. to a custom enum or anything else
            override def rawType =
              if (model.name == "SOME_SPECIAL_COLUMN_NAME") "MyCustomType" else super.rawType
          }
        }
      })

      val codegen = Await.result(codegenFuture, Duration.Inf)
      codegen.writeToFile(
        "slick.jdbc.MySQLProfile",
        "src/main/scala",
        "me.gwisp2.cwsk.model",
        "Tables",
        "Tables.scala"
      )
    }
}